. "$PSScriptRoot\settings.ps1"
$defaults
$settings = $defaults
$SID  = [System.Security.Principal.WindowsIdentity]::GetCurrent().user
$SID_Users = "S-1-5-32-545"
$SID_Administrators = "S-1-5-32-544"
$SID_Guests = "S-1-5-32-546"
$SID_BackupOperators ="S-1-5-32-551"
$SID_RemoteDesktop ="S-1-5-32-555"
$SID_AuthenticatedUsers = "S-1-5-11"
$SID_LocalAccount = "S-1-5-113"
$SID_LocalAccountAndAdmin = "S-1-5-114"
$SID_Guests = "S-1-5-32-546"
$SID_NetworkService = "S-1-5-20"
$SID_ALLSERVICES = "S-1-5-80-0"
$SID_NetworkConfigOperators = "S-1-5-32-556"
$SID_PerfLogUsers = "S-1-5-32-559"
$SID_PerfMonUsers = "S-1-5-32-558"
$SID_EventLogUsers = "S-1-5-32-573"
$SID_RemoteMgtUsers = "S-1-5-32-580"
$SID_DomainPrefix = $SID.AccountdomainSid
$SID_DomainAdmin = "$SID_DomainPrefix-512"
$SID_EnterpriseAdmin = "$SID_DomainPrefix-519"
$SID_SchemaAdmin = "$SID_DomainPrefix-518"

# Compatibility
if ($PSVersionTable.PSVersion.major -lt 7 -or ($PSVersionTable.PSVersion.major -eq 7 -and $PSVersionTable.PSVersion.minor -lt 2)) {
    $PSStyle = @{}
    $psstyle.reset = ""
    $compat = $true
    Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicy.psm1" -DisableNameChecking -ErrorAction SilentlyContinue
	Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicyParser.psm1" -DisableNameChecking -ErrorAction SilentlyContinue
    Import-module "$PSScriptRoot\modules\ADDSInfo\ADDSInfo.psd1" -DisableNameChecking
} else {
    # TODO: investigate why this is needed. Seems to be, to access [Microsoft.GroupPolicy] namespace
    import-module grouppolicy -SkipEditionCheck
    Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicy.psm1" -DisableNameChecking 2>nul 3>nul
	Import-Module "$PSScriptRoot\modules\GPRegistryPolicy\GPRegistryPolicyParser.psm1" -DisableNameChecking  2>nul 3>nul
    Import-module "$PSScriptRoot\modules\ADDSInfo\ADDSInfo.psd1" -DisableNameChecking 2>nul 3>nul
}

# Get public and private function definition files
    $public = @( Get-ChildItem -path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue | sort-object fullName)
    $private = @( Get-ChildItem -path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue | sort-object fullName)
    $variables = @( Get-ChildItem -path $PSScriptRoot\vars\*.ps1 -ErrorAction SilentlyContinue | sort-object fullName)



#Dot Source the files
    foreach ($import in @($Private + $Public))
    {
        Try
        {
            . $import.fullname
        }
        Catch
        {
            write-error -Message "Failed to import function $($import.fullname): $_"
        }
    }
    $ObjectGUIDs = get-ADObjectGUIDs
    foreach ($import in @($variables))
    {
        Try
        {
            . $import.fullname
        }
        Catch
        {
            write-error -Message "Failed to import function $($import.fullname): $_"
            write-error $_.scriptstacktrace
        }
    }

if ($PSVersionTable.psversion.major -ge 7) {
    $CompatMode = $False
} else {
    $CompatMode = $true
}



# Stuff to do
    # Read in or create an initial config file and variable
    # Export functions that are WIP
    # set variables visible to module
#$ConfirmPreference = "High"

Export-moduleMember -function $public.basename
Export-modulemember -variable settings
Export-modulemember -variable defaults
Export-modulemember -variable orgTemplate
Export-modulemember -variable GlobalTemplate
Export-modulemember -variable ComponentTemplate
Get-started