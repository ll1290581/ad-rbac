#Region OU Struct definition
$BasicOUStructures = @(

)

$ToplevelStruct = @{
    Name = $(Split-LDAPPath -distinguishedName $settings['OUPaths']['TenantRoot'] -leaf -NodeNameOnly)
    Description = "Business Units, Tenants, or Teams with a shared leadership"
    Path = $(Split-LDAPPath -distinguishedName $settings['OUPaths']['TenantRoot'] -Parent)
    Template = @{
        OUs = @(
            @{
                name = $Settings['Names']['OrgsOU']
                Description = " ---- Being created ---"
            }
        )
        DefaultRights = @()
        DefaultRoles = @()
    }
}

$NetgroupName = "Netgroups"
$SudoRolesName = "SudoRoles"
$LinuxFeaturesOUStruct = @{
    Name=$Settings['Names']['LinuxFeaturesOU']
    Description="Linux-related objects such as sudoRoles supported by sssd-ad"
    Path=$(Split-LDAPPath -distinguishedName $settings['OUPaths']['LinuxFeaturesBase'] -Parent)
    Template = @{
        OUs = @(
            @{
                Name=$Settings['Names']['NetgroupsOU']
                Description="Netgroups that group multiple hosts together for assigning sudoRoles. Referenced by ldap_netgroup_search_base"
            }
            @{
                Name=$Settings['Names']['SudoersOU']
                Description="LDAP-based sudoers rules. Referenced by ldap_sudo_search_base. Host assignment is done via netgroups."
            }
        )
    }
}

$GlobalOUStruct = @{
    Name=$(Split-LDAPPath -distinguishedName $settings['OUPaths']['Global'] -leaf -nodeNameOnly)
    Description="Objects not specific to a business unit; users, global rights, etc"
    Path=$(Split-LDAPPath -distinguishedName $settings['OUPaths']['Global'] -parent)
}
#endregion
