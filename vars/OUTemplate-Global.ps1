$GlobalTemplate = @{}
$GlobalTemplate.add(
    "sudoRoles", @(
        'LinuxSudoFull'
        'LinuxSudoFullNoAuth'
        'LinuxSudoOperate'
        'LinuxSudoConfigure'
    )
)
$GlobalTemplate.Add(
    "LDAPContainers", @(
        @{
            Name        = $Settings['Names']['UsersBase']
            Type        = "OrganizationalUnit"
            Description = "Default location for user accounts."
        }
        @{
            Name        = $Settings['Names']['UnprivUsersOU']
            Type        = "OrganizationalUnit"
            RelativePath= "OU={0}" -f $Settings['Names']['UsersBase']
            Description = "Default location for user accounts."
        }
        @{
            Name        = $Settings['Names']['UsersTier0OU']
            Type        = "OrganizationalUnit"
            RelativePath= "OU={0}" -f $Settings['Names']['UsersBase']
            Description = "Sensitive / Administrative user accounts. Generally domain admins etc."
        }
        @{
            Name        = $Settings['Names']['EndpointsOU']
            Type        = "OrganizationalUnit"
            Description = "Global and unassigned computer endpoints"
        }
        @{
            Name        = $Settings['Names']['DefaultComputersOU']
            Type        = "OrganizationalUnit"
            RelativePath= "OU={0}" -f $Settings['Names']['EndpointsOU']
            Description = "Computer objects not associated with a Component"
        }
        @{
            Name        = $Settings['Names']['EndpointPAWs']
            Type        = "OrganizationalUnit"
            RelativePath= "OU={0}" -f $Settings['Names']['EndpointsOU']
            Description = "T0 Privileged Access Workstations"
        }
        @{
            Name        = $Settings['Names']['EndpointPKI']
            Type        = "OrganizationalUnit"
            RelativePath= "OU={0}" -f $Settings['Names']['EndpointsOU']
            Description = "PKI / certificate systems"
        }
        @{
            DistinguishedName = $Settings['OUPaths']['LinuxFeaturesBase']
            Type        = "Container"
            Description="Linux-related objects such as sudoRoles supported by sssd-ad"
        }
        @{
            DistinguishedName = $Settings['OUPaths']['LinuxNetgroups']
            Type        = "Container"
            Description="Netgroups that group multiple hosts together for assigning sudoRoles. Referenced by ldap_netgroup_search_base"
        }
        @{
            DistinguishedName = $Settings['OUPaths']['LinuxSudoers']
            Type        = "Container"
            Description="LDAP-based sudoers rules. Referenced by ldap_sudo_search_base. Host assignment is done via netgroups."
        }
        @{
            DistinguishedName        = $Settings['OUPaths']['GlobalRights']
            Type        = "OrganizationalUnit"
            Description = "Rights applied across entire RAD Tenant"
        }
        @{
            DistinguishedName        = $Settings['OUPaths']['GlobalRoles']
            Type        = "OrganizationalUnit"
            Description = "Roles with broad rights"
        }
        @{
            DistinguishedName        = $Settings['OUPaths']['OrgsBase']
            Type        = "OrganizationalUnit"
            Description = "Business Units, Tenants, or Teams with a shared leadership"
        }
    )
)
$GlobalTemplate.Add(
    "DefaultRoles", @(
        <#@{
            nameSuffix      = $Settings['names']['RolesList']['Admin']
            Protected       = $True
            Owner           = $true
            Description     = "Global Admin. Assumed to have full administrative rights on all systems in child components. Does not have domain admin rights"
            Rights          = @(
                'LAPS'
                'GPOEdit'
                'GPOAudit'
                'GPOLink'
                'OUCreate'
                'OUManage'
                'ManageRights'
                'ManageRoles'
                'SudoManager'
                'DNSOperator'
            )
            AuxiliaryGroups = @(
                "Group Policy Creator Owners"
                #"DHCP Administrators" -- https://www.falconitservices.com/dhcp-administrators-group-missing-from-active-directory/
            )
        }#>

        @{
            nameSuffix      = $Settings['names']['RolesList']['RBACAdmin']
            Protected       = $true
            Description     = "Manage all RBAC groups, OUs, and GPOs. Imlicit full control of everything under RBAC root. Restricted Login."
            Rights          = @(
                'ManageRights'
                'ManageRoles'
                'OUManage'
                'GPOEdit'
                'GPOLink'
                'OrgManage'
                'OrgDelete'
                'SudoManager'
                'DenyRemote'
                'DenyService'
                'GPOCreate'

            )
        }
        @{
            nameSuffix      = $Settings['names']['RolesList']['Operator']
            Protected       = $false
            Description     = "Local admin rights, DNS / DHCP, Cert enrollment, and domain join"
            Rights          = @(
                'CreateDeleteComputer'
                'PKIEnroll'
                'WinAdmin'
                'GPOAudit'
                'LogonRemote'
                'LogonLocal'
                'LinuxSudoFull'
                'LAPS'
                'DHCPAdmin'
                'DNSOperator'
                'DFSNamespaceManage'
                'DFSReplCreate'
                'DenyService'
                )
        }
        @{
            nameSuffix      = $Settings['names']['RolesList']['SvcRemoteAdmin']
            Protected       = $false
            Owner           = $False
            Description     = "Local admin rights. Sudo without password. Suitable for remote Scan services like Nessus."
            Rights          = @(
                'WinAdmin'
                'LinuxSudoFullNoAuth'
                'LogonRemote'
                'DenyService'
            )
        }
        @{
            nameSuffix      = $Settings['names']['RolesList']['OrgAdmin']
            Protected       = $false
            Description     = "Access to create and delete Orgs. Implicit full control of all objects under all orgs."
            Rights          = @(
                'OrgManage'
                'OrgDelete'
                'SudoManager'
                'DenyService'
                'GPOCreate'
            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['AppAdmin']
            Protected   = $false
            Description = "Admin access within all applications under all components. This does not explicitly grant RDP / SSH access or host root."
            Rights      = @(
                'AppAdmin'
                'AppPoweruser'
                'AppAccess'
                'DenyService'
            )
        }
        @{
            nameSuffix      = $Settings['names']['RolesList']['GPOAdmin']
            Protected       = $false
            Description     = "Permissions to edit, link and troubleshoot all GPOs created within RBAC structure"
            Rights          = @(
                'GPOEdit'
                'GPOLink'
                'GPOAudit'
                'DenyService'
                'GPOCreate'
            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['PKIAdmin']
            Protected   = $true
            Owner       = $false
            Description = "Permissions to approve cert requests and manage CAs. Restricted login."
            Rights      = @(
                'PKIManageCA'
                'PKIIssue'
                'DenyRemote'
                'DenyService'
            )
        }

        @{
            nameSuffix  = $Settings['names']['RolesList']['AccountAdmin']
            Protected   = $false
            Owner       = $false
            Description = "Create and reset standard accounts; create sensitive accounts"
            Rights      = @(
                'UserCreate'
                'UserPasswdReset'
                'UserControl'
                'AdminUserCreate'
                'AdminUserControl'
                'DenyService'
            )
        }
    )
)
$GlobalTemplate.Add(
    "DefaultRights", @(
        'DHCPAdmin'
        'DNSOperator'
        'DFSReplCreate'
        'DFSNamespaceManage'
        'PKIManageCA'
      <#'PKIEnrollmentAgent']#>
        'PKIIssue'
        'PKIEnroll'
        'UserCreate'
        'UserControl'
        'UserPasswdReset'
        'AdminUserPasswdReset'
        'AdminUserCreate'
        'AdminUserControl'
        'SudoManager'
        'AppAccess'
        'AppPoweruser'
        'AppAdmin'
        'CreateDeleteComputer'
        'GPOCreate'
        'GPOAudit'
        'GPOLink'
        'GPOEdit'
        'OUCreate'
        'OUManage'
        'OrgManage'
        'OrgDelete'
        'ManageRights'
        'ManageRoles'
        'LAPS'
        'WinAdmin'
        'WinOps'
        foreach ($role in $globalTemplate['sudoRoles']){
            $role
        }
        'LogonLocal'
        'LogonRemote'
        'LogonBatch'
        'LogonService'
        'DenyRemote'
        'DenyService'

    )
)
$GlobalTemplate.Add(
    "OUDelegations", @(
                # Some groups need to be edited by OrgManage
                @{
                    ADPathQuery      = @{
                        filter = ("objectClass -eq 'Group' -and name -like '*{0}'" -f $Settings['Rights']['GPOCreate'].NameSuffix )
                        searchBase =  $Settings['OUPaths']['GlobalRights']
                    }
                    ApplyDefaultDeny       = $false
                    ACLs = @(
                        @{
                            Right = 'OrgManage'
                            ACEs            = $Settings['DACLs']['GroupControl']
                        }
                    )
                }
        @{
            ADPath                 = "CN=MicrosoftDNS,DC=DomainDNSZones,{0}" -f $(get-dsinfo).rootDomainNamingContext
            ApplyDefaultDeny       = $False
            ACLs                   = @(
                @{
                    Right = 'DNSOperator'
                    ACEs            = $Settings['DACLs']['DNSOperator']
                }
            )
        }
        @{
            ADPath                 = "CN=MicrosoftDNS,DC=ForestDNSZones,{0}" -f $(get-dsinfo).rootDomainNamingContext
            ApplyDefaultDeny       = $False
            ACLs                   = @(
                @{
                    Right = 'DNSOperator'
                    ACEs            = $Settings['DACLs']['DNSOperator']
                }
            )
        }
        @{
            ADPath                 = "CN=MicrosoftDNS,CN=System,{0}" -f $(get-dsinfo).rootDomainNamingContext
            ApplyDefaultDeny       = $False
            ACLs                   = @(
                @{
                    Right = 'DNSOperator'
                    ACEs            = $Settings['DACLs']['DNSOperator']
                }
                @{
                    Principal = "DNSAdmins"
                    ACEs = @(
                        @{
                            ADRight         = "GenericRead","ExtendedRight"
                            InheritanceType = "All"
                        }
                    )
                }
            )
        }
        @{
            ADPath                 = "CN=Dfs-Configuration,CN=System,{0}" -f $(get-dsinfo).rootDomainNamingContext
            ApplyDefaultDeny       = $False
            ACLS                   = @(
                @{
                    Right = 'DFSNamespaceManage'
                    ACEs            = $Settings['DACLs']['DFSNamespaceManage']
                }
            )
        }
        @{
            ADPath                 = "CN=DFSR-GlobalSettings,CN=System,{0}" -f $(get-dsinfo).rootDomainNamingContext
            ApplyDefaultDeny       = $False
            ACLS                   = @(
                @{
                    Right = 'DFSReplCreate'
                    ACEs            = $Settings['DACLs']['DFSReplCreate']
                }
            )
        }

        @{
            ADPath                 = $Settings['OUPaths']['TenantRoot']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "All"
            ACLs                   = @(
                #region OUManagement
                @{
                    Right = 'OUCreate'
                    ACEs            = $Settings['DACLs']['OUCreate']
                }
                @{
                    Right = 'OUManage'
                    ACEs            = $Settings['DACLs']['OUManage']
                }
                #endregion
                #region GPOs
                @{
                    Right = 'GPOAudit'
                    ACEs            = $Settings['DACLs']['GPOAudit']
                }
                @{
                    Right = 'GPOLink'
                    ACEs            = @(
                        @{
                            ADRight         = "ReadProperty, WriteProperty"
                            TargetObject    = "GP-Link"
                            InheritanceType = "All"
                        }
                    )
                }
                @{
                    Right = 'GPOEdit'
                    ACEs            = $Settings['DACLs']['GPOEdit']
                }
                #endRegion
                #Region RightsManagement
                @{
                    Right = 'ManageRights'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
                #endregion
                #Region LAPSReadPassword
                if ($ObjectGUIDs.name.contains("ms-LAPS-EncryptedPassword")) {
                    @{
                        Right = 'LAPS'
                        ACEs            = $Settings['DACLs']['LAPS']
                    }
                    @{
                        Principal = "NT Authority\SELF"
                        ACEs      = $Settings['DACLs']['LAPS']
                    }
                }
                else {
                    write-loghandler -level "warning" -message "ms-LAPS-EncryptedPassword schema object is missing: you may need to update-lapsADSchema"
                }
                #endregion
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['OrgsBase']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'OrgManage'
                    ACEs            = @(
                        $Settings['DACLs']['OUCreate']
                        $Settings['DACLs']['OUManage']
                        $Settings['DACLs']['GroupControl']
                        $Settings['DACLs']['GPOLink']
                    )
                }
                @{
                    Right = 'CreateDeleteComputer'
                    ACEs =   $Settings['DACLs']['AddComputerAncestor']
                }
                @{
                    Principal = "Everyone"
                    ACEs      = @(
                        @{
                            ADRight         = "CreateChild"
                            TargetObject    = "Computer"
                            Action          = "Deny"
                            InheritanceType = "none"
                        }
                    )
                }
            )

        }
        @{
            ADPath           = "CN=NetServices,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext']
            ApplyDefaultDeny = $false
            ACLs             = @(
                @{
                    Right = 'DHCPAdmin'
                    ACEs            = $Settings['DACLs']['DHCPAdmin']
                }
            )
        }
        @{
            ADPath           = "CN=OID,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext']
            ApplyDefaultDeny = $false
            ACLs             = @(
                if ([bool]$FEATURES_ADCS) {
                    @{
                        Right = 'PKIManageCA'
                        ACEs            = $Settings['DACLs']['PKIManageOIDs']
                    }
                }
                else {
                    write-loghandler -level "warning" -message "ADCS feature not enabled, skipping delegations."
                }
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['LinuxFeaturesBase']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'OUCreate'
                    ACEs            = $Settings['DACLs']['OUCreate']
                }
                @{
                    Right = 'OUManage'
                    ACEs            = $Settings['DACLs']['OUManage']
                }
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['LinuxSudoers']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'SudoManager'
                    ACEs            = @(
                        $Settings['DACLs']['OUCreate']
                        $Settings['DACLs']['OUManage']
                        if ($ObjectGUIDs.name.contains("sudoRole")) {
                            @{
                                ADRight         = "genericAll"
                                TargetObject    = "sudoRole"
                                InheritanceType = "All"
                            }
                        }
                        else {
                            write-loghandler -level "warning" -message "SudoRole schema object is missing: you may need a schema mod."
                        }
                    )
                }
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['LinuxNetgroups']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'SudoManager'
                    ACEs            = @(
                        @{
                            ADRight         = "genericAll"
                            TargetObject    = "NisNetgroup"
                            InheritanceType = "All"
                        }
                        @{
                            ADRight         = "CreateChild"
                            TargetObject    = "Organizational-Unit"
                            InheritanceType = "All"
                        }
                    )
                }
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['DefaultUsers']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "All"
            ACLs                   = @(
                @{
                    Right = 'UserCreate'
                    ACEs            = $Settings['DACLs']['userCreate']
                }
                @{
                    Right = 'UserControl'
                    ACEs            = $Settings['DACLs']['userControl']
                }
                #endRegion
                #region normal password reset delegation
                @{
                    Right = 'UserPasswdReset'
                    ACEs            = $Settings['DACLs']['UserPasswdReset']
                }
                #endregion
            )
        }
        # "OU=$SensitiveUsersOU"
        @{
            ADPath                 = $Settings['OUPaths']['PrivilegedUsers']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "All"
            ACLs                   = @(
                @{
                    Right = 'AdminUserCreate'
                    ACEs            = $Settings['DACLs']['UserCreate']
                }
                @{
                    Right = 'AdminUserControl'
                    ACEs            = $Settings['DACLs']['userControl']
                }
                @{
                    Right = 'AdminUserPasswdReset'
                    ACEs            = $Settings['DACLs']['UserPasswdReset']
                }
            )
        }
        @{
            ADPath                 = $Settings['OUPaths']['DefaultComputers']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                # Todo: tighten up permissions
                # These permissions are broader than they should be. Look into restricting, but following properties may be needed:
                ## Common-name, Sam-Account-name, Description, Display-name, attributeCertificateAttribute, Service-Principal-Name, DNS-Host-name
                ## See also: https://learn.microsoft.com/en-us/answers/questions/973272/delegate-help-desk-users-permission-to-move-users
                @{
                    Right = 'CreateDeleteComputer'
                    ACEs            = $Settings['DACLs']['AddComputer']
                }

            )
        }
        @{
            ADPathLeafOU           = "OU={0}" -f $Settings['Names']['RightsOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                #region rightsAdmin
                @{
                    Right = 'ManageRights'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
            )
        }
        @{
            ADPathLeafOU           = "OU={0}" -f $Settings['Names']['RolesOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                #Region RolesManager
                @{
                    Right = 'ManageRoles'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
                #EndRegion
            )
        }
        # PKI
        if ($(Test-RBACFeatures).PKI) {
            if ([bool](Test-Path ("CN=Certificate Templates,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext']))) {
                # "CN=Certificate Templates,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext']
                @{
                    ADPath           = "CN=Certificate Templates,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext']
                    ApplyDefaultDeny = $false
                    ACLs             = @(
                        @{
                            Right = 'PKIManageCA'
                            ACEs            = $Settings['DACLs']['PKITemplateManage']
                        }
                    )
                }
                @{ # The DACLs apparently need to be directly on the template to apply. Inheritance does not work (can we test this?)
                    ADPathQuery      = @{filter = "objectClass -eq 'pKICertificateTemplate'"; searchBase = "CN=Certificate Templates,CN=Public Key Services,CN=Services,$Runtime['Domain']['ConfigurationNamingContext']" }
                    ApplyDefaultDeny = $false
                    ACLs             = @(
                        @{
                            Right = 'PKIManageCA'
                            ACEs            = $Settings['DACLs']['PKITemplateManage']
                        }
                    )
                }
                @{
                    ADPathQuery      = @{filter = "objectClass -eq 'pKICertificateTemplate' -and name -like '__*'"; searchBase = "CN=Certificate Templates,CN=Public Key Services,CN=Services,$Runtime['Domain']['ConfigurationNamingContext']" }
                    ApplyDefaultDeny = $false
                    ACLs             = @(
                        @{
                            Right = 'PKIEnroll'
                            ACEs            = $Settings['DACLs']['PKITemplateManage']
                        }
                    )

                }
                if ([bool](Test-path "AD:CN=Enrollment Services,CN=Certificate Templates,CN=Public Key Services,CN=Services,$Runtime['Domain']['ConfigurationNamingContext']")) {
                    @{
                        ADPathQuery      = @{filter = "objectClass -eq 'pKIEnrollmentService'"; searchBase = "CN=Enrollment Services,CN=Certificate Templates,CN=Public Key Services,CN=Services,$Runtime['Domain']['ConfigurationNamingContext']" }
                        ApplyDefaultDeny = $false
                        ACLs             = @(
                            @{
                                Right = 'PKIEnroll'
                                ACEs            = $Settings['DACLs']['PKITemplateManage']
                            }
                        )
                    }
                }
                else {
                    write-loghandler -level "warning" -message ("Path not found, skipping delegations: CN=Enrollment Services,CN=Certificate Templates,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext'])
                }
            }
            else {
                write-loghandler -level "warning" -message ("Path not found, skipping delegations: CN=Certificate Templates,CN=Public Key Services,CN=Services,{0}" -f $Runtime['Domain']['ConfigurationNamingContext'])
            }

        }
        else {
            write-loghandler -level "warning" -message ("Cannot find Public key services OU; you may not have installed an Enterprise CA. Skipping PKI delegations.")
        }
    )
)
$GlobalTemplate.Add(
    "GPOs", @(
        @{
            Metadata = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixHigh']
                Comment       = @"
#########################################################################
`tDO NOT EDIT THIS GPO -- CHANGES WILL BE OVERWRITTEN

`tThis GPO is automatically generated by 'add-rbacGlobal'.
`tMake desired edits in the Settings GPO
#########################################################################

Applies basic host-based access control
`t* Adds various "right-global-*" rights to corresponding User Rights Assignments
`t* Adds rights (e.g. WindowsAdmin) to corresponding local groups via restricted groups
`t* Configures LAPS and sets LAPS principal to the right-global-laps... group
"@
                AlwaysRebuild = $true
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @(
                            'GPOEdit'
                        )
                        Principals = @()
                    }
                }
            }
            SecEdit  = @{
                "Privilege Rights" = @{
                    SeInteractiveLogonRight       = @{
                        SIDS       = @(
                            $SID_Administrators
                        )
                        Rights     = @(
                            'LogonLocal'
                        )
                        Principals = @()
                    }
                    SeRemoteInteractiveLogonRight = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_RemoteDesktop
                        )
                        Rights     = @(
                            'LogonRemote'
                        )
                        Principals = @()
                    }
                    SeDenyRemoteInteractiveLogonRight = @{
                        Rights     = @(
                            'DenyRemote'
                        )
                    }
                    SeDenyServiceLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeDenyBatchLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeServiceLogonRight           = @{
                        SIDS       = @(
                            $SID_NetworkService
                            $SID_ALLSERVICES
                        )
                        Rights     = @(
                            'LogonService'
                        )
                        Principals = @()
                    }
                    SeBatchLogonRight             = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_BackupOperators
                            $SID_PerfLogUsers
                        )
                        Rights     = @(
                            'LogonBatch'
                        )
                        Principals = @()
                    }
                }
                "Group Membership" = @{
                    "*$($SID_ALLSERVICES)"          = @{
                        MemberOf = @{
                            SIDS       = @(
                                # Needed for Security log access-- many services (splunk, ADFS?) want this
                                # This seems to get overridden by component-level GPO so we apply it there as well.
                                $SID_EventLogUsers
                            )
                            Rights     = @()
                            Principals = @()
                        }
                    }
                    'WinAdmin'    = @{
                        ResolveKeyName = $true
                        MemberOf       = @{
                            SIDS = @($SID_Administrators)
                        }
                    }
                    'WinOps'      = @{
                        ResolveKeyName = $True
                        MemberOf       = @{
                            SIDS = @(
                                $SID_NetworkConfigOperators
                                $SID_PerfLogUsers
                                $SID_PerfMonUsers
                                $SID_EventLogUsers
                            )
                        }
                    }
                    'LogonRemote' = @{
                        ResolveKeyName = $true
                        MemberOf       = @{
                            SIDS = @(
                                $SID_RemoteMgtUsers
                                $SID_RemoteDesktop
                            )
                        }
                    }
                }
            }
            RegPol   = @(
                @{
                    KeyName         = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName       = "ADPasswordEncryptionPrincipal"
                    ValueType       = "REG_SZ"
                    ValueCollection = @{
                        SIDs       = @()
                        Rights     = @(
                            'LAPS'
                        )
                        Principals = @()
                    }
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PostAuthenticationResetDelay"
                    ValueType = "REG_DWORD"
                    ValueData = "8"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PostAuthenticationActions"
                    ValueType = "REG_DWORD"
                    ValueData = "1"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PasswordComplexity"
                    ValueType = "REG_DWORD"
                    ValueData = "4"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PasswordLength"
                    ValueType = "REG_DWORD"
                    ValueData = "20"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PasswordAgeDays"
                    ValueType = "REG_DWORD"
                    ValueData = "60"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "PwdExpirationProtectionEnabled"
                    ValueType = "REG_DWORD"
                    ValueData = "1"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "BackupDirectory"
                    ValueType = "REG_DWORD"
                    ValueData = "2"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "ADPasswordEncryptionEnabled"
                    ValueType = "REG_DWORD"
                    ValueData = "1"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "ADEncryptedPasswordHistorySize"
                    ValueType = "REG_DWORD"
                    ValueData = "3"
                }
                @{
                    KeyName   = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName = "ADBackupDSRMPassword"
                    ValueType = "REG_DWORD"
                    ValueData = "0"
                }
            )
        }

        @{
            Metadata             = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixLow']
                AlwaysRebuild = $False
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @(
                            'GPOEdit'
                        )
                        Principals = @()
                    }
                }
            }
            GPPrefRegistryValues = @(
                @{
                    Context   = 'Computer'
                    Key       = 'Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\security_mmc.exe'
                    ValueName = 'about'
                    value     = 2
                    type      = 'DWORD'
                    Action    = 'Update'
                }
            )
        }

        @{
            Metadata             = @{
                LinkOrder     = 3
                NamePrefix    = "{0}-Hardened" -f $Settings['names']['GPOs']['PrefixLow']
                AlwaysRebuild = $False
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @(
                            'GPOEdit'
                        )
                        Principals = @()
                    }
                }
            }
        }
    )
)
