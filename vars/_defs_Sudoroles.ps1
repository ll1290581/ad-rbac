$SUDO_PASSWD_TYPES=@(
    ""
    "-nopasswd"
)

$SUDO_ROLE_DEFS = @(
	@{
		name="operate"
		description="read logs, manage services, adjust networking, Kill processes"
		sudoCommand=@(
			"/usr/bin/dmesg",
			"/usr/bin/journalctl",
			"/usr/sbin/aureport",
			"/usr/sbin/ausearch",
			"/usr/bin/tail /var/log/audit/*"
			"/usr/bin/systemctl stop *",
			"/usr/bin/systemctl start *",
			"/usr/bin/systemctl restart *",
			"/usr/bin/systemctl reload *",
			"/usr/bin/systemctl status *"
			"/usr/bin/nmtui",
			"/usr/bin/nmcli",
			"/usr/bin/hostnamectl",
			"/usr/bin/resolvectl",
			"/usr/sbin/ip",
			"/usr/bin/firewall-cmd",
			"/usr/sbin/dhclient",
			"/usr/sbin/ifconfig",
			"/usr/sbin/route"
			"/usr/bin/nice",
			"/usr/bin/kill",
			"/usr/bin/killall"
		)
	}
	@{
		name="software"
		description="yum/dnf"
		sudoCommand=@(
			"/usr/bin/dnf",
			"/usr/bin/yum",
			"/usr/bin/rpm"
		)
	}
	@{
		name="full"
		description="Full Sudo rights: selinux, arbitrary edit, chmod"
		sudoCommand=@(
			"ALL"
		)
	}
)