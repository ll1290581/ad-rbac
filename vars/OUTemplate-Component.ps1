$ComponentTemplate = @{}
$componentTemplate.add(
    "sudoRoles", @(
        'LinuxSudoFull'
        'LinuxSudoFullNoAuth'
        'LinuxSudoOperate'
        'LinuxSudoConfigure'
    )
)
$ComponentTemplate.add(
    "LDAPContainers", @(
        @{
            Name        = $Settings['Names']['EndpointsOU']
            Description = "Computers, Hosts, Devices within or owned by this component"
        }
        @{
            name        = $settings['names']['RightsOU']
            Type        = "Container"
            Description = "Groups granting privileges within or owned by this component. Users should not be members of these groups. Only service accounts or roles should be members of these groups."
        }
        @{
            name        = $settings['names']['RolesOU']
            Type        = "Container"
            description = "Roles associated with multiple 'rights'. Only users should be members of 'role' groups."
        }
        @{
            name        = $Settings['Names']['ServiceAcctOU']
            description = "Service Accounts within or owned by this component"
        }
    )
)
$ComponentTemplate.add(
    "DefaultRoles", @(
        @{
            nameSuffix  = $Settings['names']['RolesList']['Owner']
            Owner       = $true
            Description = "Owner of this component. Assumed to have full administrative rights on all systems in child components."
            Rights      = @(
                'DenyService'
                'AppAccessComponent'
                'AppPoweruserComponent'
                'AppAdminComponent'
                'CreateDeleteComputer'
                'WinAdmin'
                'LogonLocal'
                'LogonRemote'
                'GPOAudit'
                'GPOEdit'
                'GPOLink'
                'LAPS'
                'ManageRights'
                'ManageRoles'
                'ServiceAccountMSA'
                'ServiceAccountLegacy'
                'LinuxSudoFull'
                # No OUCreate
                # No OUManage
            )
            # No GPO Creators / owners
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['Operator']
            Owner       = $False
            Description = "Local operator and app-modify rights."
            Rights      = @(
                'DenyService'
                'AppAccessComponent'
                'AppPoweruserComponent'
                'AppAdminComponent'
                'CreateDeleteComputer'
                'GPOAudit'
                'GPOEdit'
                'LAPS'
                'LogonLocal'
                'LogonRemote'
                'WinAdmin'
                'LinuxSudoOperate'
                'LinuxSudoConfigure'
                'ServiceAccountMSA'
            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['SvcLocal']
            Owner       = $False
            Description = "Service Account with local admin rights."
            Rights      = @(
                'DenyRemote'
                'CreateDeleteComputer'
                'GPOAudit'
                'LogonLocal'
                'LogonService'
                'LogonBatch'
                'WinAdmin'
                'LinuxSudoFullNoAuth'
                'GenerateSecurityAudit'
                'SeTcbPrivilege'

            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['User']
            Owner       = $false
            Description = "Standard user for this component. Assumed to be allowed to log in to the application."
            Rights      = @(
                'DenyService'
                'AppAccessComponent'
                'AppPoweruserComponent'
                'LogonLocal'
                'LogonRemote'
            )
        }

    )
)
$ComponentTemplate.add(
    "DefaultRights", @(
        'AppAccessComponent'
        'AppPoweruserComponent'
        'AppAdminComponent'
        'CreateDeleteComputer'
        'GPOAudit'
        'GPOLink'
        'GPOEdit'
        'ManageRights'
        'ManageRoles'
        'OUCreate'
        'OUManage'
        'LAPS'
        'ServiceAccountMSA'
        'ServiceAccountLegacy'
        'WinAdmin'
        'WinOps'
        foreach ($role in $componentTemplate['sudoRoles']){
            $role
        }
        'GenerateSecurityAudit'
        'SeTcbPrivilege'
        'LogonLocal'
        'LogonRemote'
        'LogonBatch'
        'LogonService'
        'DenyRemote'
        'DenyService'
    )
)




$ComponentTemplate.add(
    "OUDelegations", @(
        # "OU={0}" -f $Settings['Names']['EndpointsOU']
        @{
            ADPathLeafOU           = "OU={0}" -f $Settings['Names']['EndpointsOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'CreateDeleteComputer'
                    ACEs            = @(
                        $Settings['DACLs']['AddComputer']
                        @{ # Necessary to override default deny.
                            ADRight         = "CreateChild, DeleteChild"
                            TargetObject    = "Computer"
                            InheritanceType = "All"
                        }
                    )
                }
            )
        }
        @{
            ADPathLeafOU           = "OU={0}" -f $Settings['Names']['RightsOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'ManageRights'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
            )
        }
        @{
            ADPathLeafOU           = "OU={0}" -f $Settings['Names']['RolesOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'ManageRoles'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
            )
        }
        @{
            ADPathLeafOU           = "OU={0}" -f $settings['Names']['ServiceAcctOU']
            ApplyDefaultDeny       = $true
            DefaultDenyInheritance = "None"
            ACLs                   = @(
                @{
                    Right = 'ServiceAccountMSA'
                    ACEs            = @(
                        $Settings['DACLs']['ServiceAccountMSA']
                        @{  # Necessary to override default deny rule
                            ADRight         = "CreateChild"
                            TargetObject    = @("ms-DS-Managed-Service-Account", "ms-DS-Group-Managed-Service-Account")
                            InheritanceType = "All"
                        }
                    )
                }
                @{
                    Right = 'ServiceAccountLegacy'
                    ACEs            = @(
                        $Settings['DACLs']['ServiceAccountLegacy']
                        $Settings['DACLs']['userCreate'] # Necessary to override default deny rule
                    )
                }
            )
        }
        # ""
        @{
            ADPathLeafOU = ""
            ACLs         = @(
                @{
                    Right = 'OUCreate'
                    ACEs            = $Settings['DACLs']['OUCreate']
                }
                @{
                    Right = 'GPOAudit'
                    ACEs            = $Settings['DACLs']['GPOAudit']
                }
                @{
                    Right = 'GPOLink'
                    ACEs            = $Settings['DACLs']['GPOLink']
                }
                @{
                    Right = 'GPOEdit'
                    ACEs            = $Settings['DACLs']['GPOEdit']
                }
                if ($ObjectGUIDs.name.contains("ms-LAPS-EncryptedPassword")) {
                    @{
                        Right = 'LAPS'
                        ACEs            = $Settings['DACLs']['LAPS']
                    }
                }
                else {
                    write-loghandler -level "warning" -message "ms-LAPS-EncryptedPassword schema object is missing: you may need to update-lapsADSchema"
                }
            )
        }
    )
)
$ComponentTemplate.add(
    "GPOs", @(
        @{
            Metadata = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixHigh']
                Comment       = @"
#########################################################################
`tDO NOT EDIT THIS GPO -- CHANGES WILL BE OVERWRITTEN

`tThis GPO is automatically generated by 'add-rbacComponent'.
`tMake desired edits in the Settings GPO
#########################################################################

Applies basic host-based access control
`t* Adds various "right-global-*" rights to corresponding User Rights Assignments
`t* Adds rights (e.g. WindowsAdmin) to corresponding local groups via restricted groups
`t* Configures LAPS and sets LAPS principal to the right-global-laps... group
"@
                AlwaysRebuild = $true
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @()
                        Principals = @()
                    }
                }
            }
            SecEdit  = @{
                "Privilege Rights" = @{
                    SeTcbPrivilege              = @{
                        Rights = @(
                            'SeTcbPrivilege'
                        )
                    }
                    SeAuditPrivilege              = @{
                        SIDS       = @(
                            $SID_NetworkService
                            $SID_ALLSERVICES
                        )
                        Rights = @(
                            'GenerateSecurityAudit'
                        )
                    }
                    SeDenyRemoteInteractiveLogonRight = @{
                        Rights     = @(
                            'DenyRemote'
                        )
                    }
                    SeDenyServiceLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeDenyBatchLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeInteractiveLogonRight       = @{
                        SIDS       = @(
                            $SID_Administrators
                        )
                        Rights     = @(
                            'LogonLocal'
                        )
                        Principals = @()
                    }
                    SeRemoteInteractiveLogonRight = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_RemoteDesktop
                        )
                        Rights     = @(
                            'LogonRemote'
                        )
                        Principals = @()
                    }
                    SeServiceLogonRight           = @{
                        SIDS       = @(
                            $SID_NetworkService
                            $SID_ALLSERVICES
                        )
                        Rights     = @(
                            'LogonService'
                        )
                        Principals = @()
                    }
                    SeBatchLogonRight             = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_BackupOperators
                            $SID_PerfLogUsers
                        )
                        Rights     = @(
                            'LogonBatch'
                        )
                        Principals = @()
                    }
                }
                "Group Membership" = @{
                    "*$($SID_Administrators)"         = @{
                        Members = @{
                            SIDS       = @(
                            )
                            Rights     = @(
                                'WinAdmin'
                            )
                            Principals = @()
                        }
                    }
                    "*$($SID_NetworkConfigOperators)" = @{
                        Members = @{
                            SIDS   = @()
                            Rights = @(
                                'WinOps'
                            )
                        }
                    }
                    "*$($SID_PerfLogUsers)"           = @{
                        Members = @{
                            SIDS       = @()
                            Rights     = @(
                                'WinOps'
                            )
                            Principals = @()
                        }
                    }
                    "*$($SID_PerfMonUsers)"           = @{
                        Members = @{
                            SIDS       = @()
                            Rights     = @(
                                'WinOps'
                            )
                            Principals = @()
                        }
                    }
                    "*$($SID_EventLogUsers)"          = @{
                        Members = @{
                            SIDS       = @(
                                # Needed for Security log access-- many services (splunk, ADFS?) want this
                                $SID_ALLSERVICES
                            )
                            Rights     = @(
                                'WinOps'
                            )
                            Principals = @()
                        }
                    }
                    "*$($SID_RemoteMgtUsers)"         = @{
                        Members = @{
                            SIDS       = @()
                            Rights     = @(
                                'LogonRemote'
                            )
                            Principals = @()
                        }
                    }
                    "*$($SID_RemoteDesktop)"          = @{
                        Members = @{
                            SIDS       = @()
                            Rights     = @(
                                'LogonRemote'
                            )
                            Principals = @()
                        }
                    }
                }
            }
            RegPol   = @(
                @{
                    KeyName         = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName       = "ADPasswordEncryptionPrincipal"
                    ValueType       = "REG_SZ"
                    ValueCollection = @{
                        SIDs       = @()
                        Rights     = @(
                            'LAPS'
                        )
                        Principals = @()
                    }
                }
            )
        }

        @{
            Metadata = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixLow']
                AlwaysRebuild = $False
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @(
                            'GPOEdit'
                        )
                        Principals = @()
                    }
                }
            }
        }
    )
)