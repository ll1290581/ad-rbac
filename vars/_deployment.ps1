$SID_Users = "S-1-5-32-545"
$SID_Administrators = "S-1-5-32-544"
$SID_Guests = "S-1-5-32-546"
$SID_BackupOperators ="S-1-5-32-551"
$SID_RemoteDesktop ="S-1-5-32-555"
$SID_AuthenticatedUsers = "S-1-5-11"
$SID_LocalAccount = "S-1-5-113"
$SID_LocalAccountAndAdmin = "S-1-5-114"
$SID_Guests = "S-1-5-32-546"
$SID_NetworkService = "S-1-5-20"
$SID_ALLSERVICES = "S-1-5-80-0"
$SID_NetworkConfigOperators = "S-1-5-32-556"
$SID_PerfLogUsers = "S-1-5-32-559"
$SID_PerfMonUsers = "S-1-5-32-558"
$SID_EventLogUsers = "S-1-5-32-573"
$SID_RemoteMgtUsers = "S-1-5-32-580"
$SID_DomainPrefix = ($SID.domainSid -split "-")[0..5] -join "-"
$SID_DomainAdmin = "$SID_DomainPrefix-512"
$SID_EnterpriseAdmin = "$SID_DomainPrefix-519"
$SID_SchemaAdmin = "$SID_DomainPrefix-518"

$OrganizationConfig = [ordered]@{
    GlobalOUName = $GlobalOrgName
    LinuxFeaturesOUName = "LinuxFeatures"
    NetgroupsOUName = "Netgroups"
    SudorolesOUName = "sudoroles"
    OrgsOUName = "Orgs"
    OUList = @(
        @{
            Org="Infrastructure"
            Description="Core Production Infrastructure"
            Components=@(
                @{
                    Component="VMWare"
                    Description="ESXi and vCenter"
                }
                @{
                    Component="PKI"
                    Description="Certificate Infrastructure"
                }
                @{
                    Component = "ServiceNow"
                    Description = "ServiceNow ITSM and ticketing"
                }
                @{
                    Component = "JumpBoxes"
                    Description = "Bastion Hosts"
                }
                @{
                    Component = "IPManagement"
                    Description = "IPAM and DHCP"
                }
                @{
                    Component = "Updates"
                    Description = "Avanti, WSUS, and Satellite"
                }
            )
        }
        @{
            Org="InformationAssurance"
            Description="Compliance, Security, and Monitoring"
            Components=@(
                @{
                    Component="Splunk"
                    Description="Splunk monitoring stack"
                }
                @{
                    Component="SecurityScans"
                    Description="Nessus and similar"
                }
            )
        }
        @{
            Org="Network"
            Description="Network management"
            Components=@(
                @{
                    Component="Hardware"
                    Description="Routers and Switches"
                }
            )
        }
        @{
            org = "DevOps"
            Description = "Software Engineering and Development"
            Components = @(
                @{
                    Component = "Jira"
                    Description = "Jira project planning system"
                }
                @{
                    Component = "Confluence"
                    Description = "Fill Me in"
                }
            )
        }
        @{
            Org = "Ops"
            Description = "Program Operations and Administration"
            Components = @(
                @{
                    Component = "CMS"
                    Description = "CMS"
                }
                @{
                    Component = "Desktops"
                    Description = "End user desktops including VDI"
                }
            )
        }
    )
}