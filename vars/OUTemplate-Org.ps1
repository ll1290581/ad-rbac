$OrgTemplate = @{}
$OrgTemplate.add(
    "sudoRoles", @(
        'LinuxSudoFull'
        'LinuxSudoFullNoAuth'
        'LinuxSudoOperate'
        'LinuxSudoConfigure'
    )
)
$OrgTemplate.Add(
    "LDAPContainers", @(
        @{
            name        = $settings['names']['RightsOU']
            Type        = "Container"
            Description = "Groups granting privileges for GLOBAL aspects of this org, such as GPO linking or OU management. Users should not be members of these groups. Only other groups should be members of 'rights' groups."
        }
        @{
            name        = $settings['names']['RolesOU']
            Type        = "Container"
            description = "Roles associated with multiple 'rights'. Only users should be members of 'role' groups."
        }
        @{
            Name        = $Settings['Names']['ComponentsOU']
            Description = "(Delegation,GPO) A complete application, project, or app stack with a common lifecycle and administrative ownership"
        }
    )
)
$OrgTemplate.Add(
    "DefaultRoles", @(
        @{
            nameSuffix      = $Settings['names']['RolesList']['Owner']
            Owner           = $true
            Description     = "Owner of this Org. Assumed to have full administrative rights on all systems in child components."
            Rights          = @(
                'DenyService'
                'AppAccess'
                'AppPoweruser'
                'AppAdmin'
                'CreateDeleteComputer'
                'WinAdmin'
                'LogonLocal'
                'LogonRemote'
                'GPOAudit'
                'GPOEdit'
                'GPOLink'
                'LAPS'
                'ManageRights'
                'ManageRoles'
                'ServiceAccountMSA'
                'ServiceAccountLegacy'
                'LinuxSudoFull'
                'OUCreate'
                'OUManage'
            )
            AuxiliaryGroups = @(
                #TODO: This works due to mock, but is still a bit ugly
                $(get-rbacGlobal -detailed -mock).Rights['GPOCreate'].name
            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['Operator']
            Owner       = $False
            Description = "Elevated rights for OS and application. Note: Does not have App Admin"
            Rights      = @(
                'DenyService'
                'AppAccess'
                'AppPoweruser'
                # No app Admin at org level
                'CreateDeleteComputer'
                'GPOAudit'
                'GPOEdit'
                'LAPS'
                'LogonLocal'
                'LogonRemote'
                'WinAdmin'
                'LinuxSudoOperate'
                'LinuxSudoConfigure'
                'ServiceAccountMSA'
            )
        }
        @{
            nameSuffix      = $Settings['names']['RolesList']['SvcRemoteAdmin']
            Protected       = $false
            Owner           = $False
            Description     = "Local admin rights. Sudo without password. Suitable for remote Scan services like Nessus."
            Rights          = @(
                'WinAdmin'
                'LinuxSudoFullNoAuth'
                'LogonRemote'
                'DenyService'
            )
        }
        @{
            nameSuffix  = $Settings['names']['RolesList']['User']
            Owner       = $false
            Description = "Act as user for applications under org"
            Rights      = @(
                'DenyService'
                'AppAccess'
                'AppPoweruser'
                'LogonLocal'
                'LogonRemote'
            )
        }
    )
)
$OrgTemplate.Add(
    "DefaultRights", @(
        'AppAccess'
        'AppPoweruser'
        'AppAdmin'
        'CreateDeleteComputer'
        'GPOAudit'
        'GPOLink'
        'GPOEdit'
        'OUCreate'
        'OUManage'
        'ManageRights'
        'ManageRoles'
        'LAPS'
        'WinAdmin'
        'WinOps'
        'ServiceAccountMSA'
        'ServiceAccountLegacy'
        foreach ($role in $orgTemplate['sudoRoles']){
            $role
        }
        'LogonLocal'
        'LogonRemote'
        'LogonBatch'
        'LogonService'
        'DenyRemote'
        'DenyService'
    )
)

$OrgTemplate.Add(
    "OUDelegations", @(
        # ""
        @{
            ADPathLeafOU = ""
            ApplyDefaultDeny = $true
            DefaultDenyInheritance = "None"
            ACLs         = @(
                @{
                    Right = 'GPOAudit'
                    ACEs            = $Settings['DACLs']['GPOAudit']
                }
                @{
                    Right = 'GPOLink'
                    ACEs            = $Settings['DACLs']['GPOLink']
                }
                @{
                    Right = 'GPOEdit'
                    ACEs            = $Settings['DACLs']['GPOEdit']
                }
                if ((test-rBACFeatures).LAPS) {
                    write-loghandler -level "warning" -message "Enabling LAPS features"
                    @{
                        Right = 'LAPS'
                        ACEs            = $Settings['DACLs']['LAPS']
                    }
                }
                else {
                    write-loghandler -level "warning" -message "ms-LAPS-EncryptedPassword schema object is missing: you may need to update-lapsADSchema"
                }
            )
        }
        @{
            ADPathLeafOU = "OU={0}" -f $Settings['Names']['ComponentsOU']
            ApplyDefaultDeny = $true
            DefaultDenyInheritance = "None"
            ACLs         = @(
                @{
                    Right = 'CreateDeleteComputer'
                    ACEs            = $Settings['DACLs']['AddComputer']
                }
                @{
                    Right = 'OUCreate'
                    ACEs            = $Settings['DACLs']['OUCreate']
                }
                @{
                    Right = 'OUManage'
                    ACEs            = $Settings['DACLs']['OUManage']
                }
                @{
                    Right = 'ManageRights'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
                @{
                    Right = 'ServiceAccountMSA'
                    ACEs            = $Settings['DACLs']['ServiceAccountMSA']
                }
                @{
                    Right = 'ServiceAccountLegacy'
                    ACEs            = $Settings['DACLs']['ServiceAccountLegacy']
                }
                @{
                    Principal = "Everyone"
                    ACEs      = @(
                        @{
                            ADRight         = "CreateChild"
                            TargetObject    = "Computer"
                            Action          = "Deny"
                            InheritanceType = "none"
                        }
                    )
                }
            )

        }
        @{
            ADPathLeafOU = "OU={0}" -f $Settings['Names']['RightsOU']
            ApplyDefaultDeny = $true
            DefaultDenyInheritance = "None"
            ACLs         = @(
                @{
                    Right = 'ManageRights'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
            )

        }
        @{
            ADPathLeafOU = "OU={0}" -f $Settings['Names']['RolesOU']
            ApplyDefaultDeny = $true
            DefaultDenyInheritance = "None"
            ACLs         = @(
                @{
                    Right = 'ManageRoles'
                    ACEs            = $Settings['DACLs']['GroupControl']
                }
            )
        }
    )
)
$OrgTemplate.Add(
    "GPOs", @(
        @{
            Metadata = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixHigh']
                Comment       = @"
#########################################################################
`tDO NOT EDIT THIS GPO -- CHANGES WILL BE OVERWRITTEN

`tThis GPO is automatically generated by 'add-rbacOrg'.
`tMake desired edits in the Settings GPO
#########################################################################

Applies basic host-based access control
`t* Adds various "right-global-*" rights to corresponding User Rights Assignments
`t* Adds rights (e.g. WindowsAdmin) to corresponding local groups via restricted groups
`t* Configures LAPS and sets LAPS principal to the right-global-laps... group
"@
                AlwaysRebuild = $true
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @()
                        Principals = @()
                    }
                }
            }
            SecEdit  = @{
                "Privilege Rights" = @{
                    SeInteractiveLogonRight       = @{
                        SIDS       = @(
                            $SID_Administrators
                        )
                        Rights     = @(
                            'LogonLocal'
                        )
                        Principals = @()
                    }
                    SeRemoteInteractiveLogonRight = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_RemoteDesktop
                        )
                        Rights     = @(
                            'LogonRemote'
                        )
                        Principals = @()
                    }
                    SeDenyRemoteInteractiveLogonRight = @{
                        Rights     = @(
                            'DenyRemote'
                        )
                    }
                    SeDenyServiceLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeDenyBatchLogonRight = @{
                        Rights     = @(
                            'DenyService'
                        )
                    }
                    SeServiceLogonRight           = @{
                        SIDS       = @(
                            $SID_NetworkService
                            $SID_ALLSERVICES
                        )
                        Rights     = @(
                            'LogonService'
                        )
                        Principals = @()
                    }
                    SeBatchLogonRight             = @{
                        SIDS       = @(
                            $SID_Administrators
                            $SID_BackupOperators
                            $SID_PerfLogUsers
                        )
                        Rights     = @(
                            'LogonBatch'
                        )
                        Principals = @()
                    }
                }
                "Group Membership" = @{
                    "*$($SID_ALLSERVICES)"          = @{
                        MemberOf = @{
                            SIDS       = @(
                                # Needed for Security log access-- many services (splunk, ADFS?) want this
                                # This seems to get overridden by component-level GPO so we apply it there as well.
                                $SID_EventLogUsers
                            )
                            Rights     = @()
                            Principals = @()
                        }
                    }
                    'WinAdmin' = @{
                        ResolveKeyName = $true
                        MemberOf       = @{
                            SIDS = @($SID_Administrators)
                        }
                    }
                    'WinOps'   = @{
                        ResolveKeyName = $True
                        MemberOf       = @{
                            SIDS = @(
                                $SID_NetworkConfigOperators
                                $SID_PerfLogUsers
                                $SID_PerfMonUsers
                                $SID_EventLogUsers
                            )
                        }
                    }
                    'LogonRemote'  = @{
                        ResolveKeyName = $true
                        MemberOf       = @{
                            SIDS = @(
                                $SID_RemoteMgtUsers
                                $SID_RemoteDesktop
                            )
                        }
                    }
                }
            }
            RegPol   = @(
                @{
                    KeyName         = "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\LAPS"
                    ValueName       = "ADPasswordEncryptionPrincipal"
                    ValueType       = "REG_SZ"
                    ValueCollection = @{
                        SIDs       = @()
                        Rights     = @(
                            'LAPS'
                        )
                        Principals = @()
                    }
                }
            )
        }

        @{
            Metadata = @{
                LinkOrder     = 1
                NamePrefix    = $Settings['names']['GPOs']['PrefixLow']

                AlwaysRebuild = $False
                GPPermissions = @{
                    GPOEdit = @{
                        SIDs       = @()
                        Rights     = @(
                            'GPOEdit'
                        )
                        Principals = @()
                    }
                }
            }
        }
    )
)