@echo off
setlocal ENABLEDELAYEDEXPANSION
set user=%userdomain%\%username%
set launchdir="%~dp0"
call :detect_ps_versions
set ps_cur_index=0
goto :menu


:menu
cls
Echo.
Echo #################################################
echo  Logged on as:        %user%
echo  Powershell Versions:
echo.
for /L %%A in (0 1 %ps_version_num%) do (
	if DEFINED psVersions[%%A].Name (
		if %%A EQU %ps_cur_index% (
			call set executable=%%psVersions[%%A].exe%%
			call set name=%%psVersions[%%A].Name%%
			set sel=**SELECTED**
		) else (
			set sel=
		)
		call echo     !sel!      %%psVersions[%%A].Name%%
	)
)
echo.
Echo #################################################

echo.
echo 1 - Launch Management Shell  (%user% @ %name%)
echo 2 - Open powershell          (%user% @ %name%)
echo 3 - Attempt to update shell  (will ask for credentials)
echo 4 - Relaunch as another user
echo 5 - Switch Powershell Version
echo 6 - exit
echo.
echo.
set /P M=Choose an option and then press ENTER:
if %M%==1 goto :start_shell
if %M%==2 goto :ps_launch
if %M%==3 goto :update
if %M%==4 goto :runas
if %M%==5 goto :switchVersion
if %M%==6 goto :eof
goto :menu


:detect_ps_versions
set ps_version_num=0

set executable=pwsh.exe
call :try_ps_version


set executable=powershell.exe
call :try_ps_version
if NOT %ps_version_num% GEQ 1 (
	echo.
	echo #######################  ERROR ############################
	echo.
	echo Something might be wrong with your install, powershell isn't found. Cannot continue.
	pause
	exit
)
goto :eof


:try_ps_version
for /F "tokens=*" %%g in ('%executable% -command "$psversiontable.psversion.toString()"') do (
	SET psVersions[%ps_version_num%].Name=Powershell %%g
	SET psVersions[%ps_version_num%].exe=%executable%
	call echo Detected: %%psVersions[%ps_version_num%].Name%%
	set /A "ps_version_num+=1"
)
goto :eof

:ps_launch
set baseparams=-executionpolicy bypass -noexit -noprofile -nologo
echo %executable%
%executable% %baseparams% %params%
pause
goto :eof

:start_shell
if %executable%=="powershell.exe" echo "Powershell 7 not found, but you should really be using it. Will continue in compatibility mode"
set params=-command "clear; write-host 'Please wait, shell is starting up....'; write-host 'Loading modules from: %~dp0Modules\';foreach ($module in $(gci %~dp0Modules\)) {write-progress -activity 'Loading Modules' -status $module.name; import-module $module.fullname -passthru -disablenamechecking ; write-progress -activity 'Loading Modules' -completed}; write-host ''; get-started;"
goto :ps_launch

:update
set params=-command "new-psdrive -Name shell -PSProvider FileSystem -root %launchdir% -Credential (get-credential); cd shell:\Modules\ad-rbac; git pull; if ($LASTEXITCODE -eq 128) {write-warning 'You may need to paste that command into the shell, and then re-run: git pull'}"
goto :ps_launch

:runas
echo.
set /P newuser=Please enter username in NetBios format (%User%):
runas /user:%newuser% %~dpnx0
goto :eof

:switchVersion
set /A "ps_cur_index+=1"
if %ps_cur_index% GEQ %ps_version_num% (
	set ps_cur_index=0
)
goto :menu