@echo off
pwsh.exe -v && goto :ps7
powershell.exe -h && goto :ps5
echo "Something might be wrong with your install, powershell isn't found"
pause
goto :eof

:ps7
pwsh.exe -executionpolicy bypass -noexit -noprofile -nologo -command "clear; write-host 'Please wait, shell is starting up....'; write-host 'Loading modules from: %~dp0Modules\';foreach ($module in $(gci %~dp0Modules\)) {write-progress -activity 'Loading Modules' -status $module.name; import-module $module.fullname -passthru -disablenamechecking ; write-progress -activity 'Loading Modules' -completed}; write-host ''; get-started;"
goto :eof

:ps5
cls
echo "Powershell 7 not found, but you should really be using it. Will continue in compatibility mode"
pause
powershell.exe -executionpolicy bypass -noexit -noprofile -nologo -command "clear; write-host 'Please wait, shell is starting up....'; write-host 'Loading modules from: %~dp0Modules\';foreach ($module in $(gci %~dp0Modules\)) {write-progress -activity 'Loading Modules' -status $module.name; import-module $module.fullname -passthru -disablenamechecking ; write-progress -activity 'Loading Modules' -completed}; write-host ''; get-started;"
pause
goto :eof

