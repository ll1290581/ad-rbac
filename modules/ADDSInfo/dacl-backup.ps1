﻿import-module ./
$ds = get-dsinfo
$DomainSID = $(
    [System.Security.Principal.SecurityIdentifier]::new(
        [byte[]](get-dsinfo -infoset Domain -outFormat Hashtable)["objectSid"],
        [int]0
    )
).value
$SDDLMaplist = import-csv .\sddl_map.csv | foreach-object {
    $_.RIDValue = "$DomainSID-$($_.RIDValue)"
    $_
}
$DACLList = [ordered]@{
    NamingContexts = [ordered]@{}
    ntSecurityDescriptors = [ordered]@{}
}
$NamingContextList = @(
    "configurationNamingContext"
    "defaultNamingContext"
    "schemaNamingContext"
)
foreach ($namingContext in $namingContextList) {
    write-warning "starting $namingContext"
    $DACLList['NamingContexts'].add($namingContext,[System.Collections.ArrayList]::new())
    $results = [adsisearcher]::new(
            [adsi]"LDAP://$($ds.$namingContext)",
            "(!(|(objectClass=dnsZone)(objectClass=dnsNode)(objectClass=computer)))",
            @("ntSecurityDescriptor","objectClass","defaultSecurityDescriptor")
    ).findAll() | foreach-object {
        if ($_.path -like "*Domain1*") {
            write-warning "Skipping $($_.path)"

        } else {
            $thisObject = $_.GetDirectoryEntry()
            @{
                DN = $thisObject.DistinguishedName.value
                SDDLString = $thisObject.objectSecurity.sddl
                objectClass = $thisobject.objectclass[-1]
                Parent = $(try { if ([string]::IsNullOrEmpty($thisobject.parent)){""} else {$thisobject.parent}} catch {""})
                DefaultSecurityDescriptor = $(
                    if (-not [string]::IsNullOrEmpty($thisobject.defaultSecurityDescriptor)) {
                        $thisObject.defaultSecurityDescriptor
                    } else {
                        ""
                    }
                )
            }
        }
    }
#    $results | group-object objectClass | sort-object count -descending | foreach-object {
#        $DACLList['NamingContexts'][$namingContext].add($_.name,[ordered]@{})
#    }
    foreach ($DefaultSD in $($results.getEnumerator() | group-object DefaultSecurityDescriptor | sort-object Count -Descending)) {
        if (-not [string]::IsNullOrWhiteSpace($defaultSD.name)) {
            $DefaultSDHash = $(
                Get-FileHash -Algorithm sha256 -InputStream $(
                    [IO.MemoryStream]::new(
                        [byte[]][char[]]$(
                            $DefaultSD.name
                        )
                    )
                )
            ).Hash.substring(0,8)
            write-warning $defaultSDHash
            if ($DACLList['ntSecurityDescriptors'].contains($DefaultSDHash)) {
                write-warning "FoundMatch"
                $fixedDefaultSD = $DACLList['ntSecurityDescriptors'][$defaultSDHash]
            } else {
                $FixeddefaultSD = [System.Text.StringBuilder]::new($_.defaultSecurityDescriptor)
                if ($_.defaultSecurityDescriptor -like "*$DomainSID*"){
                    foreach ($mapping in $SDDLMapList) {
                        if ($_.defaultSecurityDescriptor -like "*$($mapping.ridvalue)*") {
                            write-warning "Match on $($mapping.ridvalue)"
                            [void]$FixeddefaultSD.replace($mapping.ridValue,$mapping.SDDLString)
                        }
                    }
                    [void]$FixeddefaultSD.replace($DomainSID,"§DOMAIN_SID§")
                }
                $DACLList['ntSecurityDescriptors'].add($defaultSDHash,$fixedDefaultSD.toString())
            }
        } else {
            $DefaultSDHash = $null
        }
        foreach ($SDDLGroup in $($DefaultSD.group | group-object SDDLString | sort-object Count -Descending)) {
            $SDDLHash = $(
                Get-FileHash -Algorithm sha256 -InputStream $(
                    [IO.MemoryStream]::new(
                        [byte[]][char[]]$(
                            $SDDLGroup.name
                        )
                    )
                )
            ).Hash.substring(0,8)

            $FixedSDDL = [System.Text.StringBuilder]::new($SDDLGroup.name)
            if ($DACLList['ntSecurityDescriptors'].contains($SDDLHash)) {
                write-warning "FoundMatch (SDDL)"
                $fixedSDDL = $DACLList['ntSecurityDescriptors'][$SDDLHash]
            } else {
                if ($SDDLGroup.name -like "*$DomainSID*"){
                    foreach ($mapping in $SDDLMapList) {
                        if ($SDDLGroup.name -like "*$($mapping.ridvalue)*") {
                            [void]$FixedSDDL.replace($mapping.ridValue,$mapping.SDDLString)
                        }
                    }
                    [void]$FixedSDDL.replace($DomainSID,"§DOMAIN_SID§")
                }
                $DACLList['ntSecurityDescriptors'].add($SDDLHash,$FixedSDDL.toString())
            }
            foreach ($objectClass in $($SDDLGroup.group | group-object objectClass ) ) {
                $objectClass.group | sort-object parent | foreach-object {
                    $object = [ordered]@{
                        DN = $_.DN
                        objectClass = $_.objectClass
                        SDDLHash = $SDDLHash
                    }
                    if ($null -ne $defaultSDHash) {
                        $object.set_item("DefaultSDHash", $DefaultSDHash)
                    }
                    [void]$DACLList['NamingContexts'][$namingContext].add($Object)

                }
            }
       }
    }
}
