﻿function Invoke-COMObject{
    [CmdletBinding()]
    param (
        [Parameter(mandatory)]
        $COMObject,

        # Invokes a method instead of getting / setting
        [switch]
        $Method,

        # Property to resolve
        [Parameter(mandatory)]
        [string]
        $Property,

        # Value to set on property / method
        [Parameter()]
        [string]
        $Value
    )

    # These are sometimes other types that cant be cast, like 'system.directoryservices.propertyValueCollection'
    if ($comobject -isnot "__ComObject") {

    }
    if ($method) {
        $invoke = "InvokeMethod"
    } elseIf ($myInvocation.BoundParameters.containsKey("Value")) {
        $invoke = "SetProperty"
    } else {
        $invoke = "GetProperty"
    }
    [System.__ComObject].InvokeMember($property, $invoke, $null, $COMObject, $value)
}

function Get-DSInfo {
    <#
    .SYNOPSIS
        Fetches information about AD Domain using ADSI
    .DESCRIPTION
        Functions similar to get-addomain, get-adrootDSE etc, but without relying on ActiveDirectory module and is thus much lighter
    .NOTES
        This may not function on system without [ADSI] type / interface.
    .LINK
        Specify a URI to a help page, this will show when Get-Help -Online is used.
    .EXAMPLE
        Get-DSInfo -infoset Domain | select-object name, distinguishedName

        Returns a DirectoryEntry for the Domain, listing some attributes
    .EXAMPLE
        Get-DSInfo -infoset RootDSE | select-object rootDomainNamingContext,DomainFunctionality,forestFunctionality

        Returns a DirectoryEntry for the RootDSE and shows the domain / forest functional level
    .OUTPUTS
        System.DirectoryServices.DirectoryEntry  (outFormat ADSI)
        Hashtable                                (outFormat Hashtable)
        PSCustomObject                           (outFormat PSCustomObject)
    #>


    [CmdletBinding()]
    param (
        # Info to return
        [ValidateSet('RootDSE', 'Domain')]
        [String]
        $infoSet = 'RootDSE',

        # Format to output
        [ValidateSet('ADSI', 'PSCustomObject', 'Hashtable')]
        [string]
        $outFormat = "PSCustomObject"
    )

    $output = switch ($infoset) {
        'RootDSE' {
            [ADSI]"LDAP://RootDSE"
            break
        }
        'Domain' {
            $searcher = [adsisearcher]'(objectclass=domain)'
            $searcher.searchScope = 'base'
            $searcher.FindOne() | foreach-object {$_.psbase.getDirectoryEntry()}
            $searcher.FindOne() | foreach-object {$_.psbase.getDirectoryEntry()}
            break
        }
        Default { break}
    }
    if ($outFormat -eq 'ADSI') {
        return $output
    } else {
        $hashOutput = [hashtable]::new()
        foreach ($attribute in $($output.properties.getEnumerator())) {
            if ($null -ne $attribute.key) {
                $hashOutput.add($attribute.key, $attribute.value)
            } elseif ($null -ne $attribute.PropertyName){
                $hashOutput.add($attribute.propertyName, $attribute.value)
            } else {
                throw "Not sure what this output is...."
            }

        }
        if ($outFormat -eq "Hashtable") {
            return $hashOutput
        } else {
            return [PSCustomObject]$hashOutput
        }
    }
}

function resolve-dsGroupType {
    [CmdletBinding()]
    param (
        [Parameter()]
        [int]
        $groupType
    )
    BEGIN {
        $groupTypes = @{
            0x80000002 = "Global"
            0x80000004 = "DomainLocal"
            0x80000008 = "Universal"
        }
    }
    Process {
        $GroupTypes[$groupType]
    }

}

function resolve-dsSIDFromName {
    [CmdletBinding()]
    param (
        [Parameter()]
        [String]
        $objectName
    )
    $NTPrincipal   = [System.Security.Principal.NTAccount]::new($objectName)
    $NTPrincipal.translate([System.Security.Principal.SecurityIdentifier]).value
}

function resolve-dsNameFromSID {
    [CmdletBinding()]
    param (
        [Parameter()]
        [System.Security.Principal.SecurityIdentifier]$objectSID
    )
    $objectSID.Translate([System.Security.Principal.NTAccount]).value
}



function get-DSObject {
    <#
        .SYNOPSIS
            Fetches an AD Object using the ADSI interface
        .DESCRIPTION
            This functions similarly to get-ADObject, with the following differences:
                * It does not require the ActiveDirectory module or RSAT
                * It is substantially faster
                * It returns an ADSI DirectoryEntry that can be manipulated with put / putex / setinfo()
        .NOTES
            This may not be supported on Linux, as it requires the [adsi] interface which may not be present in Core.
        .LINK
            Specify a URI to a help page, this will show when Get-Help -Online is used.
        .EXAMPLE
            Get-DSObject -objectClass "computer" -name "myComputer"
                distinguishedName : {CN=test,CN=Computers,DC=lab,DC=local}
                Path              : LDAP://CN=test,CN=Computers,DC=lab,DC=local
        .EXAMPLE
            $myComputer = Get-DSObject -objectClass "computer" -name "myComputer"
            $myComputer.put('operatingSystem', "vCenter")
            $myComputer.put('operatingSystemVersion', "7.1")
            $myComputer.setInfo()

            Grab the directory object for a computer, and set its OS and OSVersion.

        .OUTPUTS
            [System.DirectoryServices.DirectoryEntry]
    #>
    [CmdletBinding(DefaultParameterSetName="LDAPFilter")]
    param (
        # LDAP Filter for searching
        [Parameter(parameterSetName="DistinguishedName", mandatory)]
        [string]
        $DistinguishedName,

        # LDAP Filter for searching
        [Parameter(parameterSetName="LDAPFilter", mandatory)]
        [string]
        $LDAPFilter,

        # Object class to search for
        [Parameter(parameterSetName="autofilter", mandatory)]
        [ArgumentCompleter({
            [OutputType([System.Management.Automation.CompletionResult])]
            param(
                [string] $CommandName,
                [string] $ParameterName,
                [string] $WordToComplete,
                [System.Management.Automation.Language.CommandAst] $CommandAst,
                [System.Collections.IDictionary] $FakeBoundParameters
            )
            $(Get-DSSchemaClass -objectName "$wordToComplete*").ldapdisplayname
        })]
        [string]
        $objectClass,

        # name of object
        [Parameter(parameterSetName="autofilter", mandatory)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Name,

        # SearchBase under which search occurs
        [Parameter(parameterSetName="autofilter")]
        [Parameter(parameterSetName="LDAPFilter")]
        [String]
        $Searchbase = ([ADSI]"LDAP://RootDSE").defaultNamingContext,

        # Properties to fetch
        [string[]]
        $Properties,

        # Search Scope
        [Parameter(parameterSetName="autofilter")]
        [Parameter(parameterSetName="LDAPFilter")]
        [System.DirectoryServices.SearchScope]
        $SearchScope,

        # Specific Server to query
        [Parameter()]
        [String]$Server

    )
    begin {
    }
    Process {
        if ($DistinguishedName) {
            if ($server) {
                $object = [adsi]"LDAP://$server/$distinguishedName"
            } else {
                $object = [adsi]"LDAP://$distinguishedName"
            }
            if ($properties) {
                $object.getInfoEx(@($properties),0)
            } else {
                $object.getInfo()
            }
            return $object
        } else {
            if (-not $LDAPFilter) {
                $LDAPFilter = "(&(objectClass=$objectClass)(name=$name))"
            }
            if ($server) {
                $URI = "LDAP://$server/$searchBase"
            } else {
                $URI = "LDAP://$searchBase"
            }
            $searcher = if ($properties) {
                [adsisearcher]::new(
                    [ADSI]"$URI",
                    $LDAPFilter,
                    $Properties
                )
            } else {
                [adsisearcher]::new(
                    [ADSI]"$URI",
                    $LDAPFilter
                )
            }
            if ($searchScope) {
                $searcher.searchScope = $searchScope
            }
            $searcher.findAll() | foreach-object {
                if ($null -ne $_) {
                    $_.getDirectoryEntry()
                }
            }
        }
    }
}

function Set-DSObject {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        # CN or SAMAccountName of object
        [Parameter(mandatory)]
        [String]
        $Name,

        # objectClass to create
        [Parameter(Mandatory)]
        [ArgumentCompleter({
            [OutputType([System.Management.Automation.CompletionResult])]
            param(
                [string] $CommandName,
                [string] $ParameterName,
                [string] $WordToComplete,
                [System.Management.Automation.Language.CommandAst] $CommandAst,
                [System.Collections.IDictionary] $FakeBoundParameters
            )
            $(Get-DSSchemaClass -objectName "$wordToComplete*").ldapdisplayname
        })]
        [String]
        $objectClass,

        # Parent path where object will be created
        [Parameter()]
        [String]
        $Path,

        # Output as a pscustomobject with easy identification of updated Attributes
        [Parameter()]
        [switch]
        $PrettyOutput,

        # hashtable of attributes to set on creation
        [Parameter(ParameterSetName="Attributes", mandatory)]
        [hashtable]
        $Attributes,

        # Update attributes even if object already exists.
        [Parameter(ParameterSetName="Attributes")]
        [switch]
        $ForceUpdate

    )
    Begin {
        $WellknownGUIDs = @{
            'computer' = 'AA312825768811D1ADED00C04FD8D5CD'
            'user' = 'A9D1CA15768811D1ADED00C04FD8D5CD'
            'msDS-ManagedServiceAccount' = '1EB93889E40C45DF9F0C64D23BBB6237'
            'ms-DS-Group-Managed-Service-Account' = '1EB93889E40C45DF9F0C64D23BBB6237'
        }
        $colorgood = $psstyle.foreground.FromConsoleColor("Green")
        $colorupd = $psstyle.foreground.FromConsoleColor("Yellow")
        $colorerr = $psstyle.foreground.FromConsoleColor("Red")
        $colorpend = $psstyle.foreground.FromConsoleColor("cyan")
    }

    Process{
        trap {
            $ThisStatus.Status = "$($colorErr)Error"
            $ThisStatus
            throw $_
        }

        $thisStatus = [ordered]@{
            Status = "$($colorpend)Pending"
            Name = $Name
        }
        if (-not $path) {
            if ($objectClass -in $WellknownGUIDs.keys) {
                $ParentPath = "LDAP://<WKGUID=$($WellknownGUIDs[$objectClass]),$(([ADSI]"LDAP://RootDSE").defaultNamingContext)>"
                if ([adsi]::Exists($parentPath)) {
                    $parent = [adsi]"LDAP://$(([adsi]$parentPath).distinguishedName)"
                    Write-verbose "Using default location for '$objectClass'`: $($parent.distinguishedName)"
                } else {
                    throw "Missing well-known path, you should really get that checked out."
                }
            } else {
                throw "You must provide a parent path for $objectClass object types."
            }
        } else {
            $parentPath = "LDAP://$path"
            if ([adsi]::Exists($parentPath)) {
                $parent = [ADSI]$parentPath
            } else {
                write-warning "Parent object does not exist: $parentpath"
                throw "Missing Parent"
            }
        }
        $thisStatus.add("Path",$parent.distinguishedName.value)

        $objectLDAPName = switch ($objectClass) {
            'user' {"CN=$name"; break}
            'computer' {"CN=$name"; break}
            'organizationalUnit' {"OU=$name"; break}
            default {"CN=$name"; break}
        }
        $objectFullPath = "LDAP://$objectLDAPName,$($parent.distinguishedName)"
        $needsUpdate = $false
        if ([adsi]::exists($objectFullPath)) {
            if (-not $forceUpdate) {
                write-warning "Object already exists. If you want to update its attributes, use the 'forceUpdate' switch."
                throw "The Object already exists: $objectFullPath"
            } else {
                $targetObject = [adsi]$objectFullPath
                $thisStatus.Status = "$($colorpend)Updating"
                $targetObject.getInfo()
            }
        } else {
            $isnew = $true
            $thisStatus.Status = "$($colorpend)Creating"
            $targetObject = $parent.create($objectClass, $objectLDAPName)
            if ($null -ne $attributes.samAccountName) {
                switch ($objectClass) {
                    "computer" {
                        $targetObject.put("samAccountName","$($name[0..15] -join '')`$")
                    }
                    "user" {
                        $targetObject.put("samAccountName","$($name[0..20] -join '')")
                    }
                }
            }
            $needsUpdate = $true
        }
        foreach ($attribute in $Attributes.GetEnumerator()) {
            write-verbose "Trying $($attribute.key)"
            if ($targetObject.psbase.invokeGet($attribute.key) -eq $attribute.value) {
                write-verbose "Skipping $($attribute.key). Value already matches $($attribute.value)"
                $thisStatus.add(
                    $attribute.key,
                    ($attribute.value.toString()[0..48] -join "")
                )
            } else {
                $needsUpdate = $true
                write-verbose "Setting $($attribute.Name): $($targetObject.psbase.invokeGet($attribute.key)) --> $($attribute.value)"
                $targetObject.put($attribute.key, $attribute.value)
                $thisStatus.add(
                    $attribute.key,
                    ("{0}{1}" -f $colorupd, ($attribute.value.toString()[0..48] -join ""))
                )
            }
        }
        if ($needsUpdate) {
            if ($pscmdlet.shouldProcess($targetObject.distinguishedName, "Updating directory object")) {
                $targetObject.setinfo()
                if ($isNew) {
                    $targetObject.setPassword($(get-randomPassword -AsPlainText -PasswordLength 32 -forceComplex))
                    $targetObject.setInfo()
                }
            }
            $thisStatus.Status = $thisStatus.Status.replace("$colorpend","$colorUpd")
            $thisStatus.Status = $thisStatus.Status.replace("ing", "ed")
        } else {
            $thisStatus.Status  = "$($colorgood)Done"
            write-verbose "Nothing to do, object is already up to date."
        }
        [pscustomobject]$thisStatus
        $targetObject.Close()
        $parent.close()
    }
}

function get-randomPassword {
    [CmdletBinding()]
    param (
        [Parameter()]
        [ValidateRange(6,128)]
        [Int]
        $PasswordLength = 14,

        [switch]
        $forceComplex,

        [Switch]
        $AsPlainText

    )
    BEGIN {
        [char[]]$Exclusions = 'vwuOmnil1-.,%'
    }
    PROCESS{
        $GeneratedCharacters = @{
            Uppercase = (97..122) | get-random -count 32 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            Lowercase = (65..90) | get-random -count 128 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            Numeric = (48..57) | get-random -count 16 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            SpecialChar = [Char[]]('!!@#$%&*()=?}][{') | get-random -count 4 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
        }

        $StringSet = $GeneratedCharacters.Uppercase + $GeneratedCharacters.Lowercase + $GeneratedCharacters.Numeric + $GeneratedCharacters.SpecialChar
        $Complexity = get-random -count 1 -inputObject $GeneratedCharacters.Lowercase
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.Uppercase
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.Numeric
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.SpecialChar

        $PreScramble = -join(get-random -count ($passwordLength-4) -InputObject $StringSet)
        $GeneratedPassword = $($PreScramble + $Complexity) | sort-object {get-random}
        if ($AsPlainText) {
            $GeneratedPassword
        } else {
            $GeneratedPassword | ConvertTo-SecureString -AsPlainText -force
        }
    }
}

function Get-DSSchemaClass {
    [CmdletBinding(DefaultParameterSetName = "all")]
    param (
        [Parameter(ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [string[]]
        $objectName,

        # SchemaIDGUID to resolve
        [Parameter(ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [guid[]]
        $SchemaIDGUID
    )
    Begin {
        $searchBase = "LDAP://$($(get-dsinfo -infoSet RootDSE).SchemaNamingContext)"
        $Properties = @("Name", "AdminDescription", "ldapdisplayname", "SchemaIDGUID", "ObjectClass", "ObjectGUID", "defaultSecurityDescriptor")
    }
    Process {
        # Filter Structure
        <#
        (&
            (|
                (objectClass=...)
                (objectClass)
            )
            (|
                (name=...)
                (name=...)
                (schemaidguid=...)
            )
        )
        #>
        $FilterBuilder = [System.Text.StringBuilder]::new("(|(objectClass=classSchema)(objectClass=attributeSchema))")
        if ($SchemaIDGUID -or $objectName) {
            [void]$FilterBuilder.insert(0,"(&")
            [void]$filterBuilder.append("(|")
            if ($null -ne $SchemaIDGUID) {
                $SchemaIDGUID | foreach-object {
                    $GUIDString = $($_.toByteArray() | foreach-object {'\{0:X2}' -f $_}) -join ""
                    [void]$FilterBuilder.append("(schemaIDGUID=$GUIDString)")
                }
            }
            if ($null -ne $objectName) {
                $objectName | foreach-object {
                    [void]$FilterBuilder.append("(name=$_)")
                    [void]$FilterBuilder.append("(ldapdisplayname=$_)")
                }
            }
            [void]$FilterBuilder.Append("))")
        }
        $LDAPFilter = $FilterBuilder.ToString()
        write-verbose "SearchBase: $searchBase"
        write-verbose "Filter: $LDAPFilter"
        [adsisearcher]::new(
            [adsi]$searchBase,
            $LDAPFilter,
            $Properties
        ).findAll() | foreach-object {
            [pscustomObject]@{
                Name = [String]$_.properties['Name']
                ldapDisplayName = [string]$_.properties['ldapdisplayname']
                Type = "Object"
                Description = [String]$_.properties['AdminDescription']
                adspath = $($_.properties['adspath'])
                ObjectClass = $_.properties['objectClass'][ $_.properties['objectClass'].count - 1 ]
                ObjectGUID = [GUID]$($_.properties['objectGUID'])
                SchemaIDGUID = [GUID]$($_.properties['SchemaIDGUID'])
                GUID = [GUID]$($_.properties['SchemaIDGUID'])
                AppliesTo = $null
                DefaultSecurityDescriptor = $(
                    $SecurityDescriptor = [System.DirectoryServices.ActiveDirectorySecurity]::new()
                    $securityDescriptor.SetSecurityDescriptorSddlForm($_.properties['DefaultSecurityDescriptor'])
                    $securityDescriptor
                )
            }
        }
    }
}

function Get-DSExtendedRight {
        [CmdletBinding(DefaultParameterSetName = "default")]
        [CmdletBinding()]
        param (

        # Name of extended right to resolve
        [Parameter( ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ArgumentCompleter({
            [OutputType([System.Management.Automation.CompletionResult])]
            param(
                [string] $CommandName,
                [string] $ParameterName,
                [string] $WordToComplete,
                [System.Management.Automation.Language.CommandAst] $CommandAst,
                [System.Collections.IDictionary] $FakeBoundParameters
            )
            $CompletionResults = (get-dsextendedRight -Name "$wordToComplete*").name
            return $CompletionResults
        })]
        [String[]]
        $ExtendedRightName,

        # GUID(s) of rights to resolve
        [Parameter()]
        [GUID[]]
        $RightsGUID
    )

    Begin {
        $SearchBase = [adsi]"LDAP://CN=Extended-Rights,$(([ADSI]"LDAP://RootDSE").ConfigurationNamingContext)"
        $objectClass = "ControlAccessRight"
        $Properties = @("Name", "RightsGUID", "ldapdisplayname", "DisplayName", "ObjectClass", "objectGUID", "AppliesTo")
    }
    Process {
        if ($RightsGUID -or $ExtendedRightName) {
            $FilterBuilder = [System.Text.StringBuilder]::new("(&(objectClass=ControlAccessRight)(|")
            if ($null -ne $RightsGUID) {
                $RightsGUID | foreach-object {
                    [void]$FilterBuilder.append("(RightsGUID=$($_.toString()))")
                }
            }
            if ($null -ne $ExtendedRightName) {
                $ExtendedRightName | foreach-object {
                    [void]$FilterBuilder.append("(name=$_)")
                }
            }
            [void]$FilterBuilder.Append("))")
            $LDAPFilter = $FilterBuilder.ToString()
        } else {
            $ldapFilter = "(objectClass=ControlAccessRight)"
        }
        write-verbose "Using filter: $LDAPFilter"
        [adsiSearcher]::new(
            $SearchBase,
            $LDAPFilter,
            $Properties
        ).findall() | foreach-object {
            [pscustomobject]@{
                Name = [String]$_.properties['Name']
                ldapDisplayName = [string]$_.properties['ldapdisplayname']
                Type = "Right"
                Description = [String]$_.properties['DisplayName']
                ObjectClass = $_.properties['objectClass'][ $_.properties['objectClass'].count - 1 ]
                ObjectGUID = [GUID]$($_.properties['objectGUID'])
                GUID = [GUID]$($_.properties['RightsGUID'])
                RightsGUID = [GUID]$($_.properties['RightsGUID'])
                adspath = $($_.properties['adspath'])
                AppliesTo =  [GUID[]]$_.properties['appliesTo']
            }
        }
    }
}

function Get-DSAdminContextMenu {
    [CmdletBinding()]
    param (
        # CodePage to check
        [Parameter()]
        [Int]
        $CodePage = 409,

        # ObjectType to modify
        [Parameter(Mandatory)]
        [ArgumentCompleter({
            [OutputType([System.Management.Automation.CompletionResult])]
            param(
                [string] $CommandName,
                [string] $ParameterName,
                [string] $WordToComplete,
                [System.Management.Automation.Language.CommandAst] $CommandAst,
                [System.Collections.IDictionary] $FakeBoundParameters
            )
            $results = [adsisearcher]::new(
                [adsi]"LDAP://$(([adsi]"LDAP://rootDSE").SchemaNamingContext)",
                "(&(SchemaIDGUID=*)(name=$wordToComplete*))",
                @("name", "ldapdisplayname", "AdminDescription")
            ).findAll() | foreach-object {
                [string]$($_.properties.ldapdisplayname)
            }
            return $results
        })]
        [String]
        $LDAPObjectName,

        # Return raw string rather than PSCustomObject
        [switch]
        $AsString
    )
    $DisplaySpecifier = [adsi]"LDAP://CN=$LDAPObjectName-Display,CN=$CodePage,CN=DisplaySpecifiers,CN=Configuration,$(([adsi]"LDAP://rootDSE").defaultNamingContext)"
    if ($null -ne $DisplaySpecifier) {
        $DisplaySpecifier.adminContextMenu.getEnumerator() | foreach-object {
            if (-not $asString) {
                $menuSplit = $_.split(",");
                if ($menuSplit.count -eq 2 -and $menusplit[1] -like "{*}") {
                    [pscustomobject]@{
                        Priority = $menuSplit[0]
                        Clsid    = $menuSplit[1]
                        MenuText = $null
                        Command =  $null
                    }
                } elseif ($menusplit.count -eq 3) {
                    [pscustomobject]@{
                        Priority = $menuSplit[0]
                        Clsid    = $null
                        MenuText = $MenuSplit[1]
                        Command  = $menuSplit[2]
                    }
                }
            } else {
                [string]$_
            }
        }
    } else {
        write-warning "No displayspecifiers found for this class."
    }
}
Function Set-DSAdminContextMenu {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        # CodePage to check
        [Parameter()]
        [Int]
        $CodePage = 409,

        # ObjectType to modify
        [Parameter(Mandatory)]
        [ArgumentCompleter({
            [OutputType([System.Management.Automation.CompletionResult])]
            param(
                [string] $CommandName,
                [string] $ParameterName,
                [string] $WordToComplete,
                [System.Management.Automation.Language.CommandAst] $CommandAst,
                [System.Collections.IDictionary] $FakeBoundParameters
            )
            $results = [adsisearcher]::new(
                [adsi]"LDAP://$(([adsi]"LDAP://rootDSE").SchemaNamingContext)",
                "(&(SchemaIDGUID=*)(name=$wordToComplete*))",
                @("name", "ldapdisplayname", "AdminDescription")
            ).findAll() | foreach-object {
                [string]$($_.properties.ldapdisplayname)
            }
            return $results
        })]
        [String]
        $LDAPObjectName,

        # Context Menu Text
        [Parameter(mandatory)]
        [String]
        $MenuText,

        # Command that will be run. This cannot have any parameters, and must either be a full path or be in $PATH.
        # The adspath and objectClass will be passed in as arguments.
        [Parameter(Mandatory)]
        [String]
        $Command,

        # Whether to add or remove an item
        [Parameter(Mandatory)]
        [ValidateSet("Add","Remove")]
        [String]
        $Action,

        # Optionally specify a priority
        [Parameter()]
        [UInt16]$Priority
    )
    $connectString = "LDAP://CN=$LDAPObjectName-Display,CN=$CodePage,CN=DisplaySpecifiers,CN=Configuration,$(([adsi]"LDAP://rootDSE").defaultNamingContext)"
    write-verbose "Connecting: $connectString"
    $DisplaySpecifier = [adsi]$connectString
    if ($null -ne $DisplaySpecifier) {
        if ($action -eq "Remove") {
            $regex = [regex]"[0-9]+,`"?$menuText`"?,`"?$command`"?"
            $ItemsToRemove = $DisplaySpecifier.adminContextMenu.where({$_ -match $regex})
            if($PSCmdlet.ShouldProcess($item, "Removing Context Menu item")) {

            }
            foreach ($item in $itemsToRemove) {
                if($PSCmdlet.ShouldProcess($item, "Removing Context Menu item")) {
                    $displaySpecifier.adminContextMenu.remove($item)
                }
            }
            if($PSCmdlet.ShouldProcess($($DisplaySpecifier.Properties.name), "Writing back to AD")) {
                $DisplaySpecifier.setInfo()
            }
            $DisplaySpecifier.close()
        } elseif ($action -eq "Add") {
            if (-not $priority) {
                $Priority = ($CurrentDisplaySpecifier | measure-object -maximum Priority).maximum
            }
            $newSpecifier = "{0},{1},{2}" -f $priority, $MenuText, $command
            if($PSCmdlet.ShouldProcess($newSpecifier, "Add Context Menu item")) {
                $DisplaySpecifier.adminContextMenu.add($newSpecifier)
                $DisplaySpecifier.setInfo()
            }
            $DisplaySpecifier.close()
        }
    }
}

function convertfrom-IADSLargeInt {
    param(
        [System.__ComObject]$LargeIntObject
    )
    $HighPart = $LargeIntObject.getType().invokeMember("highPart", [System.Reflection.BindingFlags]::getProperty, $null, $LargeIntObject, $null)
    $LowPart = $LargeIntObject.getType().invokeMember("lowPart", [System.Reflection.BindingFlags]::getProperty, $null, $LargeIntObject, $null)
    return [int64]("0x{0:x8}{1:x8}" -f $Highpart, $lowpart)
}

function convertTo-IADSLargeInt {
    param(
        [int64]$Long
    )
    $hexRep = "{0:x16}" -f $long
    return [psCustomObject]@{
        HighPart = [int32]("0x{0:x8}" -f $hexRep.substring(0,8))
        LowPart = [int32]("0x{0:x8}" -f $hexRep.substring(8,8))
    }
}

function Get-DSACLs {

    Param(
        [Parameter(parametersetname="Live", mandatory)]
        [String]$DistinguishedName,

        [Parameter(parametersetname="stored")]
        [System.Security.AccessControl.AuthorizationRuleCollection]
        $ACLList,

        [Parameter(parametersetname="Live")]
        [Switch]$ShowDefaults,

        [Switch]$showInherited
    )

    BEGIN {
    }

    Process {
        if (-not $ACLList) {
            $targetobject = [adsi]"LDAP://$DistinguishedName"
            $targetObject.getInfo()
            If (-not $ShowDefaults) {
                $defaultSecurityDescriptor = $(Get-DSSchemaClass -objectName $targetObject.objectClass[-1]).defaultSecurityDescriptor
                foreach ($rule in $defaultSecurityDescriptor.access) {
                    $targetObject.objectSecurity.RemoveAccessRuleSpecific($rule)
                }
            }
            $ACLList = [System.Security.AccessControl.AuthorizationRuleCollection]$($targetObject.objectSecurity).access
        }
        $FilteredList = if (-not $showInherited) {
            $ACLList.where({$_.isInherited -eq $false})
        }else {
            $ACLList
        }
        # Pre-resolve the rights and objects as this prevents duplicate lookups.
        $ExtendedRightsList = ($FilteredList.where({$_.activeDirectoryRights -eq "ExtendedRight"}).objectType | sort-object -unique)
        $ExtendedRights = Get-DSExtendedRight -RightsGUID $ExtendedRightsList
        $objectTypeList = ($FilteredList.where({$_.activeDirectoryRights -ne "ExtendedRight"}).objectType | sort-object -unique)
        $objectTypes = Get-DSSchemaClass -SchemaIDGUID $objectTypeList
        foreach ($ACL in $FilteredList) {
            $AppliesTo = $objectTypes.where({$_.SchemaIDGUID -eq $ACL.InheritedObjectType}).name
            if ($ACL.activeDirectoryRights -eq "ExtendedRight") {
                $item = $ExtendedRights.where({$_.RightsGUID -eq $ACL.objectType}).name
            } else {
                $item = $objectTypes.where({$_.SchemaIDGUID -eq $ACL.objectType}).name
            }

            $ACL | select-object `
                @{Name = "Principal"; expression = { $_.identityReference.translate([system.security.principal.ntaccount]).value }},`
                @{name = "Rights"; expression = { $_.ActiveDirectoryRights }},`
                @{name = "AppliesTo"; expression = { $AppliesTo }},`
                @{Name = "Item"; expression = { $item }},`
                @{Name = "Access"; expression = { $_.accessControlType }},`
                @{name = "Inheritance"; expression = { $_.inheritanceType }},`
                inheritanceFlags,`
                IsInherited
        }
    }
}

function Resolve-DSACLs {
    [CmdletBinding(DefaultParameterSetName = 'Normal')]
    Param
    (

        # The principal that the ACE refers to
        [Parameter(mandatory, parametersetname = "Normal", ValueFromPipelineByPropertyName )]
        [Parameter(mandatory, parametersetname = "Extended", ValueFromPipelineByPropertyName )]
        [ValidateScript( { [System.Security.Principal.NTAccount]::new($_).translate([System.security.Principal.SecurityIdentifier]) })]
        $Principal,

        # The principal that the ACE refers to
        [Parameter(mandatory, parametersetname = "IdentityNormal", ValueFromPipelineByPropertyName )]
        [Parameter(mandatory, parametersetname = "IdentityExtended", ValueFromPipelineByPropertyName )]
        [System.Security.Principal.IdentityReference]
        $Identity,

        # ADRight: Generally CreateChild, DeleteChild, GenericAll, or something involving "ExtendedRight".
        [Parameter(ValueFromPipelineByPropertyName )]
        [System.directoryservices.ActiveDirectoryRights] $ADRight,

        # a specific extended right, which must be a proper AD Schema extendedright
        [Parameter(parametersetname = "IdentityExtended", mandatory, ValueFromPipelineByPropertyName )]
        [Parameter(parametersetname = "Extended", Mandatory, ValueFromPipelineByPropertyName )]
        #[ArgumentCompleter( { (Get-ADObjectGUIDs | where-object { $_.type -eq "Right" }).name })]
        [ValidateScript( { [bool](Get-DSExtendedRight -objectName $_) })]
        [String]$ExtendedRight,

        # If ADRight is "writeproperty", this can be the target attribute. If it's createchild, it's the child object type. Must be a valid AD Schema object
        [Parameter(parametersetname = "IdentityNormal", ValueFromPipelineByPropertyName )]
        [Parameter(parametersetname = "Normal", ValueFromPipelineByPropertyName )]
        #[ArgumentCompleter( { (Get-ADObjectGUIDs | where-object { $_.type -eq "Object" }).name })]
        [ValidateScript( { [bool](Get-DSSchemaClass -objectName $_) })]
        [String[]]$TargetObject,

        # Also known as InheritedObjectType, shown as "Applies to" in the GUI. This is an object GUID. This must be a valid AD Schema object name or array of objects
        [Parameter(ValueFromPipelineByPropertyName )]
        #[ArgumentCompleter( { (Get-ADObjectGUIDs | where-object { $_.type -eq "Object" }).name })]
        [ValidateScript( { [bool](Get-DSSchemaClass -objectName $_) })]
        [String[]]$AppliesTo,

        # Whether the ACE is an allow or deny entry
        [Parameter(ValueFromPipelineByPropertyName )]
        [Validateset("Allow", "Deny")]
        [System.security.AccessControl.AccessControlType] $Action = "Allow",

        # Whether and how the inheritance applies to descendents
        [Parameter(ValueFromPipelineByPropertyName )]
        [System.DirectoryServices.ActiveDirectorySecurityInheritance] $InheritanceType = "All",

        [Parameter()]
        $ObjectGUIDs = $null
    )

    Begin {
        $sw = [System.Diagnostics.Stopwatch]::StartNew()
        $acesCreated = 0
    }

    Process {

        try {
            if (-not $identity) {
                write-warning -message "Resolving principal to identity"
                $PrincipalName = $principal
                $principalSID = [System.Security.Principal.NTAccount]::new($principalName).translate([System.security.Principal.SecurityIdentifier])
                $identity = [System.Security.Principal.IdentityReference] $principalSID
            } else {
                write-warning -message "Resolving identity to principal"
                $principalName = $identity.translate([System.Security.Principal.NTAccount]).value.toString()
            }

            write-warning -message "Resolving AppliesTo to IOTs"
            try {
                $inheritedObjectTypeList = @(
                    If ($appliesTo) {
                        $AppliesTo | foreach-object {
                            $IOTName = $_
                            @{
                                Name = $IOTName
                                GUID = $(Get-DSSchemaClass -objectName $IOTName).GUID
                            }
                        }
                    }
                    else {
                        @{
                            Name = "(All / Unspec)"
                            GUID = [GUID]"00000000-0000-0000-0000-000000000000"
                        }
                    }
                )
            } catch {
                Write-LogHandler -level "Warning" -message "Failed to resolve appliesTo ($appliesTo) to an IOT GUID."
                throw $_
            }
            write-warning -message "Resolving ADRights / ExtendedRights"
            If ($ExtendedRight -and -not $ADRight) {
                write-warning -message "Setting ADRight to 'ExtendedRight'"
                $ADRight = [System.directoryservices.ActiveDirectoryRights]"ExtendedRight"
            }
            write-warning -message "Resolving ObjectTypes"
            try {
                $ObjectTypeList = $(
                    if ($extendedRight) {
                        write-warning -message "ObjectType as extended right ($extendedRight)"
                        @{
                            Name = $extendedRight
                            Type = "ExtendedRight"
                            GUID =$(Get-DSExtendedRight -objectName $extendedRight).GUID
                        }
                    }
                    elseif ($targetObject) {
                        write-warning -message "ObjectType as schema class or attribute"
                        $targetObject | foreach-object  {
                            $objectName = $_
                            @{
                                Name = $objectName
                                Type = "Schema class or attribute"
                                GUID = $(Get-DSSchemaClass -objectName $objectName).GUID
                            }
                        }
                        write-warning -message "finished resolving objectType"
                    }
                    else {
                        write-warning -message "Setting null (wildcard) objectType / attribute"
                        @{
                            Name = "Null"
                            Type = "(Unknown / wildcard)"
                            GUID = [GUID]"00000000-0000-0000-0000-000000000000"
                        }
                    }
                )
            } catch {
                write-loghandler -level "Warning" -message "Failed to resolve an objectType from extendedRight($extendedRight)/targetObject($targetObject)"
                throw $_
            }
            write-warning -message "Cycling through IOT and object lists to create single master list"
            $ACEParamList = @(
                foreach ($InheritedObjectType in $inheritedObjectTypeList) {
                    foreach ($ObjectType in $ObjectTypeList) {
                        @{
                            PrincipalName = $principalName
                            Identity = $Identity
                            ADRight = $ADRight
                            Action = $Action
                            objectType = $ObjectType['GUID']
                            InheritanceType = $InheritanceType
                            InheritedObjectType = $inheritedObjectType['GUID']
                        }
                        write-warning -target $Principalname -message ("{0,-5} {1,-16} on: {2,-24} IOT: {3,-24} {4}" -f $action, $ADRight, ($ExtendedRight + $objectType['Name']), $inheritedObjectType['Name'], $PrincipalName)
                    }
                }
            )

            $ACEParamList | foreach-object -parallel  {
                write-host $($_ | format-table | out-string)
                [System.DirectoryServices.ActiveDirectoryAccessRule]::new($_['Identity'], $_['ADRight'], $_['Action'], $_['objectType'], $_['InheritanceType'], $_['inheritedObjectType'])
            }
            $acesCreated +=$ACEParamList.count

        }
        catch {
            $_ | format-list * -force
            write-loghandler -level "warning" -message "WHOOPS"
        }
    }
    End{
        $sw.stop()
        write-warning -message "Created $AcesCreated ACEs in $($sw.ElapsedMilliseconds) ms"
    }
}





function Set-DSACLs {

    Param(
        [Parameter(parametersetname="Live", mandatory)]
        [String]$DistinguishedName,

        [Parameter(parametersetname="stored")]
        [System.Security.AccessControl.AuthorizationRuleCollection]
        $ACLList,

        [Switch]$ShowDefaults
    )

    BEGIN {
    }

    Process {
        if (-not $ACLList) {
            $ACLList = [System.Security.AccessControl.AuthorizationRuleCollection]([adsi]"LDAP://$DistinguishedName").psbase.objectSecurity.access
        }
        If (-not $ShowDefaults) {
            $ACLList = $ACLList.where({$_.IdentityReference -like "$($env:USERDOMAIN)\*"})
        }
        $objectTypes = $ACLList.objectType | sort-object -unique
        $extendedRightTime = [System.Diagnostics.Stopwatch]::new()
        $objectLookupTime = [System.Diagnostics.Stopwatch]::new()
        $ACLList | foreach-object {
            $thisObject = $_
            $objectLookupTime.start()
            $AppliesTo = (Get-DSSchemaClass -SchemaIDGUID $thisObject.InheritedObjectType).name
            $objectLookupTime.stop()
            if ($thisObject.activeDirectoryRights -eq "ExtendedRight") {
                $extendedRightTime.start()
                $item = (get-dsextendedRight -RightsGUID $thisObject.objectType).name
                $extendedRightTime.stop()
            } else {
                $objectLookupTime.start()
                $item = (Get-DSSchemaClass -SchemaIDGUID $thisObject.objectType).name
                $objectLookupTime.stop()
            }

            $thisObject | select-object `
                @{Name = "Principal"; expression = { $_.identityReference.translate([system.security.principal.ntaccount]).value }},`
                @{name = "Rights"; expression = { $_.ActiveDirectoryRights }},`
                @{name = "AppliesTo"; expression = { $AppliesTo }},`
                @{Name = "Item"; expression = { $item }},`
                @{Name = "Access"; expression = { $_.accessControlType }},`
                @{name = "Inheritance"; expression = { $_.inheritanceType }},`
                inheritanceFlags,`
                IsInherited
        }
        write-warning "ObjectLookup: $($objectLookupTime.ElapsedMilliseconds); RightLookup: $($stopwatch.ElapsedMilliseconds)"
    }
}