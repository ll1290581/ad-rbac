#
# Module manifest for module 'ps.rad'
#
# Generated by: John@Breakwaterlabs.net
#
# Generated on: 11/7/2023
#

@{

# Script module or binary module file associated with this manifest.
RootModule = 'ps.rad.psm1'

# Version number of this module.
ModuleVersion = '0.9'

# Supported PSEditions
# CompatiblePSEditions = @()

# ID used to uniquely identify this module
GUID = '5c20b4a0-9928-4d80-bf98-55f7d39225f4'

# Author of this module
Author = 'John@Breakwaterlabs.net'

# Company or vendor of this module
CompanyName = 'Breakwaterlabs'

# Copyright statement for this module
Copyright = '(c) 2023 john@breakwaterlabs.net. All Rights Reserved'

# Description of the functionality provided by this module
Description = 'Base module for RBAC-oriented AD'

# Minimum version of the PowerShell engine required by this module
PowerShellVersion = '5.1'

# Modules that must be imported into the global environment prior to importing this module
RequiredModules = @('ActiveDirectory')

# Assemblies that must be loaded prior to importing this module
# RequiredAssemblies = @()

# Script files (.ps1) that are run in the caller's environment prior to importing this module.
# ScriptsToProcess = @()

# Type files (.ps1xml) to be loaded when importing this module
# TypesToProcess = @()

# Format files (.ps1xml) to be loaded when importing this module
# FormatsToProcess = @()

# Modules to import as nested modules of the module specified in RootModule/ModuleToProcess
#NestedModules = @('ps.rad.core')

# Functions to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no functions to export.
FunctionsToExport = '*'

# Cmdlets to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no cmdlets to export.
CmdletsToExport = '*'

# Variables to export from this module
VariablesToExport = '*'

# Aliases to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no aliases to export.
AliasesToExport = '*'

# DSC resources to export from this module
# DscResourcesToExport = @()

# List of all modules packaged with this module
# ModuleList = @()

# List of all files packaged with this module
# FileList = @()

# Private data to pass to the module specified in RootModule/ModuleToProcess. This may also contain a PSData hashtable with additional module metadata used by PowerShell.
PrivateData = @{
    Versions = @{
        # SchemaVer indicates settings JSOn structure
        SchemaVer = "1"
        # APIVer is for code compatibility. Any breaking code changes will increment the ver.
        APIVer = "1"
        # NamesVer is for changes to the default names of objects. 
        # Existing installs should never use settings from a newer NamesVer without confirmation.
        NamesVer = "1"
    }
    Names = @{
        RAD = "RAD"
        Orgs = "Orgs"
        Components = "Components"
        Global = "Global"
        Computers = "Endpoints"
        Rights = "Rights"
        Roles = "Roles"
        Users = "Users"
        Admins = "Admins"
    }
    PSData = @{

        # Tags applied to this module. These help with module discovery in online galleries.
        Tags = @("ActiveDirectory", "Automation", "Security")

        # A URL to the license for this module.
        LicenseUri = 'https://www.gnu.org/licenses/agpl-3.0.en.html'

        # A URL to the main website for this project.
        ProjectUri = 'https://gitlab.com/breakwaterlabs/ps.rad'

        # A URL to an icon representing this module.
        # IconUri = ''

        # ReleaseNotes of this module
        # ReleaseNotes = ''

        # Prerelease string of this module
        # Prerelease = ''

        # Flag to indicate whether the module requires explicit user acceptance for install/update/save
        # RequireLicenseAcceptance = $false

        # External dependent modules of this module
        # ExternalModuleDependencies = @()

    } # End of PSData hashtable

} # End of PrivateData hashtable

# HelpInfo URI of this module
# HelpInfoURI = ''

# Default prefix for commands exported from this module. Override the default prefix using Import-Module -Prefix.
DefaultCommandPrefix = 'RAD'

}

