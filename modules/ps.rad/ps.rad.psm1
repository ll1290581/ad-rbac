Function Get-Settings {
  <#
      .SYNOPSIS
      Reads in settings for RAD module
      .DESCRIPTION
      This will search AD for an MS-DS-App-Configuration object named "PS.RAD".
      If it exists, it will normalize the settings and return them.
      If it does not exist, it will initialize settings with defaults and return them.

      .INPUTS
      none
      .OUTPUTS
      JSON
  #>
  [CmdletBinding()]
  Param(
  )
  Begin {
    $ModuleSettings = $MyInvocation.MyCommand.Module

    $([ordered]@{
      Description = "PS.RAD Settings for $($ModuleSettings.Description)"
      Versions = $ModuleSettings.PrivateData.Versions
      GID = [convert]::ToHexString([System.Security.Cryptography.SHA1CryptoServiceProvider]::new().ComputeHash($moduleSettings.guid.ToByteArray()))
      OID = (get-date).toFileTime() -bxor ([bitConverter]::ToInt64(([System.Security.Cryptography.SHA1CryptoServiceProvider]::new().ComputeHash($moduleSettings.guid.ToByteArray()))))
      ProjectUri = $ModuleSettings.ProjectURI
      Author = $ModuleSettings.Author
      Copyright = $ModuleSettings.Copyright
      LicenseUri = $ModuleSettings.LicenseURI
      Version = $ModuleSettings.version.tostring()
      InstallDate = (get-date)
      Names = $ModuleSettings.PrivateData.Names
      OUPaths = @{
        RADRoot = "OU={0},DC=Lab0,DC=Local" -f $ModuleSettings.PrivateData.Names.RAD
        GlobalBase = "OU={0},DC=Lab0,DC=Local" -f $ModuleSettings.PrivateData.Names.RAD
        OrgBase = "OU={1},OU={0},DC=Lab0,DC=Local" -f $ModuleSettings.PrivateData.Names.RAD, $ModuleSettings.PrivateData.Names.Orgs
      }
      FilePaths = @{
        JSON = "\\lab0.local\SYSVOL\Domains\{0}" -f $ModuleSettings.PrivateData.Names.RAD
        LDIF = "\\lab0.local\SYSVOL\Domains\{0}" -f $ModuleSettings.PrivateData.Names.RAD
        Settings = "\\lab0.local\SYSVOL\Domains\{0}" -f $ModuleSettings.PrivateData.Names.RAD
      }
      Modules = @{
        PKI = @{
          Version = "1.1.0"
          InstallDate = (get-date)
          SettingsPath = "...."
        }
      }
      })| convertto-json
      #set-adobject 'CN=PS.RAD-Settings,OU=Orgs,DC=lab0,DC=local' -Replace @{'msDS-Settings'="PS.RAD=$JSON"}
      #$ModuleSettings = [System.Collections.Specialized.OrderedDictionary]::new()
      #$ModuleSettings = ((get-adobject -filter "name -eq 'PS.RAD-Settings'" -properties "msDS-Settings").'msds-settings'.valuelist.where{$_ -like "P*"}.replace("PS.RAD=","") | ConvertFrom-Json)

    }
  }
function get-radtest {

}
