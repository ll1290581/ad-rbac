function Lock-DPAPIMessage {
    <#
    .SYNOPSIS
        Encrypts a message using DPAPI-NG and a target SID
    .DESCRIPTION
        DPAPI-NG allows encrypting messages using a target SID, which can then be decrypted only by them.
    .NOTES
        This is not directly supported in Linux, but there are python implementations (see LAPS4Linux)
    .LINK
        https://learn.microsoft.com/en-us/windows/win32/seccng/cng-dpapi
    .EXAMPLE
        Lock-DPAPIMessage -SID "S-1-5-...." -message "This is a secret"
        Outputs a DPAPI-encrypted byte array that can only be decrypted by the specified SID.
    #>
    [CmdletBinding(DefaultParameterSetName = 'MessageString')]
    param (
        # ProtectionDescriptor in the format "SID=S-1-5....". Determines who can decrypt a message
        [Parameter()]
        [String]$ProtectionDescriptor = "SID=$([System.Security.Principal.WindowsIdentity]::GetCurrent().user.value)",

        # Message to Encrypt, in String format
        [Parameter(Mandatory, ParameterSetName = "MessageString")]
        [String]$Message,

        # Message to encrypt, in base64 format
        [Parameter(ParameterSetName = "MessageB64")]
        [String]$Base64Message,

        # Message to encrypt, in Byte array format
        [Parameter(ParameterSetName = "MessageBytes")]
        [Byte[]]$MessageBytes,

        # Output message as base64 instead of byte[]
        [switch]$AsBase64
    )

    begin {}

    process {
        [byte[]]$unencryptedMessage = $(
            if ($String) {
                write-verbose -message "Converting message to Byte[]"
                [system.text.encoding]::UTF8.getBytes($String)
            } elseif ($Base64Message) {
                write-verbose -message "Converting message from base64 to byte[]"
                [system.convert]::FromBase64String($Base64Message)
            } else {
                $MessageBytes
            }
        )
        $protected = [DpapiNgUtil]::Protect($ProtectionDescriptor,$unencryptedMessage)
        if ($AsBase64) {
            [system.convert]::ToBase64String($protected)
        } else {
            $Protected
        }
    }
    End{ }
}

function New-LAPSJSON {
    <#
    .SYNOPSIS
        Creates a plaintext LAPS JSON string
    .DESCRIPTION
        LAPS Passwords need to be in JSON format before being protected by DPAPI. This takes a username and a password and converts it to the correct format.
    .NOTES
        This functionality on Linux can be seen with LAPS4Linux.
        For reference, see LAPS Tech ref: https://learn.microsoft.com/en-us/windows-server/identity/laps/laps-technical-reference
    .LINK
        Specify a URI to a help page, this will show when Get-Help -Online is used.
    .EXAMPLE
        Test-MyTestFunction -Verbose
        Explanation of the function or its result. You can include multiple examples with additional .EXAMPLE lines
    #>

    [CmdletBinding(DefaultParameterSetName="AsPlainText")]
    param (
        [Parameter(Mandatory)]
        [String]$Username,

        [Parameter()]
        [securestring]$SecurePassword,

        [Parameter()]
        [Switch]$GeneratePassword,

        # Date password was set-- stored in output
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$LastSet = [datetime]::now,

        # Normally, plaintext output will mask the password. This unmasks it
        [Parameter(ParameterSetName="AsPlaintext")]
        [Switch]$ShowPassword,

        # AD Group name to use with EncryptedPassword
        [parameter(ParameterSetName="LDAPEncrypted_Name", mandatory)]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            $DSE = ([adsi]"LDAP://rootDSE")
            [adsisearcher]::new([adsi]"LDAP://$($DSE.rootDomainNamingContext)", "(&(objectClass=group)(name=$wordToComplete*))", "name").findAll().Properties.name
        })]
        [string[]]$GroupName,

        # SIDs to use for locking encryption
        [parameter(ParameterSetName="LDAPEncrypted_SID", mandatory)]
        [string[]]$SID,

        # Format string for LDAP attribute msLAPS-Password: plaintext compressed json, UTF16-LE
        [parameter(ParameterSetName="LDAPPlain", Mandatory)]
        [Switch]$msLAPSPassword,

        # Format String for LDAP msLAPS-EncryptedFormat: bytestring as follows
        #   [uint64]filetimeutc + [int]DPAPILockedJSON.length + 0x00 0x00 0x00 0x00 + [bytes[]]DPAPILock([UTF16-LE]msLAPS-Password) + 0x00 0x00
        [parameter(ParameterSetName="LDAPEncrypted", Mandatory)]
        [parameter(ParameterSetName="LDAPEncrypted_Name", Mandatory)]
        [parameter(ParameterSetName="LDAPEncrypted_SID", Mandatory)]
        [switch]$msLAPSEncryptedPassword,

        # Output as hashtable for easy consumption by pipeline (e.g. `set-adcomputer -replace @output`)
        [parameter(ParameterSetName="LDAPEncrypted")]
        [parameter(ParameterSetName="LDAPEncrypted_Name")]
        [parameter(ParameterSetName="LDAPEncrypted_SID")]
        [parameter(ParameterSetName="LDAPPlain")]
        [Switch]$Passthru
    )

    begin {
    }

    process {
        if (-not $SecurePassword) {
            if ($GeneratePassword) {
                [SecureString]$SecurePassword = get-randomPassword -forceComplex
            } else {
                [secureString]$SecurePassword = read-host "Enter LAPS Password (Masked):" -AsSecureString
            }
        }
        $Credential = New-Object System.Management.Automation.PSCredential($username, $SecurePassword)
        if (
            [String]::IsNullOrEmpty($Credential.UserName) -or
            [String]::IsNullOrEmpty($Credential.GetNetworkCredential().Password)
        ) {
            Throw "Credential username or a password was either null, blank, or not provided."
        }

        # Dress up dateTime
        $LastSetFileTime = [datetime]::Parse($lastSet).ToFileTimeUtc()
        $LastSetLAPSJSONFormat = $LastSetFileTime.ToString("X").toLower()

        if ($showPassword -or $msLAPSEncryptedPassword -or $msLAPSPassword) {
            $LapsPw = $Credential.GetNetworkCredential().password
        } else {
            write-warning -message "Blinding password because '-ShowPassword' was not specified"
            $LAPSPw = $credential.password.GetHashCode()
        }

        $LAPSHashTable = @{
            'n' = $Credential.UserName
            'p' = $LapsPw
            't' = "$LastSetLAPSJSONFormat"
        }

        # Output
        if (-not ($msLAPSEncryptedPassword -or $msLAPSPassword)) {
            $output = $LAPSHashTable | convertto-JSON
        } else {
            $LAPSJSON = $LAPSHashTable | convertto-JSON -Compress
            if ($msLAPSPassword) {
                $format = "msLAPS-Password"
                $output = $LAPSJSO
            } elseif ($msLAPSEncryptedPassword) {
                $format = "msLAPS-EncryptedPassword"
                if (-not $SID -and -not $groupName) {
                    Write-warning "No SID or groupname was provided, please enter a security group to encrypt against."
                    $GroupName = read-host -prompt "Group Name:`r`n"
                }
                if ($groupName) {
                    $DSE = ([adsi]"LDAP://rootDSE")
                    $groupSearchFilter = [System.Text.StringBuilder]::new("(&(objectClass=group)(|")
                    $groupName | foreach-object {
                        write-verbose -message "resolving group: $_"
                        [void]$groupSearchFilter.Append("(name=$_)")
                    }
                    [void]$groupSearchFilter.append("))")
                    write-verbose -message "Using LDAP Filter: $($groupSearchFilter.toString())"
                    $SearchResults = [adsisearcher]::new(
                        [adsi]"LDAP://$($DSE.rootDomainNamingContext)",
                        $groupSearchFilter.ToString(),
                        "ObjectSID"
                    ).findall()

                    if ($null -ne $searchResults) {
                        $SID = $SearchResults | foreach-object {
                            [Security.Principal.SecurityIdentifier]::new(
                                [byte[]]$($_.getDirectoryEntry().objectSID),
                                0
                            ).value
                        }
                    }
                }
                if ($null -eq $SID) {
                    throw "Missing SID, cannot use DPAPI to lock message."
                }
                $ProtectionDescriptor = $($SID | foreach-object {"SID=$_"}) -join " OR "
                write-verbose -message "Using protectionDescriptor: $ProtectionDescriptor"
                write-verbose -message "Padding LAPS message as UTF-16-LE + NUL NUL"
                $LAPSPadding=[system.text.encoding]::UTF8.GetBytes(@(0x00, 0x00) -as [Char[]])
                $LAPSBytes = [system.text.encoding]::Unicode.GetBytes($LAPSJSON) + $LAPSPadding
                $LockedMessage = lock-DPAPIMessage -ProtectionDescriptor $ProtectionDescriptor -messageBytes $LAPSBytes
                $PreMagic = [bitconverter]::GetBytes([uint64]$LastSetFileTime) + [bitconverter]::GetBytes([int]$LockedMessage.Length) + [byte]0x00 + [byte]0x00 + [byte]0x00 + [byte]0x00
                [byte[]]$Output = $($premagic + $LockedMessage)
            }
            if ($passthru) {
                [hashtable]@{
                    "$format" = $output
                }
            } else {
                $output
            }
        }
    }

    end { }
}

function Set-LAPSPassword {
    [CmdletBinding(SupportsShouldProcess,ConfirmImpact='high')]
    param (

        # Computer Object to store credential in
        [Parameter(Mandatory)]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            $DSE = ([adsi]"LDAP://rootDSE")
            [adsisearcher]::new([adsi]"LDAP://$($DSE.rootDomainNamingContext)", "(&(objectClass=computer)(name=$wordToComplete*))", "name").findAll().Properties.name
        })]
        [String]
        $ComputerName,

        # Encrypted LAPS Credential to store
        [Parameter(Mandatory)]
        [Byte[]]
        $msLAPSEncryptedPassword
    )
    Begin {
        $DSE = ([adsi]"LDAP://rootDSE")
        # CASE SENSITIVE
        $ATTRIB_HISTORY = "mslaps-encryptedpasswordhistory"
        $ATTRIB_PASSWORD = "mslaps-encryptedpassword"
        $MAX_HISTORY = 4
    }

    Process {
        $objectName = $ComputerName
        $objectClass = "computer"
        $SearchResults = [adsisearcher]::new(
            [adsi]"LDAP://$($DSE.rootDomainNamingContext)",
            "(&(objectClass=$objectClass)(name=$objectName))",
            @("$ATTRIB_PASSWORD","$ATTRIB_HISTORY")
        ).findAll()
        if ($searchResults.count -ne 1) {
            throw "Too many or too few objects returned ($($SearchResults.count))"
        } else {
            $ADObject = $SearchResults.getDirectoryEntry()
            $CurrentPassword = [byte[]]$($ADObject.$ATTRIB_PASSWORD.getEnumerator())
            if ([convert]::ToBase64String($CurrentPassword) -eq [convert]::ToBase64String($msLAPSEncryptedPassword)) {
                write-warning "Current and new password objects are the same. No change."
            } else {
                try {
                    $ADObject.$ATTRIB_HISTORY.insert(0, [byte[]]$CurrentPassword)
                    for ($i = $ADObject.$ATTRIB_HISTORY.count; $i -gt $MAX_HISTORY -and $i -gt 1; $i--) {
                        # Careful of zero-index issues.
                        if ($i -gt $MAX_HISTORY) {
                            write-warning "Trimming password history item $i."
                            $ADObject.$ATTRIB_HISTORY.removeAt($i-1)
                        }
                    }
                    if($PSCmdlet.ShouldProcess($ADobject.distinguishedName,"Updating $ATTRIB_PASSWORD and putting current in $ATTRIB_HISTORY")) {
                        $ADObject.psbase.invokeSet($ATTRIB_PASSWORD, [byte[]]$msLAPSEncryptedPassword)
                        $ADObject.setInfo()
                    }
                } catch {
                    throw "Whoops, something went wrong. Current password was not changed"
                }
            }
        }
    }
}

function get-randomPassword {
    [CmdletBinding()]
    param (
        [Parameter()]
        [ValidateRange(6,128)]
        [Int]
        $PasswordLength = 14,

        [switch]
        $forceComplex,

        [Switch]
        $AsPlainText

    )
    BEGIN {
        [char[]]$Exclusions = 'vwuOmnil1-.,%'
    }
    PROCESS{
        $GeneratedCharacters = @{
            Uppercase = (97..122) | get-random -count 32 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            Lowercase = (65..90) | get-random -count 128 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            Numeric = (48..57) | get-random -count 16 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
            SpecialChar = [Char[]]('!!@#$%&*()=?}][{') | get-random -count 4 | where-object {$Exclusions -notContains $_} | foreach-object {[Char]$_}
        }

        $StringSet = $GeneratedCharacters.Uppercase + $GeneratedCharacters.Lowercase + $GeneratedCharacters.Numeric + $GeneratedCharacters.SpecialChar
        $Complexity = get-random -count 1 -inputObject $GeneratedCharacters.Lowercase
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.Uppercase
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.Numeric
        $Complexity += get-random -count 1 -inputObject $GeneratedCharacters.SpecialChar

        $PreScramble = -join(get-random -count ($passwordLength-4) -InputObject $StringSet)
        $GeneratedPassword = $($PreScramble + $Complexity) | sort-object {get-random}
        if ($AsPlainText) {
            $GeneratedPassword
        } else {
            $GeneratedPassword | ConvertTo-SecureString -AsPlainText -force
        }
    }
}



$DPAPI_TypeDef = @"
using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

public static class DpapiNgUtil
{
    public static string ProtectBase64(string protectionDescriptor, string input)
    {
        byte[] output = Protect(protectionDescriptor, Encoding.UTF8.GetBytes(input));
        return Convert.ToBase64String(output);
    }

    public static string UnprotectBase64(string input)
    {
        byte[] bytes = Convert.FromBase64String(input);
        byte[] output = Unprotect(bytes);
        return Encoding.UTF8.GetString(output);
    }

    public static byte[] Protect(string protectionDescriptor, byte[] data)
    {
        using (NCryptProtectionDescriptorHandle handle = NCryptProtectionDescriptorHandle.Create(protectionDescriptor))
        {
            return Protect(handle, data);
        }
    }

    internal static byte[] Protect(NCryptProtectionDescriptorHandle descriptor, byte[] data)
    {
        uint cbProtectedBlob;
        LocalAllocHandle protectedBlobHandle;
        int status = NativeMethods.NCryptProtectSecret(descriptor, NativeMethods.NCRYPT_SILENT_FLAG, data, (uint)data.Length, IntPtr.Zero, IntPtr.Zero, out protectedBlobHandle, out cbProtectedBlob);
        if(status != 0)
        {
            throw new CryptographicException(status);
        }

        using (protectedBlobHandle)
        {
            byte[] retVal = new byte[cbProtectedBlob];
            Marshal.Copy(protectedBlobHandle.DangerousGetHandle(), retVal, 0, retVal.Length);
            return retVal;
        }
    }

    public static byte[] Unprotect(byte[] protectedData)
    {
        uint cbData;
        LocalAllocHandle dataHandle;
        int status = NativeMethods.NCryptUnprotectSecret(IntPtr.Zero, NativeMethods.NCRYPT_SILENT_FLAG, protectedData, (uint)protectedData.Length, IntPtr.Zero, IntPtr.Zero, out dataHandle, out cbData);
        if (status != 0)
        {
            throw new CryptographicException(status);
        }

        using (dataHandle)
        {
            byte[] retVal = new byte[cbData];
            Marshal.Copy(dataHandle.DangerousGetHandle(), retVal, 0, retVal.Length);
            return retVal;
        }
    }
}

internal class LocalAllocHandle : SafeHandle
{
    // Called by P/Invoke when returning SafeHandles
    private LocalAllocHandle() : base(IntPtr.Zero, ownsHandle: true) { }

    // Do not provide a finalizer - SafeHandle's critical finalizer will
    // call ReleaseHandle for you.

    public override bool IsInvalid
    {
        get { return handle == IntPtr.Zero; }
    }

    protected override bool ReleaseHandle()
    {
        IntPtr retVal = NativeMethods.LocalFree(handle);
        return (retVal == IntPtr.Zero);
    }
}

internal class NCryptProtectionDescriptorHandle : SafeHandle
{
    // Called by P/Invoke when returning SafeHandles
    private NCryptProtectionDescriptorHandle() : base(IntPtr.Zero, ownsHandle: true) { }

    // Do not provide a finalizer - SafeHandle's critical finalizer will
    // call ReleaseHandle for you.

    public override bool IsInvalid
    {
        get { return handle == IntPtr.Zero; }
    }

    public static NCryptProtectionDescriptorHandle Create(string protectionDescriptor)
    {
        NCryptProtectionDescriptorHandle descriptorHandle;
        int status = NativeMethods.NCryptCreateProtectionDescriptor(protectionDescriptor, 0, out descriptorHandle);
        if (status != 0) {
            throw new CryptographicException(status);
        }
        return descriptorHandle;
    }

    protected override bool ReleaseHandle()
    {
        int retVal = NativeMethods.NCryptCloseProtectionDescriptor(handle);
        return (retVal == 0);
    }
}

internal static class NativeMethods
{
    private const string KERNEL32LIB = "kernel32.dll";
    private const string NCRYPTLIB = "ncrypt.dll";

    internal const uint NCRYPT_SILENT_FLAG = 0x00000040;


    // http://msdn.microsoft.com/en-us/library/windows/desktop/aa366730(v=vs.85).aspx
    [DllImport(KERNEL32LIB, SetLastError = true)]
    internal static extern IntPtr LocalFree(
        [In] IntPtr handle);

    // http://msdn.microsoft.com/en-us/library/windows/desktop/hh706799(v=vs.85).aspx
    [DllImport(NCRYPTLIB)]
    internal extern static int NCryptCloseProtectionDescriptor(
        [In] IntPtr hDescriptor);

    // http://msdn.microsoft.com/en-us/library/windows/desktop/hh706800(v=vs.85).aspx
    [DllImport(NCRYPTLIB, CharSet = CharSet.Unicode)]
    internal extern static int NCryptCreateProtectionDescriptor(
        [In] string pwszDescriptorString,
        [In] uint dwFlags,
        [Out] out NCryptProtectionDescriptorHandle phDescriptor);

    // http://msdn.microsoft.com/en-us/library/windows/desktop/hh706802(v=vs.85).aspx
    [DllImport(NCRYPTLIB)]
    internal extern static int NCryptProtectSecret(
        [In] NCryptProtectionDescriptorHandle hDescriptor,
        [In] uint dwFlags,
        [In] byte[] pbData,
        [In] uint cbData,
        [In] IntPtr pMemPara,
        [In] IntPtr hWnd,
        [Out] out LocalAllocHandle ppbProtectedBlob,
        [Out] out uint pcbProtectedBlob);

    // http://msdn.microsoft.com/en-us/library/windows/desktop/hh706811(v=vs.85).aspx
    [DllImport(NCRYPTLIB)]
    internal extern static int NCryptUnprotectSecret(
        [In] IntPtr phDescriptor,
        [In] uint dwFlags,
        [In] byte[] pbProtectedBlob,
        [In] uint cbProtectedBlob,
        [In] IntPtr pMemPara,
        [In] IntPtr hWnd,
        [Out] out LocalAllocHandle ppbData,
        [Out] out uint pcbData);
}
"@
add-type -TypeDefinition $DPAPI_TypeDef