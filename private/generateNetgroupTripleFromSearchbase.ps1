Function generateNetgroupTripleFromSearchbase {

    [CmdletBinding(DefaultParameterSetName='None')]
    Param(

        [Parameter(Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [string]$SearchBase,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    $EndpointList = get-adComputer -server $server -searchBase $SearchBase -filter "SAMAccountName -like '*'"
    $NISNetgroupTriple = [System.Collections.Generic.List[String]]::new()
    foreach ($endpoint in $endpointList) {
        $NISNetgroupTriple.add("({0},,)" -f $endpoint.name)
    }
    $NISNetgroupTriple
}
