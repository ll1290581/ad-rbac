function get-wellknownsid {
    Param (
        [System.Security.Principal.WellKnownSidType]$account
    )
    begin {
        $DSE = ([adsi]"LDAP://rootDSE")
        $thisDomain = @{
            DN = $DSE.defaultNamingContext.value
            DNSRoot = $ADSIDomain
            NetBIOS = $env:UserDomain
            SID = (New-Object 'Security.Principal.SecurityIdentifier' ($ADSIDomain.objectSID).value,0).value       
        }
    }
    process{
        $SIDSuffix = [System.Security.Principal.WellKnownSidType]::$account.value__
        "$DomainSID-$SIDSuffix"

    }
    
}