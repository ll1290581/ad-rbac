
function resolveEntityReferences {
    [CmdletBinding(DefaultParameterSetName = "rightsprefix")]
    Param(

        [Parameter(ParameterSetName = "rightsprefix")]
        [String]$RightsPrefix,
        [Parameter(ParameterSetName = "element")]
        [PSCustomObject]$rbacElement,
        [Hashtable]$RightsAndPrincipals,
        [Parameter(ParameterSetName = "element")]
        [switch]$IncludeParents,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    begin {
        $Netbios = (get-addomain).netBiosName
    }
    Process {
        try {
        $resolvedObjects = [System.Collections.ArrayList]::new()
        foreach ($item in $RightsAndPrincipals.GetEnumerator()) {
            foreach ($entity in $item.value) {
                write-loghandler -level "Verbose" -message ("Resolving entity reference: {0} : {1}" -f $item.key, $entity)
                if (-not [String]::IsNullOrEmpty($entity) ) {
                    $resolvedObjects = $(
                        if ($item.key -eq "Rights") {
                            $theseRights = [System.Collections.ArrayList]::new()
                            [void]$theseRights.add($rbacElement.Rights[$entity].name)
                            if ($IncludeParents) {
                                foreach ($parent in $rbacElement.Rights[$entity].ResolvedParents) {
                                    [void]$theseRights.add($parent)
                                }
                            }
                            $theseRights | foreach-object {
                                @{
                                    Name = $_
                                    SamAccountname = $_
                                    ObjectClass = "Group"
                                    SID = resolve-dsSIDFromName -objectName $_
                                }
                            }
                        } elseif ($item.key -eq "SIDS") {
                            $name = resolve-dsNameFromSID -objectSID $entity
                            @{
                                Name = $name
                                SamAccountname = $name
                                ObjectClass = "unknown"
                                SID = $entity
                            }
                        } elseif ($item.key -eq "Principals"){
                            $Filter = "(SAMAccountName=$entity)"
                            get-dsobject -server $server -LDAPfilter $Filter -properties objectClass,samaccountname | foreach-object {
                                if ($null -ne $_) {
                                    @{
                                        Name = $_.name.value
                                        objectClass = $_.objectClass.value[-1]
                                        SAMAccountName = $_.samaccountname.value
                                        SID =  [System.Security.Principal.SecurityIdentifier]::new($_.objectSID.value,0).value
                                    }
                                }
                            }
                        }
                    )
                    $resolvedObjects | foreach-object {
                        if ([string]::IsNullOrWhiteSpace($_['SID'])) {
                            throw "Problem resolving $($_['name'])"
                        }
                        [PSCustomObject]@{
                            Type        = $item.key
                            Name        = $_['name']
                            SID         = $_['SID']
                            ObjectClass = if ($_['objectClass']) {$_['objectClass']} else {"unknown"}
                            GPORef      = if ($_['SID']) { "*$($_['SID'])" } else { $_['name'] }
                            StdRef      = if ($_['SID']) { $_['SID'] } else { $_['name'] }
                            # This shouldnt be null, if we have a name we should use it.
                            NetBIOS     = if ( -not $null -eq $_.SAMAccountName) {
                                "{0}\{1}" -f $netBios, $_['SamAccountName']
                            }
                            elseif ( -not $null -eq $_['name'] ) {
                                "{0}\{1}" -f $netBios, $_['name']
                            }
                            else { $null }
                        }
                    }
                }
            }
        }
    } catch {
        Write-warning "Something bad happened"
        write-error $_

    }
    }
}
