function CreateOrSetGroup {
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$Name,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$Description,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$Path,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$GroupScope,

        [Parameter(ValueFromPipelineByPropertyName)]
        [String]$Info,

        [Parameter(ValueFromPipelineByPropertyName)]
        [String[]]$MemberOf=$null,

        [Parameter(ValueFromPipelineByPropertyName)]
        [String[]]$Members=$null,

        # Clears the 'MemberOf' attribute of the target group by removing it from other groups.
        # Typically used to remove rights that a role participates in.
        [Switch]$ResetMembership,

        # Removes all (right/role) members from this group. This only targets groups and filters by right/role names.
        # Typically used to remove all roles from a given right without disrupting e.g. service accounts.
        [Switch]$ResetMembers,

        [switch]$AdoptGroups,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    Begin{
        $CreatedGroups = [System.Collections.Generic.List[String]]::new()
        $MembershipChanges = [System.Collections.Generic.List[System.collections.hashtable]]::new()
        write-loghandler -level "Verbose" -message "Initializing OU/Container Processing (DC: $server)"
    }
    PROCESS {
        $AdoptGroups = $true
        write-loghandler -level "Debug" -message "Started processing '$name'" -indentLevel 1
        $myOut = [ordered]@{
            Status = "Pending"
            Name = $Name
            Description = $Description
        }
        $GroupParams = @{
            Description = $Description
            confirm = $false
            SAMAccountName = $name
        }
        $ScopeParams = @{
            GroupScope = $groupScope
        }

        $GroupDN = "CN=$name,$path"
        $setParam=@{identity=$groupDN}
        $newParam=@{
            Name=$name
            Path=$path
            GroupCategory="Security"
        }
        if ($info) {
            $setParam.add("replace",@{info=$info})
            $newParam.add("otherAttributes",@{info=$info})
        }


        $PSShouldProcessMsg = $(write-logHandler -passthru -target $GroupDN -message "Set Description and metadata")
        $Results = if ($PSCmdlet.ShouldProcess.invoke($PSShouldProcessMsg)) {
            try {
                if ($resetMembership) {
                    write-loghandler -level "warning" -message "Resetting membership $GroupDN"
                    get-adgroup -identity $groupDN -properties memberOf -server $server | select-object -expand memberOf | remove-adgroupmember -member $groupDN -confirm:$false -server $server
                }
                $myOut['status'] = "-membership"
                if ($resetMembers) {
                    # TODO: This filter's a bit nasty-- it doesn't account well for the rightsprefix.
                    $removals = @(get-adgroupmember -identity $groupDN -server $server   | where-object {$_.objectClass -eq 'group' -and ($_.name -like "*$($settings.Names.RightsName)-*" -or $_.name -like "*$($settings.Names.RolesName)-*")})
                    if ($removals.count -gt 0) {
                        write-loghandler -level "warning" -message "Resetting members for $GroupDN. $($removals.count) groups to be removed."
                        remove-adgroupMember -server $server -identity $groupDN -Members $Removals
                    }
                }
                $myOut['status'] = "-members"


                if ($members) {
                    add-adgroupMember -server $server -identity $GroupDN -members $members
                    $myOut['status'] = "+members"
                }

                Set-ADGroup -server $server @GroupParams @ScopeParams @setParam
                $myOut['status'] = "..Set"
                $myOut['status'] = "Updated"
            } Catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
                write-loghandler -level "Verbose" -message "     Didn't exist; creating"
                try {
                    New-ADGroup -server $server @newParam @GroupParams @ScopeParams  -passthru
                    $myOut['status'] = "NEW"
                    $CreatedGroups.add($GroupDN)
                } catch [Microsoft.ActiveDirectory.Management.ADException] {
                    if ($_.exception.InnerException.Message -eq "The supplied entry already exists.") {
                        if ($AdoptGroups ) {
                            Write-loghandler -level "info" -message "Adopting group $name."
                            get-adgroup -Identity $name | Move-ADObject -TargetPath $path
                        } else {
                            Write-loghandler -level "Warning" -message "Group $name already exists. Use AdoptGroups if you want to adopt it."
                            throw $_
                        }
                    } else {
                        throw $_
                    }
                } catch {
                    write-warning $_.exception.getType().fullname
                    write-loghandler -level "warning" -message ("Error creating {0} at {1}" -f $name,$path)
                    #throw $_
                }

            } catch [Microsoft.ActiveDirectory.Management.ADException] {
                if ($_.exception.InnerException.Message -eq "The supplied entry already exists.") {
                    if ($AdoptGroups ) {
                        Write-loghandler -level "info" -message "Adopting group $name."
                        get-adgroup -Identity $name | Move-ADObject -TargetPath $path
                    } else {
                        Write-loghandler -level "Warning" -message "Group $name already exists. Use AdoptGroups if you want to adopt it."
                        throw $_
                    }
                } elseIf ($_.exception.message -like "A * group cannot have a * group as a member." ) {
                    write-warning "Not updating scope due to scope issue!"
                    Set-ADGroup -server $server @GroupParams  @setParam
                }else {
                    throw $_
                }

            }
        }
        if ($null -ne $members -and $members.count -gt 0) {
            if ([string]::IsNullOrEmpty($groupDN)) {
                write-loghandler -level "Debug" -message "you have null members for $groupDN"
            } else {
                $MembershipChanges.add(@{
                    Identity = $GroupDN
                    Members = $members
                }) | out-null
            }
        }
        if ($null -ne $memberOf -and $memberOf.count -gt 0) {
            foreach ($g in $memberOf) {
                if ([string]::IsNullOrEmpty($g)) {
                    write-loghandler -level "Debug" -message "you have null memberofs for $groupDN"
                } else {
                    $MembershipChanges.add(@{
                        Identity = $g
                        Members = $groupDN
                    }) | out-null
                }
            }
        }
        [pscustomobject]$myOut
    }
    END{
        Write-loghandler -level "Debug" -message "Waiting for creation of new Groups"
        for ($i = 0; $i -lt $CreatedGroups.count; $i++) {
            $ProgressActivity = "Waiting for creation of new Groups ({0} / {1})" -f $($i+1), $Createdgroups.count
            $item = $CreatedGroups[$i]
            $name = $item.split(",")[0].split("=")[1]
            $status = $name
            write-Progress -id 1 -Activity $ProgressActivity -status $status  -PercentComplete (($i/$createdGroups.count) * 100)   -SecondsRemaining $Settings.AppSettings.SleepTimeout
            for ($j = 0; $j -lt $Settings.AppSettings.SleepTimeout/$Settings.AppSettings.SleepLength; $j+=$Settings.AppSettings.SleepLength) {
                $itemExists = [bool](Get-ADGroup -server $server -filter "distinguishedName -eq '$item'")
                Write-Progress -id 1 -Activity $ProgressActivity  -Status $status  -SecondsRemaining $($Settings.AppSettings.SleepTimeout - $j) -PercentComplete (($i/$createdGroups.count) * 100)
                if ($itemExists) {
                    break
                }
                start-sleep -seconds $Settings.AppSettings.SleepLength
            }
        }
        write-Progress -id 1 $progressActivity -Completed
        if ($MembershipChanges.count -gt 0) {
            write-loghandler -level "Verbose" -message "Updating memberships"
            foreach ($line in $MembershipChanges) {
                if ($PSCmdlet.ShouldProcess($line.identity,"Add child membership")) {
                    try {
                        add-adgroupMember -server $server @line
                    } catch {
                        write-warning $_.exception.getType().fullname
                        write-loghandler -level "warning" -message ("Error adding {0} to {1}" -f $line.members,$line.identity)
                        #write-loghandler -level "warning" -message "Name: $Name; Description: $Description; Path: $Path; Parent: $g"
                        throw $_
                    }
                }
            }
        }
    }
}