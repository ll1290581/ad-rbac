function Get-ADObjectGUIDs {

    $GetADObjectParams = @{
        searchBase = (Get-ADRootDSE).SchemaNamingContext
        LDAPFilter = '(SchemaIDGUID=*)'
        Properties = @("Name", "SchemaIDGUID", "AdminDescription", "ObjectClass", "ObjectGUID")
    }

    $ADObjExtParams = @{
        SearchBase = "CN=Extended-Rights,$((Get-ADRootDSE).ConfigurationNamingContext)"
        LDAPFilter = '(ObjectClass=ControlAccessRight)'
        Properties = @("Name", "RightsGUID", "DisplayName", "ObjectClass", "AppliesTo")
    }


    $objectGUIDs = $(
        get-ADObject @GetADObjectParams | select-object `
            Name,`
            @{name = "Type"; expression = { "Object" }},`
            @{name = "Description"; expression = { $_.AdminDescription }},`
            ObjectClass,`
            ObjectGUID,`
            @{name = "GUID"; expression = { [GUID]$_.SchemaIDGUID }},`
            @{name = "AppliesTo"; expression = { @{} }} | sort-object Name

        #Define the "All Properties" GUID
        <#"" | select-object `
            @{Name="Name"; expression = { "All Properties" }},`
            @{n="Type"; expression = { "Object" }},`
            @{n="Description"; expression = { "All Properties" }},`
            @{n="objectClass"; e={"none" }},`
            @{n="GUID"; expression = { [GUID]"00000000-0000-0000-0000-000000000000"}}
            #>




        # Dump the extended Rights
        get-ADObject @ADObjExtParams| select-object `
            name,`
            @{name = "Type"; expression = { "Right" }},`
            @{name = "Description"; expression = { $_.displayname }},`
            objectClass,`
            objectGUID,`
            @{name = "GUID"; expression = { [GUID]$_.RightsGUID }},`
            @{name = "AppliesTo"; expression = { $_.appliesTo }} | sort-object Name
    )

    <#if ($asHashTable) {
        $GUIDHashTable = @{
            Object = [hashtable]::new()
            Right = [hashtable]::new()
        }
        foreach ($Object in $ObjectGUIDs) {
            $GUID = $object.GUID.ToString()
            try {
                $GUIDHashTable[$object.type].add("$($Object.GUID)",$object)
            } Catch {
                $oldObject = $GUIDHashTable[$object.type][$GUID]
                write-loghandler -level "warning" -message ("`r`nCould not add entry: $($Object.GUID)")
                write-loghandler -level "warning" -message ("`tNew: {0,-40} : {1,-20} : {2}" -f $object.name, $object.objectClass, $object.Description)
                write-loghandler -level "warning" -message ("`tOld: {0,-40} : {1,-20} : {2}" -f $oldobject.name, $oldobject.objectClass, $oldobject.Description)
            }
        }
        $GUIDHashTable
    } else {
        $ObjectGUIDs
    }#>
    $ObjectGUIDs

}