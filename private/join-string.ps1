function join-string {
    Param (
        [String[]]$inputObject,
        [String]$Separator
    )
    ($inputObject | foreach-object { if ($_) {$_}}) -join $Separator
}