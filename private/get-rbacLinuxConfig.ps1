function get-RBACLinuxConfig {
        Param (
                [Parameter()]
                [String]$Name
        )
        Begin {
                $DomainInfo = get-addomain
                $DNSRoot = $DomainInfo.DNSRoot
        $config_sssd = @"
[sssd]
debug_level = 4
domains = $DNSRoot
# Allow users to log in without UPN
default_domain_suffix = $DNSRoot
config_file_version = 2
# ssh and sudo to allow use of sudoers, sshPubkey. Autofs also possible
services = nss, pam, sudo, ssh

[pam]
debug_level = 4
# For debugging. Range = 0-3
pam_verbosity = 1
pam_gssapi_services = sudo, sudo-i

[sudo]
debug_level = 5
sudo_timed = true

[autofs]
debug_level = 4

[ssh]
debug_level = 5
ssh_hash_known_hosts = true
ssh_known_hosts_timeout = 180

[pac]
debug_level = 4

[nss]
debug_level = 4

[domain/$DNSRoot]
debug_level = 4
cache_credentials = True
default_shell = /bin/bash
fallback_homedir = /home/%u@%d
id_provider = ad
# Kerberos options
krb5_realm = $DomainDNSRoot
# This is in keyring, root can get with difficulty
krb5_store_password_if_offline = True
krb5_use_fast = demand
krb5_validate = True
account_cache_expiration = 1
# Allow AD to manage access control
access_provider = ad
ad_domain = $DomainDNSRoot
ad_gpo_access_control = enforcing
ad_gpo_map_interactive = +vmtoolsd
# Dynamic DNS updates (secure)
dyndns_refresh_interval = 86400
dyndns_update_ptr = true
dyndns_update = true
dyndns_auth = gss-tsig
dyndns_refresh_interval_offset = 600
# ID mapping so uidNumber and gidNumber are not needed / used
ldap_id_mapping = True
ldap_force_upper_case_realm = True
ldap_schema = ad
ldap_user_extra_attrs = sshPublicKey:sshPublicKey,phone:telephoneNumber,email:mail
ldap_user_gecos = displayName
ldap_user_ssh_public_key = sshPublicKey
# Add linux integrations
ldap_netgroup_search_base = $($Settings.OUPaths.Netgroups)?subtree?
ldap_sudo_search_base = $($Settings.OUPaths.Sudoers)?subtree?
ldap_autofs_search_base = OU=autofs,OU=LinuxFeatures,DC=$DOMAIN?subtree?
# Fully handles group nesting so nesting-level is not needed
ldap_use_tokengroups = True
realmd_tags = manages-system joined-with-adcli
# This allows unqualified usernames, but conflicts with default domain
# use_fully_qualified_names = True
"@


        $SchemaMods = [hashtable]::new()
        $SchemaMods.add("sshPublicKey",@{
                name = "sshPublicKey"
                document = $schema_sshPublicKey
        })
        $SchemaMods.add("SudoRoles",@{
                name = "SudoRoles"
                Document = $schema_SudoRoles
        })

        if (((get-module -listavailable | where-object {$_.name -like "LAPS"}).exportedCommands.values.name) -contains "Update-LapsADSchema") {
                $SchemaMods.add("LAPS",@{
                        name = "LAPS"
                        Command = "import-module LAPS; update-LapsADSchema -verbose"
                })
        }
        if ($name) {
                $SchemaMods.getEnumerator() | where-object {$_.key -eq $name}
        } else {
                $SchemaMods.getEnumerator()
        }
}
process {}

}