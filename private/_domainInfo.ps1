
$RootDSE = [adsi]"LDAP://rootDSE"
$ADForestInfo = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest()
$ADDomainInfo = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
$ADSIDomain = [adsi]"LDAP://$($RootDSE.defaultNamingContext.value)"

$ADTable = [hashtable]::new()
$ADTable['RootDSE'] = @{
	configurationNamingContext = $rootDSE.configurationNamingContext
	defaultNamingContext = $rootDSE.defaultNamingContext
	dnsHostName = $rootDSE.dnsHostName
	domainControllerFunctionality = $rootDSE.domainControllerFunctionality
	domainFunctionality = $rootDSE.domainFunctionality  ## Convert
	dsServiceName = $rootDSE.dsServiceName
	forestFunctionality = $rootDSE.forestFunctionality  ## Convert
	highestCommittedUSN = $rootDSE.highestCommittedUSN
	isGlobalCatalogReady = $rootDSE.isGlobalCatalogReady
	isSynchronized = $rootDSE.isSynchronized
	ldapServiceName = $rootDSE.ldapServiceName
	namingContexts = $rootDSE.namingContexts
	rootDomainNamingContext = $rootDSE.rootDomainNamingContext
	schemaNamingContext = $rootDSE.schemaNamingContext
	serverName = $rootDSE.serverName
	subschemaSubentry = $rootDSE.subschemaSubentry
	supportedCapabilities = $rootDSE.supportedCapabilities
	supportedControl = $rootDSE.supportedControl
	supportedLDAPPolicies = $rootDSE.supportedLDAPPolicies
	supportedLDAPVersion = $rootDSE.supportedLDAPVersion
	supportedSASLMechanisms = $rootDSE.supportedSASLMechanisms
}
$ADTable['Forest'] = @{
    Name = $ADForestInfo.Name
	Sites = $ADForestInfo.Sites
	GlobalCatalogs = $ADForestInfo.GlobalCatalogs
	ApplicationPartitions = $ADForestInfo.ApplicationPartitions
	ForestMode = $ADForestInfo.ForestMode
	Schema = $ADForestInfo.Schema
	SchemaRoleOwner = $ADForestInfo.SchemaRoleOwner
	NamingRoleOwner = $ADForestInfo.NamingRoleOwner
	RootDomain = $ADForestInfo.RootDomain
}
$ADTable['Domain'] = @{
    Name = $ADDomainInfo.Name
	Forest = $ADDomainInfo.Forest
	DomainControllers = $ADDomainInfo.DomainControllers
	Children = $ADDomainInfo.Children
	DomainMode = $ADDomainInfo.DomainMode
	Parent = $ADDomainInfo.Parent
	PdcRoleOwner = $ADDomainInfo.PdcRoleOwner
	RidRoleOwner = $ADDomainInfo.RidRoleOwner
	InfrastructureRoleOwner = $ADDomainInfo.InfrastructureRoleOwner
    SID = (New-Object 'Security.Principal.SecurityIdentifier' ([byte[]]$ADSIDomain.objectSID.value),0).value
    DN = $($RootDSE.defaultNamingContext.value)
    Netbios = $env:UserDomain
}