function get-RBACSchemaMods {
        Param (
                [Parameter()]
                [String]$Name
        )
        #region sudoers.schema
#Source: https://github.com/lbt/sudo/blob/master/doc/schema.ActiveDirectory
$schema_SudoRoles = @'
dn: CN=sudoUser,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoUser
distinguishedName: CN=sudoUser,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.1
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoUser
adminDescription: User(s) who may run sudo
oMSyntax: 22
searchFlags: 1
lDAPDisplayName: sudoUser
name: sudoUser
schemaIDGUID:: JrGcaKpnoU+0s+HgeFjAbg==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoHost,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoHost
distinguishedName: CN=sudoHost,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.2
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoHost
adminDescription: Host(s) who may run sudo
oMSyntax: 22
lDAPDisplayName: sudoHost
name: sudoHost
schemaIDGUID:: d0TTjg+Y6U28g/Y+ns2k4w==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoCommand,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoCommand
distinguishedName: CN=sudoCommand,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.3
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoCommand
adminDescription: Command(s) to be executed by sudo
oMSyntax: 22
lDAPDisplayName: sudoCommand
name: sudoCommand
schemaIDGUID:: D6QR4P5UyUen3RGYJCHCPg==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoRunAs,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoRunAs
distinguishedName: CN=sudoRunAs,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.4
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoRunAs
adminDescription: User(s) impersonated by sudo (deprecated)
oMSyntax: 22
lDAPDisplayName: sudoRunAs
name: sudoRunAs
schemaIDGUID:: CP98mCQTyUKKxGrQeM80hQ==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoOption,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoOption
distinguishedName: CN=sudoOption,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.5
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoOption
adminDescription: Option(s) followed by sudo
oMSyntax: 22
lDAPDisplayName: sudoOption
name: sudoOption
schemaIDGUID:: ojaPzBBlAEmsvrHxQctLnA==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoRunAsUser,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoRunAsUser
distinguishedName: CN=sudoRunAsUser,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.6
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoRunAsUser
adminDescription: User(s) impersonated by sudo
oMSyntax: 22
lDAPDisplayName: sudoRunAsUser
name: sudoRunAsUser
schemaIDGUID:: 9C52yPYd3RG3jMR2VtiVkw==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoRunAsGroup,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoRunAsGroup
distinguishedName: CN=sudoRunAsGroup,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.7
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoRunAsGroup
adminDescription: Groups(s) impersonated by sudo
oMSyntax: 22
lDAPDisplayName: sudoRunAsGroup
name: sudoRunAsGroup
schemaIDGUID:: xJhSt/Yd3RGJPTB1VtiVkw==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoNotBefore,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoNotBefore
distinguishedName: CN=sudoNotBefore,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.8
attributeSyntax: 2.5.5.11
isSingleValued: TRUE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoNotBefore
adminDescription: Start of time interval for which the entry is valid
oMSyntax: 24
lDAPDisplayName:  sudoNotBefore
name: sudoNotBefore
schemaIDGUID:: dm1HnRfY4RGf4gopYYhwmw==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoNotAfter,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoNotAfter
distinguishedName: CN=sudoNotAfter,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.9
attributeSyntax: 2.5.5.11
isSingleValued: TRUE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoNotAfter
adminDescription: End of time interval for which the entry is valid
oMSyntax: 24
lDAPDisplayName:  sudoNotAfter
name: sudoNotAfter
schemaIDGUID:: OAr/pBfY4RG9dBIpYYhwmw==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn: CN=sudoOrder,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sudoOrder
distinguishedName: CN=sudoOrder,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.15953.9.1.10
attributeSyntax: 2.5.5.9
isSingleValued: TRUE
showInAdvancedViewOnly: TRUE
adminDisplayName: sudoOrder
adminDescription: an integer to order the sudoRole entries
oMSyntax: 2
lDAPDisplayName:  sudoOrder
name: sudoOrder
schemaIDGUID:: 0J8yrRfY4RGIYBUpYYhwmw==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn:
changetype: modify
add: schemaUpdateNow
schemaUpdateNow: 1
-

dn: CN=sudoRole,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: classSchema
cn: sudoRole
distinguishedName: CN=sudoRole,CN=Schema,CN=Configuration,DC=X
instanceType: 4
possSuperiors: container
possSuperiors: top
subClassOf: top
governsID: 1.3.6.1.4.1.15953.9.2.1
mayContain: sudoCommand
mayContain: sudoHost
mayContain: sudoOption
mayContain: sudoRunAs
mayContain: sudoRunAsUser
mayContain: sudoRunAsGroup
mayContain: sudoUser
mayContain: sudoNotBefore
mayContain: sudoNotAfter
mayContain: sudoOrder
rDNAttID: cn
showInAdvancedViewOnly: FALSE
adminDisplayName: sudoRole
adminDescription: Sudoer Entries
objectClassCategory: 1
lDAPDisplayName: sudoRole
name: sudoRole
schemaIDGUID:: SQn432lnZ0+ukbdh3+gN3w==
systemOnly: FALSE
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,DC=X
defaultObjectCategory: CN=sudoRole,CN=Schema,CN=Configuration,DC=X
'@
#endRegion
        #region sshPublicKey.schema
$schema_sshPublicKey = @'
dn: CN=sshPublicKey,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: attributeSchema
cn: sshPublicKey
distinguishedname: CN=sshPublicKey,CN=Schema,CN=Configuration,DC=X
instanceType: 4
attributeID: 1.3.6.1.4.1.24552.1.1.1.13
attributeSyntax: 2.5.5.5
isSingleValued: FALSE
showInAdvancedViewOnly: FALSE
adminDisplayName: sshPublicKey
adminDescription: Public Keys for SSH Access
oMSyntax: 22
searchFlags: 1
lDAPDisplayName: sshPublicKey
name: sshPublicKey
schemaIDGUID:: jjeuryF7mk6tiuwBGSpIlA==
objectCategory: CN=Attribute-Schema,CN=Schema,CN=Configuration,DC=X

dn:
changetype: modify
add: schemaUpdateNow
schemaUpdateNow: 1
-

dn: CN=ldapPublicKey,CN=Schema,CN=Configuration,DC=X
changetype: add
objectClass: top
objectClass: classSchema
cn: ldapPublicKey
distinguishedName: CN=ldapPublicKey,CN=Schema,CN=Configuration,DC=X
instanceType: 4
subClassOf: top
governsID: 1.3.6.1.4.1.24552.500.1.1.2.0
mayContain: sshPublicKey
rDNAttID: cn
showInAdvancedViewOnly: FALSE
adminDisplayName: ldapPublicKey
objectClassCategory: 3
lDAPDisplayName: ldapPublicKey
name: ldapPublicKey
schemaIDGUID:: QHQEekKp7EeyycrUmMgtaA==
systemOnly: FALSE
objectCategory: CN=Class-Schema,CN=Schema,CN=Configuration,DC=X

dn:
changetype: modify
add: schemaUpdateNow
schemaUpdateNow: 1
-

dn: CN=User,CN=Schema,CN=Configuration,DC=X
changetype: modify
add: auxiliaryClass
auxiliaryClass: ldapPublicKey
-

dn:
changetype: modify
add: schemaUpdateNow
schemaUpdateNow: 1
-
'@
#endregion
        $SchemaMods = [hashtable]::new()
        $SchemaMods.add("sshPublicKey",@{
                name = "sshPublicKey"
                document = $schema_sshPublicKey
        })
        $SchemaMods.add("SudoRoles",@{
                name = "SudoRoles"
                Document = $schema_SudoRoles
        })

        if (((get-module -listavailable | where-object {$_.name -like "LAPS"}).exportedCommands.values.name) -contains "Update-LapsADSchema") {
                $SchemaMods.add("LAPS",@{
                        name = "LAPS"
                        Command = "import-module LAPS; update-LapsADSchema -verbose"
                })
        }
        if ($name) {
                $SchemaMods.getEnumerator() | where-object {$_.key -eq $name}
        } else {
                $SchemaMods.getEnumerator()
        }
}