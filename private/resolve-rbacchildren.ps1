function resolve-rbacChildren {
    [CmdletBinding(DefaultParameterSetName='Name')]
    Param
    (
        # Common name of OU
        [Parameter(ParameterSetName="Name", mandatory, ValueFromPipelineByPropertyName)]
        [String]
        $Name,

        # Relative path from base DN to this child
        [Parameter(ParameterSetName="Name", ValueFromPipelineByPropertyName)]
        [String]
        $RelativePath="",

        # Full Distinguished name of child
        [Parameter(ParameterSetName="DN", mandatory, ValueFromPipelineByPropertyName)]
        [String]
        $DistinguishedName,

        # Description of child OU
        [Parameter(ValueFromPipelineByPropertyName)]
        [String]
        $Description,

        # Base LDAP path for relative paths
        [Parameter(mandatory, ValueFromPipelineByPropertyName)]
        [String]
        $BaseDN,

        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )

    Begin {
    }

    PROCESS {
        try {
            $SplitDN = $(
                if ($distinguishedName) {
                    $DistinguishedName
                } else {
                    if ($relativePath) {
                        # remove any number of trailing or opening commas
                        $subpath = ",{0}," -f $($relativepath -replace '(?<!\\),+$','' -replace '^,+','')
                    } else {
                        $subpath = ","
                    }
                    "OU={0}{1}{2}" -f $name, $subpath, $baseDN
                }
            ) | split-LDAPPath -asHashtable

            $thisChild = [ordered]@{
                DistinguishedName = $SplitDN['DistinguishedName']
            }
            if ($Description) {
                $thisChild.add("Description", $description)
            }
            $ReturnObject = [ordered]@{
                $splitDN['LeafName'] = [pscustomobject]$thisChild
            }
            return $ReturnObject
        } catch {
            throw $_
        }
    }
}