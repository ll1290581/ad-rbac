function Get-OUACLs {
    Param(
        [Parameter(parametersetname="Live")]
        [String]$OU,

        [Parameter(parametersetname="stored")]
        $ACLList,

        [Switch]$ShowDefaults,

        [Parameter()]
        $ObjectGUIDs = $(write-warning "This is slow!"; get-ADObjectGUIDs)
    )

    BEGIN {
    }

    Process {
        if (-not $ACLList) {
            $ACLList = (get-acl -path "AD:$OU").access
        }
        If (-not $ShowDefaults) {
            $ACLList = $ACLList | where-object {$_.IdentityReference -like "$($env:USERDOMAIN)\*"}
        }

        $ACLList | foreach-object {
            $thisObject = $_
            $AppliesTo = ($objectGUIDs | where-object {$_.GUID -eq $thisObject.InheritedObjectType}).name

            if ($thisObject.activeDirectoryRights -eq "ExtendedRight") {
                $item = ($objectGUIDs | where-object {$_.GUID -eq $thisObject.objectType -and $_.Type -eq "Right" -and ($thisObject.InheritedObjectType -eq "00000000-0000-0000-0000-000000000000" -or $_.appliesTo -contains $thisObject.inheritedObjectType)}).name
            } else {
                $item = ($objectGUIDs | where-object {$_.GUID -eq $thisObject.objectType -and $_.Type -eq "Object"}).name
            }


            $thisObject | select-object `
                @{Name = "Principal"; expression = { $_.identityReference.translate([system.security.principal.ntaccount]).value }},`
                @{name = "Rights"; expression = { $_.ActiveDirectoryRights }},`
                @{name = "AppliesTo"; expression = { $AppliesTo }},`
                @{Name = "Item"; expression = { $item }},`
                @{Name = "Access"; expression = { $_.accessControlType }},`
                @{name = "Inheritance"; expression = { $_.inheritanceType }},`
                inheritanceFlags,`
                IsInherited
        }
    }
}