function Add-OUPermissions {
    [CmdletBinding(DefaultParameterSetName = 'Normal', SupportsShouldProcess = $true)]
    Param
    (
        [Parameter(ValueFromPipelineByPropertyName, ValueFromPipeline)]
        #[ValidateScript( {get-adorganizationalUnit -identity $_ })]
        [String]$Path,

        [Parameter(ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [System.Security.AccessControl.AuthorizationRuleCollection]$ACEList,

        # Rebuild from a Base ACL
        [Parameter(ParameterSetName = "Rebuild", Mandatory)]
        [switch]
        $Rebuild

    )
    Begin {
        $dsinfo = get-dsinfo
        $allowedRebuildTypes = @(
            "organizationalUnit"
        )
        $ForbiddenRebuildPaths = [System.Collections.ArrayList]::new()
        $dsinfo.namingcontexts.where({ $_ -ne $dsinfo.rootDomainNamingContext }) | foreach-object {
            [void]$ForbiddenRebuildPaths.add("*$_")
        }
        [void]$ForbiddenRebuildPaths.add( "*CN=System,$($dsinfo.rootDomainNamingContext)")
        [void]$ForbiddenRebuildPaths.add( "OU=Domain Controllers,$($dsinfo.rootDomainNamingContext)")
        [void]$ForbiddenRebuildPaths.add( "CN=Users,$($dsinfo.rootDomainNamingContext)")
        [void]$ForbiddenRebuildPaths.add( "CN=Computers,$($dsinfo.rootDomainNamingContext)")
        [void]$ForbiddenRebuildPaths.add( "CN=Builtin,$($dsinfo.rootDomainNamingContext)")
        [void]$ForbiddenRebuildPaths.add( "CN=keys,$($dsinfo.rootDomainNamingContext)")
        # DNSAdmins can be 1101, and another one 1102. While this could skip some  ACLs to purge, that only happens on protected paths and should be extremely rare.
        $MinSIDRIDToPurge = 1103
    }

    Process {
        try {
            $pathType = ((get-dsobject -DistinguishedName $path).objectClass)[-1]
            $CurrentACL= get-acl -path "AD:$Path"
            write-loghandler -level "Info" -message "Processing DACLs" -target $path
            if ($rebuild) {
                write-loghandler -level "Verbose" -message "Rebuilding DACLs" -target $path
                if ($rebuild -and (-not $pathType -in $allowedRebuildTypes -or [bool]($ForbiddenRebuildPaths.Where({ $path -like $_ })) )) {
                    write-loghandler -level "warning" -message "Rebuild requested but path is forbidden or not an allowed type." -suppressCaller -indentlevel 1
                    write-loghandler -level "warning" -message "Instead of full rebuild, only removing rules for non-built-in principals." -suppressCaller -indentlevel 1
                    # We do two foreach cycles to minimize traffic for SID resolution (Does this create traffic)
                    $IdentitiesToPurge = $CurrentACL.access.identityReference | sort-object -unique | foreach-object {
                        $sid = $_.translate([System.Security.Principal.SecurityIdentifier])
                        if ($null -ne $sid.accountDomainSID -and [int]$sid.value.replace("$($sid.accountDomainSID)-", "") -ge $MinSIDRIDToPurge) {
                            write-loghandler -level "Verbose" -message "Purging rules for: $identity" -suppressCaller -indentlevel 2
                            $_
                        }
                    }
                    $BaseACL = [System.DirectoryServices.ActiveDirectorySecurity]::new()
                    # We need to do an add because we want to be able to compare old vs new object, and ACLs don't seem cloneable
                    foreach ($rule in $currentACL.access) {
                        if ($rule.identityReference -notIn $IdentitiesToPurge) {
                            $BaseACL.AddAccessRule($rule)
                        } else{
                            write-loghandler -level "Debug" -suppressCaller -message "Skipping rule (purgeable identity: $($rule.IdentityReference))" -indentLevel 2
                        }
                    }
                    $Action = "{0,-20} ({1,3} incoming; {2,3} purged)" -f "Rebuilding*** rules", $aceList.count, $($currentACL.Access.count - $BaseACL.access.count)
                } else {
                    $BaseACL = (get-dsschemaClass -objectName "Organizational-unit").defaultSecurityDescriptor
                    $Action = "{0,-20} ({1,3} incoming; ({2,3} Default rules)" -f "Rebuilding DACL", $aceList.count, $BaseACL.count
                }
            } else {
                $baseACL = get-acl -path "AD:$Path"
                $Action = "{0,-20} ({1,3} incoming)" -f "Adding Rules", $aceList.count
            }
            foreach ($Rule in $ACEList) {
                $BaseACL.AddAccessRule($Rule)
            }
            $message = write-loghandler -level Info -message $Action -target $path -passthru
            if ($PSCmdlet.ShouldProcess.invoke($($message))) {
                write-host (get-dsacls -acllist $AceLIST  | format-Table |out-string)
                $targetObject = [adsi]"LDAP://$path"
                $targetObject.getInfo()
                $targetObject.get_Options().securityMasks = [System.DirectoryServices.SecurityMasks]::Dacl
                $targetobject.objectSecurity = $baseACL
                $targetObject.setinfo()
                #set-ACL -path "AD:$Path" -ACLObject $baseACL | out-null
            }
        }
        catch {
            write-warning $_.exception.getType().fullname
            $_ | format-list * -force
            write-loghandler -level "warning" -message "WHOOPS"
        }
    }
}


