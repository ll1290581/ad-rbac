Function get-currentUserGroups {
    [System.Security.Principal.WindowsIdentity]::GetCurrent().groups | foreach-object {
        [PSCustomObject]@{
            Name = $_.Translate([System.Security.Principal.NTAccount])
            SID = $_.value
        }
    }
}