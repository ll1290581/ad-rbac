# This doesnt work outside of functions
$moduleData = $MyInvocation.MyCommand.Module
$RootDSE = [ADSI]"LDAP://RootDSE"
$Domain = [adsi]"LDAP://$($RootDSE.defaultNamingContext)"
$Runtime = [ordered]@{
    Date   = $(Get-Date)
    RootDSE = [ordered]@{
        DefaultDC = $RootDSE.dnsHostName.value
        ConfigurationNamingContext = $rootDSE.configurationNamingContext.value
    }
    Domain = [ordered]@{
        DefaultDC                  = $RootDSE.dnsHostName.value
        DN                         = $domain.distinguishedname.value
        DNSRoot                    = $_.DNSRoot
        Netbios                    = $_.NetbiosName
        ConfigurationNamingContext = $rootDSE.configurationNamingContext.value
    }
    Stamp  = [ordered]@{
        SID  = [System.Security.Principal.WindowsIdentity]::GetCurrent().user.value
        Name = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
        Time = $(Get-Date).ToFileTimeUtc()
    }
}

$Defaults = [ordered]@{}

$Defaults['Names'] = [ordered]@{
    SettingsNames = [ordered]@{
        AppSettings = "AppSettings"
        API         = "API"
        FilePaths   = "FilePaths"
        OUPaths     = "OUPaths"
        Templates   = "Templates"
        Metadata    = "Metadata"
        Names       = "Names"
        Rights      = "Rights"
        sudodefs    = "sudodefs"
    }
}
$Defaults['Names'] = [ordered]@{
    __Metadata         = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['Names']
        Description = "AD Object Naming"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    SettingsNames      = $Defaults['Names']['SettingsNames']
    SettingsObject     = "Metadata"
    SettingsContainer  = "PS.RAD.Tenants"
    SysvolSettingsDir  = "PS.RAD"
    Tenant             = "RAD"
    GlobalOU           = "Global"
    OrgsOU             = "Orgs"
    ComponentsOU       = "Components"
    RightsOU           = "Rights"
    RightsName         = "Right"
    RightsPrefix       = "$([char]0x2756)" # ❖
    RightsProtected    = "$([char]0x26D4)" # ⛔
    RolesOU            = "Roles"
    RolesName          = "Role"
    RolesSuffix        = "$([char]0x260D)" # ☍
    RoleProtected      = "$([char]0x26A1)" # ⚡
    UsersBase          = "Users"
    UnprivUsersOU      = "Standard"
    UsersTier0OU       = "SuperAdmins"
    EndpointsOU        = "Endpoints"
    ServiceAcctOU      = "ServiceAccounts"
    DefaultComputersOU = "_Unassigned"
    EndpointPAWs       = "PAWs"
    EndpointPKI        = "PKI"
    LinuxFeaturesOU    = "LinuxFeatures"
    NetgroupsOU        = "Netgroups"
    SudoersOU          = "Sudoers"
    AppRightPrefix     = "App"
    RolesList          = @{
        Admin             = "Admin"
        Owner             = "Owner"
        Operator          = "Operator"
        User              = "User"
        AppAdmin          = "App-Admin"
        GPOAdmin          = "GPO-Admin"
        AccountAdmin      = "Account-Admin"
        RBACAdmin         = "RBAC-Admin"
        PKIAdmin          = "PKI-Admin"
        OrgAdmin          = "Orgs-Admin"
        SvcLocal          = "Svc-Local"
        SvcRemoteAdmin    = "Svc-Remote-Admin"
    }
    GPOs               = @{
        PrefixHigh = "_HBAC"
        PrefixLow  = "_Settings"
    }
}

$Defaults['Rights'] = [ordered]@{
    __Metadata            = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['Rights']
        Description = "'Rights' Security group names"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    AppAccess             = @{
        nameSuffix  = "{0}-access" -f $Defaults['Names']['AppRightPrefix']
        Description = "Allowed Log in to applications (e.g. Web UIs)."
        AddParents  = $true
    }
    AppPoweruser          = @{
        nameSuffix  = "{0}-modify" -f $Defaults['Names']['AppRightPrefix']
        Description = "Poweruser or modify access to applications (e.g. Web UIs)"
        AddParents  = $true
    }
    AppAdmin              = @{
        nameSuffix  =  "{0}-admin" -f $Defaults['Names']['AppRightPrefix']
        Description = "Admin access to applications (e.g. Web UIs)"
        AddParents  = $true
    }
    AppAccessComponent             = @{
        nameSuffix  = "{0}-access" -f $Defaults['Names']['AppRightPrefix']
        Description = "Allowed Log in to applications (e.g. Web UIs)."
        Parent='AppAccess'
        AddParents  = $true
        DoNotPrefixGroupName = $true
    }
    AppPoweruserComponent          = @{
        nameSuffix  = "{0}-modify" -f $Defaults['Names']['AppRightPrefix']
        Description = "Poweruser or modify access to applications (e.g. Web UIs)"
        Parent='AppPoweruser'
        AddParents  = $true
        DoNotPrefixGroupName = $true
    }
    AppAdminComponent              = @{
        nameSuffix  =  "{0}-admin" -f $Defaults['Names']['AppRightPrefix']
        Description = "Admin access to applications (e.g. Web UIs)"
        parent='AppAdmin'
        AddParents  = $true
        DoNotPrefixGroupName = $true
    }
    LAPS                  = @{
        nameSuffix  = "LAPSReadPassword"
        Description = "Fetch local machine passwords via Windows LAPS"
        AddParents  = $true
    }

    CreateDeleteComputer  = @{
        nameSuffix  = "AddEndpoint"
        Description = "Allowed to create / join computer objects"
    }

    UserCreate            = @{
        nameSuffix  = "UserCreate"
        Description = "Create standard users"
    }
    UserControl           = @{
        nameSuffix  = "UserControl"
        Description = "Allowed to enable / disable / delete users"
    }
    UserPasswdReset       = @{
        nameSuffix  = "UserReset"
        Description = "Reset passwords for standard users"
    }
    AdminUserCreate       = @{
        nameSuffix  = "AdminCreate"
        Description = "Create sensitive / administrative accounts"
    }
    AdminUserControl      = @{
        nameSuffix  = "AdminControl"
        Description = "Allowed to enable / disable / Delete sensitive accounts"
    }
    AdminUserPasswdReset  = @{
        nameSuffix  = "AdminReset"
        Description = "Reset passwords for sensitive / Administrative accounts"
    }

    DNSOperator           = @{
        nameSuffix  = "DNSOperator"
        Description = "Rights to manage DNS server. Built in delegations on MicrosoftDNS node in AD."
    }
    DHCPAdmin             = @{
        nameSuffix  = "DHCPAdmin"
        Description = "Authorize and manage DHCP servers."
    }
    DFSNamespaceManage    = @{
        nameSuffix = "DFSNamespaceManage"
        Description= "Manage DFS Namespaces for domain"
    }
    DFSReplCreate          = @{
        nameSuffix = "DFSCreateReplGroup"
        Description= "Permissions to create new DFS-R Groups. Permissions will need to be manually added to new Repl groups."
    }

    WinAdmin              = @{
        nameSuffix  = "WindowsAdmin"
        Description = "Local admin rights on Windows / Linux hosts"
    }
    WinOps                = @{
        nameSuffix  = "WindowsOps"
        Description = "Limited operator rights on Windows hosts: event log, performance monitoring, network changes"
    }
    SeTcbPrivilege        = @{
        nameSuffix  = "SeTcbPrivilege"
        Description = "'Act as part of operating system' rights. Usually only needed for services."
    }
    LogonLocal            = @{
        nameSuffix  = "LogonLocal"
        Description = "Rights for Local logon"
        info        = "Default mapped services: su, gdm, login\r\nWindows: Task Scheduler"
    }
    LogonRemote           = @{
        nameSuffix  = "LogonRemote"
        Description = "Allow log on through Remote Desktop Services /SSH."
        info        = @(
            "Default mapped services: sshd, cockpit\r\nWindows: Remote Desktop"
        )
    }
    LogonBatch            = @{
        nameSuffix  = "LogonBatch"
        Description = "Rights for batch logon / scheduled task / cron access"
        info        = "Default mapped services: crond\r\nWindows: Task Scheduler"
    }
    LogonService          = @{
        nameSuffix  = "LogonService"
        Description = "Rights for logon as service in this Org"
        info        = "Default mapped services: \u003cnot set\u003e\r\nWindows: Services"
    }

    DenyRemote            = @{
        nameSuffix  = "DenyRemote"
        Description = "Denied login via SSH / RDP"
    }
    DenyService           = @{
        nameSuffix  = "DenyService"
        Description = "Denied service / batch login"
    }

    PKIIssue              = @{
        nameSuffix  = "PKI-Issue"
        Description = "Approve and revoke certificates"
    }
    PKIManageCA           = @{
        nameSuffix  = "PKI-ManageCA"
        Description = "Manage CA settings and cert templates"
    }
    PKIEnroll             = @{
        nameSuffix  = "PKI-RequestCert"
        Description = "Request certificates"
    }
    GenerateSecurityAudit = @{
        nameSuffix  = "GenerateSecurityAudit"
        Description = "Primarily for use by ADFS and IIS service accounts. Granted via GPO"
    }

    ManageRoles           = @{
        nameSuffix  = "Roles-Manage"
        Description = "Create and delete new roles, and modify membership of roles."
    }
    ManageRights          = @{
        nameSuffix  = "Rights-Admin"
        Description = "Create and delete new rights, and modify membership of all groups."
    }

    ServiceAccountMSA     = @{
        nameSuffix  = "ServiceAcct-MSA"
        Description = "Create, Delete, and manage sMSA and gMSAs"
    }
    ServiceAccountLegacy  = @{
        nameSuffix  = "ServiceAcct-Legacy"
        Description = "Create, Delete, and manage legacy service accounts"
    }

    OrgManage             = @{
        nameSuffix  = "OrgManage"
        Description = "Create and delete Orgs, as well as update their DACLs. Implicit full control of all orgs."
    }
    OrgDelete             = @{
        nameSuffix  = "OrgDelete"
        Description = "Delete orgs and all subordinate objects"
    }
    GPOCreate              = @{
        nameSuffix  = "GPOCreate"
        Description = "Allowed to create new GPOs. Replaces GPOCreatorsOwners, but without rights to default domain GPOs"
    }
    GPOAudit              = @{
        nameSuffix  = "GPOAudit"
        Description = "Allowed to run RSOP and GPO Modelling"
    }
    GPOLink               = @{
        nameSuffix  = "GPOLink"
        Description = "Allowed to link and unlink GPOs in org"
    }
    GPOEdit               = @{
        nameSuffix  = "GPOEdit"
        Description = "Edit rights on All GPOs"
    }

    OUCreate              = @{
        nameSuffix  = "OUCreate"
        Description = "Create and update arbitrary Organizational Units"
    }
    OUManage              = @{
        nameSuffix  = "OUManage"
        Description = "Manage arbitrary OUs, properties and permissions on ACLs"
    }
    SudoManager           = @{
        nameSuffix  = "SudoManager"
        Description = "Rights to Create and modify Sudoroles and Netgroups. This allows gaining sudo rights on arbitrary systems."
    }
}


$defaults['Sudodefs'] = [ordered]@{
    __Metadata = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['sudodefs']
        Description = "Sudoers definitions"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    Roles      = @{
        LinuxSudoFull       = @{
            Name        = "sudo_full"
            description = "Linux Sudo: Unrestricted / full root."
            sudoCommand = @(
                "ALL"
            )
        }
        LinuxSudoFullNoAuth = @{
            Name        = "sudo_full-nopw"
            description = "Linux Sudo (no passwd): Unrestricted / full root"
            sudoOption  = "!authenticate"
            sudoCommand = @(
                "ALL"
            )
        }
        LinuxSudoOperate    = @{
            Name        = "sudo_operate"
            description = "Linux Sudo: Read logs, manage services"
            sudoOption  = "!authenticate"
            sudoCommand = @(
                "/usr/bin/dmesg",
                "/usr/bin/journalctl",
                "/usr/sbin/aureport",
                "/usr/sbin/ausearch",
                "/usr/bin/tail /var/log/audit/*"
                "/usr/bin/systemctl start *",
                "/usr/bin/systemctl restart *",
                "/usr/bin/systemctl reload *",
                "/usr/bin/systemctl status *"
                "/usr/bin/systemctl reboot"
                "/usr/sbin/tcpdump",
                "/usr/bin/reboot"
            )
        }
        LinuxSudoConfigure  = @{
            Name        = "sudo_configure"
            description = "Linux sudo: change network settings, time, firewall, and installed packages"
            sudoCommand = @(
                "/usr/bin/kill",
                "/usr/bin/killall",
                "/usr/bin/nmtui",
                "/usr/bin/nmcli",
                "/usr/bin/hostnamectl",
                "/usr/bin/resolvectl",
                "/usr/sbin/ip",
                "/usr/bin/firewall-cmd",
                "/usr/sbin/dhclient",
                "/usr/sbin/ifconfig",
                "/usr/sbin/route",
                "/usr/sbin/tcpdump",
                "/usr/bin/systemctl start *",
                "/usr/bin/systemctl stop *",
                "/usr/bin/systemctl enable *",
                "/usr/bin/systemctl disable *",
                "/usr/bin/systemctl restart *",
                "/usr/bin/systemctl reload *",
                "/usr/bin/systemctl status *"
                "/usr/bin/systemctl reboot"
                "/usr/bin/dnf",
                "/usr/bin/yum",
                "/usr/bin/rpm",
                "/usr/bin/reboot"
            )
        }
    }
}
foreach ($role in $defaults['sudoDefs']['Roles'].getEnumerator()){
    $defaults['Rights'].add($role.key,
        [ordered]@{
            nameSuffix           = $role.value['name']
            Description          = $role.value['description']
            info                 = "Allowed sudoCommands:`r`n" + $role.value['sudoCommand'] -join "`r`n"
            AddParents           = $false
            DoNotPrefixGroupName = $false
        }
    )
}

$Defaults['DACLs'] = [ordered]@{
    # 'AppliesTo' is InheritedObjectType. It's the object type we're applying the ACL to.
    # 'TargetObject' is the attribute. If ADRight is 'create/delete child', then TargetObject is the type being created.
    GroupControl = @(
        @{
            ADRight         = "CreateChild, DeleteChild"
            TargetObject    = "Group"
            InheritanceType = "All"
        }
        @{
            ADRight         = "WriteProperty, ReadProperty"
            AppliesTo    = "Group"
            InheritanceType = "All"
        }
        @{
            ADRight         = "WriteProperty"
            TargetObject    = @("Description", "Member")
            AppliesTo       = "Group"
            InheritanceType = "All"
        }
    )
    AddComputer = @(
        # This also needs to be fixed to apply only at endpoints.
        # These permissions are broader than they should be. Look into restricting, but following properties may be needed:
        ## Common-name, Sam-Account-name, Description, Display-name, attributeCertificateAttribute, Service-Principal-Name, DNS-Host-name
        ## See also: https://learn.microsoft.com/en-us/answers/questions/973272/delegate-help-desk-users-permission-to-move-users
        @{
            ADRight         = "CreateChild, DeleteChild"
            TargetObject    = "Computer"
            InheritanceType = "All" # Any other inheritance type will cause access errors on attempting to move computer objects
        }
        @{
            ADRight         = "Self, WriteProperty, GenericRead"
            TargetObject    = "Computer"
            InheritanceType = "Descendents"
        }
        @{
            ADRight       = "ExtendedRight"
            ExtendedRight = "User-Force-Change-Password"
            AppliesTo     = "Computer"
            InheritanceType = "Descendents"
        }
    )
    AddComputerAncestor = @(
        @{
            ADRight         = "CreateChild, DeleteChild"
            TargetObject    = "Computer"
            InheritanceType = "Descendents"        }
        @{
            ADRight         = "Self, WriteProperty, GenericRead"
            TargetObject    = "Computer"
            InheritanceType = "Descendents"
        }
        @{
            ADRight       = "ExtendedRight"
            ExtendedRight = "User-Force-Change-Password"
            AppliesTo     = "Computer"
            InheritanceType = "Descendents"
        }
    )
    OUCreate = @(
        @{
            ADRight         = "CreateChild"
            TargetObject    = @("Organizational-Unit","Container")
            InheritanceType = "All"
        }
        @{
            ADRight         = "WriteProperty"
            TargetObject    = "Description"
            AppliesTo       = @("Organizational-Unit","Container")
            InheritanceType = "All"
        }
    )
    OUManage = @(
        @{
            ADRight         = "WriteProperty"
            TargetObject    = "Description"
            AppliesTo       = @("Organizational-Unit","Container")
            InheritanceType = "All"
        }
        @{
            ADRight         = "WriteDacl"
            AppliesTo       = @("Organizational-Unit","Container")
            InheritanceType = "All"
        }
        @{
            ADRight         = "DeleteChild"
            TargetObject    = @("Organizational-Unit","Container")
            InheritanceType = "All"
        }
    )
    GPOLink = @(
        # TODO: This needs to become 'none' and instead have DACLs applied from parents so that GPOs cant be linked in wrong places. This needs to become a general pattern.
        @{
            ADRight         = "ReadProperty, WriteProperty"
            TargetObject    = "GP-Link"
            InheritanceType = "All"
        }
    )
    GPOEdit = @(
        @{
            ADRight         = "ReadProperty, WriteProperty"
            TargetObject    = "GP-Options"
            InheritanceType = "All"
        }
    )
    GPOAudit =  @(
        @{
            ExtendedRight   = "Generate-RSoP-Logging"
            InheritanceType = "All"
        }
    )
    LAPS = @(
        @{ # Not sure why this isn't included in the lapspsh.dll, but seems necessary. Also needed to enable LAPS RBAC right.
            ADRight         = "ReadProperty, ExtendedRight"
            TargetObject    = "ms-LAPS-EncryptedPassword"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
        @{ # Not sure why this isn't included in the lapspsh.dll, but seems necessary. Also needed to enable LAPS RBAC right.
            ADRight         = "ReadProperty, ExtendedRight"
            TargetObject    = "ms-LAPS-EncryptedPasswordHistory"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
        @{ # Not necessary for LAPS SELF, but useful for our purposes to ensure read/write. Currently no reason to allow write but not read, though this could change if more rights are added.
            ADRight         = "ReadProperty, ExtendedRight"
            TargetObject    = "ms-LAPS-Password"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
        @{ # lapspsh.dll SetLapsADComputerSelfPermission Lines 76-79
            ADRight         = "ReadProperty, WriteProperty"
            TargetObject    = "ms-LAPS-PasswordExpirationTime"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
        @{ # lapspsh.dll SetLapsADComputerSelfPermission Lines 80-81
            ADRight         = "WriteProperty"
            TargetObject    = "ms-LAPS-Password"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
        @{ # lapspsh.dll SetLapsADComputerSelfPermission Lines 82-87
            ADRight         = "ReadProperty, WriteProperty, ExtendedRight"
            ExtendedRight   = "ms-LAPS-Encrypted-Password-Attributes"
            AppliesTo       = "Computer"
            InheritanceType = "Descendents"
        }
    )
    ServiceAccountMSA =  @(
        @{
            ADRight         = "CreateChild"
            TargetObject    = @("ms-DS-Managed-Service-Account", "ms-DS-Group-Managed-Service-Account")
            InheritanceType = "Descendents"
        }
        @{
            ADRight         = "DeleteChild"
            TargetObject    = @("ms-DS-Managed-Service-Account", "ms-DS-Group-Managed-Service-Account")
            InheritanceType = "All"
        }
        @{
            ADRight         = "GenericAll"
            AppliesTo       = @("ms-DS-Managed-Service-Account", "ms-DS-Group-Managed-Service-Account")
            InheritanceType = "All"
        }
    )
    ServiceAccountLegacy = @(
        @{
            ADRight         = "CreateChild"
            TargetObject    = "User"
            InheritanceType = "Descendents"
        }
        @{
            ADRight         = "DeleteChild"
            TargetObject    = "User"
            InheritanceType = "All"
        }
        @{
            ADRight         = "GenericAll"
            AppliesTo       = "User"
            InheritanceType = "All"
        }
    )
    UserCreate = @(
        @{
            ADRight      = "CreateChild"
            TargetObject = "User"
            InheritanceType = "All"
        }
    )
    UserControl = @(
        @(
            @{
                ADRight      = "ReadProperty, WriteProperty"
                TargetObject = "User-Account-Control"
                AppliesTo    = "User"
            }
            @{
                ADRight      = "DeleteChild"
                TargetObject = "User"
            }
        )
    )
    UserPasswdReset = @(
        @{
            ADRight      = "ReadProperty, WriteProperty"
            TargetObject = "Pwd-Last-Set"
            AppliesTo    = "User"
        }
        @{
            ADRight      = "ReadProperty, WriteProperty"
            TargetObject = "Lockout-Time"
            AppliesTo    = "User"
        }
        @{
            ADRight       = "ExtendedRight"
            ExtendedRight = "User-Force-Change-Password"
            AppliesTo     = "User"
        }
    )
    DNSOperator = @(
        @{
            ADRight         = "ReadProperty", "WriteProperty"
            TargetObject    = "dnsAllowDynamic","dnsAllowXFR","dNSHostName","dnsNotifySecondaries","dNSProperty","dnsRecord","dnsRoot","dnsSecureSecondaries","dNSTombstoned"
            InheritanceType = "All"
        }
        @{
            ADRight         = "CreateChild", "DeleteChild", "GenericRead"
            TargetObject    = "dnsZone", "DnsZoneScopeContainer", "dnsNode", "dnsZoneScope"
            InheritanceType = "All"
        }
        @{
            ADRight         = "GenericRead","ExtendedRight"
            InheritanceType = "None"
        }
        # 'Self' permission seems aligned with "ExtendedWrite" which is associated with DNSAdmin escalation via DLL injection
        @{
            ADRight         = "WriteOwner, WriteDACL, Self"
            InheritanceType = "All"
            Action          = "Deny"
        }
    )
    DFSReplCreate       = @(
        @{
            ADRight         = "CreateChild"
            TargetObject    = "ms-DFSR-ReplicationGroup"
            InheritanceType = "None"
        }
    )
    DFSNamespaceManage = @(
        @{
            ADRight         = "CreateChild", "DeleteChild"
            targetObject    = "ms-DFS-Namespace-Anchor", "ms-DFS-Namespace-v2"
            InheritanceType = "None"
        }
        @{
            ADRight         = "GenericAll"
            AppliesTo    = "ms-DFS-Namespace-Anchor", "ms-DFS-Namespace-v2"
            InheritanceType = "Descendents"
        }
    )
    DHCPAdmin = @(
        @{
            ADRight         = "CreateChild, deleteChild"
            TargetObject    = "dHCP-Class"
            InheritanceType = "All"
        }
        @{
            ADRight         = "GenericAll"
            Appliesto       = "dHCP-Class"
            InheritanceType = "All"
        }
    )
    PKITemplateManage = @(
        @{
            ADRight = "ReadProperty, WriteProperty, ExtendedRight, GenericRead, WriteDacl, WriteOwner"
            Appliesto       = "PKI-Certificate-Template"
            InheritanceType = "None" #Inheritance doesn't work on PKI templates, so we're going to use a query to enumerate and set DACLs directly on object (TODO: Verify the inheritance thing again...)
        }
    )
    PKIEnroll = @(
        @{
            ADRight         = "ReadProperty, ExtendedRight"
            ExtendedRight   = "Certificate-Enrollment"
            InheritanceType = "None"
        }
    )
    PKIManageOIDs = @(
        @{
            ADRight         = "CreateChild, DeleteChild" # ReadProperty, GenericRead, WriteDacl
            TargetObject    = "ms-PKI-Enterprise-Oid"
            InheritanceType = "None"
        }
        @{
            ADRight         = "ReadProperty, WriteProperty, GenericRead, WriteDacl, WriteOwner"
            AppliesTo       = "ms-PKI-Enterprise-Oid"
            InheritanceType = "Descendents"
        }
    )

}

$Defaults['AppSettings'] = [Ordered]@{
    __Metadata             = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['AppSettings']
        Description = "Application Settings"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    SleepTimeout           = 30
    SleepLength            = 3
    RightScope             = "DomainLocal"
    RoleScope              = "Global"
    DefaultDenyObjectTypes = @(
        "Organizational-Unit"
        "User"
        "Computer"
        "Group"
        "ms-DS-Group-Managed-Service-Account"
        "ms-DS-Managed-Service-Account"
        "ms-Imaging-PSPs"
        "msMQ-Custom-Recipient"
    )
}

$Defaults['OUPaths'] = [ordered]@{
    __Metadata        = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['OUPaths']
        Description = "Locations of centrally stored SMB resources"
        ChangeLog   = @("Current model arch has Global == TenantRoot, with Linux features + orgs directly under it")
        Version     = 2
        MinVersion  = 1
    }
    SettingsContainer = "CN={0},CN={1},CN=Program Data,{2}" -f $Defaults['Names']['Tenant'], $Defaults['Names']['SettingsContainer'], $Runtime['domain'].dn
    TenantRoot        = "OU={0},{1}" -f $Defaults['Names']['Tenant'], $Runtime['domain'].dn
}
$Defaults['OUPaths'] += [ordered]@{
    Global            = "{0}" -f $Defaults['OUPaths']['TenantRoot']
    OrgsBase          = "OU={0},{1}" -f $Defaults['Names']['OrgsOU'], $Defaults['OUPaths']['TenantRoot']
    LinuxFeaturesBase = "OU={0},{1}" -f $Defaults['Names']['LinuxFeaturesOU'], $Defaults['OUPaths']['TenantRoot']
}
$Defaults['OUPaths'] += [ordered]@{
    GlobalEndpoints = "OU={0},{1}" -f $defaults['Names']['EndpointsOU'], $defaults['OUPaths']['Global']
}
$Defaults['OUPaths'] += [ordered]@{
    UsersBase = "OU={0},{1}" -f $defaults['Names']['UsersBase'], $Defaults['OUPaths']['Global']
}
$Defaults['OUPaths'] += [ordered]@{
    DefaultUsers     = "OU={0},{1}" -f $defaults['Names']['UnprivUsersOU'], $Defaults['OUPaths']['UsersBase']
    PrivilegedUsers  = "OU={0},{1}" -f $defaults['Names']['UsersTier0OU'], $Defaults['OUPaths']['UsersBase']
    DefaultComputers = "OU={0},{1}" -f $defaults['Names']['DefaultComputersOU'], $defaults['OUPaths']['GlobalEndpoints']
    PAWs             = "OU={0},{1}" -f $defaults['Names']['EndpointPAWs'], $defaults['OUPaths']['GlobalEndpoints']
    PKI              = "OU={0},{1}" -f $defaults['Names']['EndpointPKI'], $defaults['OUPaths']['GlobalEndpoints']
    GlobalRights     = "OU={0},{1}" -f $defaults['Names']['RightsOU'], $Defaults['OUPaths']['Global']
    GlobalRoles      = "OU={0},{1}" -f $defaults['Names']['RolesOU'], $Defaults['OUPaths']['Global']
    LinuxSudoers     = "OU={0},{1}" -f $Defaults['Names']['SudoersOU'], $Defaults['OUPaths']['LinuxFeaturesBase']
    LinuxNetgroups   = "OU={0},{1}" -f $Defaults['Names']['NetGroupsOU'], $Defaults['OUPaths']['LinuxFeaturesBase']
}
# Fixup for any paths where the first element is null
$itemsToFix = $Defaults['OUPaths'].GetEnumerator() | Where-Object { $_['value'] -match "(.*)(OU=,)(.*)" }
$itemsToFix | ForEach-Object {
    write-loghandler -level "warning" -message "Fixing OUPaths: $($_.key) (this is a settings issue)"
    $defaults['oupaths'][$_['key']] = $defaults['oupaths'][$_['key']] -replace "OU=,", ""
}

$Defaults['API'] = [Ordered]@{
    __Metadata = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['API']
        Description = "Code compatibility / Change notes"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
}

$Defaults['FilePaths'] = [ordered]@{
    __Metadata        = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['FilePaths']
        Description = "Locations of centrally stored SMB resources"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    BaseSysvolRADPath = "\\{0}\SYSVOL\{0}\{1}" -f $($Runtime['Domain']['dnsroot']), $Defaults['Names']['SysvolSettingsDir']
}
$Defaults['FilePaths'] += [ordered]@{
    PrimordialTemplates = "{0}\PrimordialTemplates" -f $Defaults['FilePaths']['BaseSysvolRADPath']
    JSONTemplates       = "{0}\JSONTemplates" -f $Defaults['FilePaths']['BaseSysvolRADPath']
    LDIFImports         = "{0}\LDIF_Imports" -f $Defaults['FilePaths']['BaseSysvolRADPath']
    Scripts             = "{0}\Scripts" -f $Defaults['FilePaths']['BaseSysvolRADPath']
    SettingsFile        = "{0}\settings.json" -f $Defaults['FilePaths']['BaseSysvolRADPath']
}


$Defaults['Templates'] = [ordered]@{
    __Metadata = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['Templates']
        Description = "Compatibility of DACLs, GPO, and group templates"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    Global     = [ordered]@{}
    Orgs       = [ordered]@{}
    Component  = [ordered]@{}
}

$Defaults['Metadata'] = [ordered]@{
    __Metadata     = [ordered]@{
        Name        = $Defaults['Names']['SettingsNames']['Metadata']
        Description = "Core Meta Settings"
        ChangeLog   = @("Initial Release")
        Version     = 2
        MinVersion  = 1
    }
    URL            = ""
    OID            = ""
    InstallDate    = $Runtime['Date'].ToLongDateString() + " @ " + $Runtime['Date'].ToLongTimeString()
    InstallVersion = ""
}
$Defaults['Metadata'] += @{
    Settings = [ordered]@{}
    Modules  = [ordered]@{
        Core = @{
            Version        = ""
            OID            = $Runtime['Stamp'].Time
            InstallDate    = $Runtime['date'].ToLongDateString() + " @ " + $Runtime['date'].ToLongTimeString()
            SettingsObject = "CN={0},{1}" -f $Defaults['Names']['SettingsObject'], $Defaults['OUPaths']['SettingsContainer']
        }
    }
}


foreach ($setting in $Defaults['Names']['SettingsNames'].getEnumerator()) {
    $Defaults.$($setting.key).__Metadata.Stamp = $Runtime['Stamp']
    $Defaults['Metadata']['Settings'][$($Setting.key)] = [ordered]@{
        ActiveVersion = $Defaults[$($setting.key)]['Version']
        OID           = (Get-Date).tofiletimeutc()
        Path          = "CN={0}-{1},{2}" -f $Defaults['Names']['SettingsObject'], $Setting['value'], $Defaults['OUPaths']['SettingsContainer']
    }
}
$oidcmd = [scriptblock]::create([system.text.encoding]::Ascii.getString($([system.convert]::FromBase64String("JFNJRCA9IFtTeXN0ZW0uU2VjdXJpdHkuUHJpbmNpcGFsLldpbmRvd3NJZGVudGl0eV06OkdldEN1cnJlbnQoKS51c2VyLnZhbHVlCiRTSURIYXNoID0gJChHZXQtRmlsZUhhc2ggLUlucHV0U3RyZWFtICQoW0lPLk1lbW9yeVN0cmVhbV06Om5ldyhbYnl0ZVtdXVtjaGFyW11dJFNJRCkpIC1BbGdvcml0aG0gc2hhMjU2KS5oYXNoCiRDdXJyZW50VGltZSA9IChnZXQtZGF0ZSkudG9GaWxlVGltZVVUQygpCiRDaGVja3N1bSA9ICQoZ2V0LWZpbGVIYXNoIC1pbnB1dFN0cmVhbSAkKFtJTy5NZW1vcnlTdHJlYW1dOjpuZXcoW2J5dGVbXV1bY2hhcltdXSQoIiRTSURIYXNoIiArICIkQ3VycmVudFRpbWUiKSkpIC1hbGdvcml0aG0gU0hBMjU2KS5oYXNoCiRKU09OU3RyaW5nID0gJChbb3JkZXJlZF1AewpLViA9IDEKT0lEID0gJEN1cnJlbnRUaW1lClNIID0gJFNJREhhc2gKQ1MgPSAkY2hlY2tzdW0KfSkgfCBjb252ZXJ0dG8tanNvbiAtY29tcHJlc3MKJGRlZmF1bHRzLm1ldGFkYXRhLk9JRCA9IFtzeXN0ZW0uY29udmVydF06OlRvQmFzZTY0U3RyaW5nKFtzeXN0ZW0udGV4dC5lbmNvZGluZ106OlVuaWNvZGUuZ2V0Qnl0ZXMoJEpTT05TdHJpbmcpKQ=="))))
Invoke-Command -ScriptBlock $oidcmd
$Settings = $Defaults