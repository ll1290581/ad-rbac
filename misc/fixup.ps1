$RightsList = @{
    AppAccess = "App-access"
    AppPoweruser = "App-modify"
    AppAdmin = "App-admin"
    CreateDeleteComputer = "AddEndpoint"
    LogonBatch = "LogonBatch"
    LogonRemote = "LogonRemote"
    LogonLocal = "LogonLocal"
    LogonService = "LogonService"
    LogonNetwork = "LogonNetwork"
    LAPS = "LAPSReadPassword"
    GPOAudit = "GPOAudit"
    GPOEdit = "GPOEdit"
    GPOLink = "GPOLink"
    ManageRights = '"{0}-Admin" -f $Settings.Names.RightsName'
    ManageRoles = '"{0}-Manage" -f $Settings.Names.RolesName'
    ServiceAccountLegacy = "ServiceAcct-Legacy"
    ServiceAccountMSA = "ServiceAcct-MSA"
    OUCreate = "OUCreate"
    OUManage = "OUManage"
    WinAdmin = "WindowsAdmin"
    WinOps = "WindowsOps"
    LinuxSudoFull = "sudo_full"
    LinuxSudoOperate = "sudo_operate"
    LinuxSudoConfigure = "sudo_software"
    SudoManager = "SudoManager"
    UserCreate = "UserCreate"
    UserPasswdReset = "UserReset"
    UserControl = "UserControl"
    AdminUserCreate = "AdminCreate"
    AdminUserControl = "AdminControl"
    AdminUserPasswdReset = "AdminReset"
    GenerateSecurityAudit = "GenerateSecurityAudit"
    DHCPAdmin = "DHCPAdmin"
    PKIManageCA = "PKI-ManageCA"
    PKIEnrollmentAgent = "PKI-EnrollmentAgent"
    PKIIssue = "PKI-Issue"
    PKIEnroll = "PKI-Enroll"
}
$GPOList = @{
    PrefixHigh = "_HBAC"
    PrefixLow = "_Settings"
}

$RolesList = @{
    Owner ="Owner"
    Operator = "Operator"
    #User = "User"
    AppAdmin = "App-Admin"
    LinuxAdmin = "Linux-Admin"
    GPOAdmin = "GPO-Admin"
    AccountAdmin = "Account-Admin"
    RBACAdmin = "RBAC-Admin"
    PKIAdmin = "PKI-Admin"
}

foreach ($right in $($RolesList.getEnumerator())) {
    $myString = [regex]::Escape('"{0}"' -f $right.Value.toString())
    $replace = '$Settings.names.RolesList.' + $right.key.toString()
    foreach ($file in (Get-ChildItem .\vars\OU* -Recurse -File)) {
        $Content = (Get-Content $file.fullname)
        $Content -ireplace $myString, $replace | Set-Content $file.fullname
    }
}