We want to enable property pages like "LAPS" for other object types!

Display-Specifiers[^1] to the rescue!

By setting the "adminPropertyPages"[^2] attribute on "Display-Specifier" objects corresponding to an LDAP object type,  we can control what ADUC displays.

Here's how this works.
 First, create a new 'DisplaySpecifier' object under "CN={Codepage},CN=DisplaySpecifiers,CN=Configuration,DC=Contoso,DC=Net". Its commonname should be "CN={ldapDisplayName of objectClass}-Display". For example, `CN=msDS-GroupManagedServiceAccount-Display`. Hint: English codepage is 409.

 Next, set the AdminPropertyPages attribute to a list of entries formatted as `priority,{CLSID}`. For instance, `7,{B52C1E50-1DD2-11D1-BC43-00C04FC31FD3}` (the "dial up properties" page).

 Finally, reload ADUC (dsa.msc), and open the properties of that object. Viola!

 Other interesting attributes in displaySpecifiers:
  - AdminContextMenu (right click menu in ADUC -- can specify executables!)
  - ContextMenu
  - CreateWizardExt
  - ShowInAdvancedViewOnly
  - ShellPropertyPages (controls what shows up in Explorer)

 Here are some known AdminPropertyPages:

 Computers:
13,{2FB1B669-59EA-4F64-B728-05309F2C11C8} # ???
11,{c7499700-f96b-11d2-ac78-0008c7726cf7} # ???
12,{c7436f12-a27f-4cab-aaca-2bd27ed1b773} # Attribute Editor
7,{B52C1E50-1DD2-11D1-BC43-00C04FC31FD3}  # Dial Up Properties
10,{0F65B1BF-740F-11d1-BBE6-0060081692B3} # ????
6,{4E40F770-369C-11d0-8922-00A024AB2DBB} # security
5,{6dfe6488-a212-11d0-bcd5-00c04fd8d5b6} # object
4,{6dfe648b-a212-11d0-bcd5-00c04fd8d5b6} # ManagedBy
3,{77597368-7b15-11d0-a0c2-080036af3f03} # Location
1,{6dfe6492-a212-11d0-bcd5-00c04fd8d5b6} # General, OS, Memberof, LAPS, PasswordRepl, Delegation

Users:
12,{AB255F23-2DBD-4bb6-891D-38754AC280EF}
11,{c7436f12-a27f-4cab-aaca-2bd27ed1b773}
10,{4c796c30-f96b-11d2-ac78-0008c7726cf7}
9,{FA3E1D55-16DF-446d-872E-BD04D4F39C93}
9,{AB4909C2-6BEA-11D2-B1B6-00C04F9914BD}
8,{0910dd01-df8c-11d1-ae27-00c04fa35813}
7,{8c5b1b50-d46e-11d1-8091-00a024c48131}
6,{4E40F770-369C-11d0-8922-00A024AB2DBB}
5,{6dfe6488-a212-11d0-bcd5-00c04fd8d5b6}
4,{FD57D295-4FD9-11D1-854E-00C04FC31FD3}
3,{B52C1E50-1DD2-11D1-BC43-00C04FC31FD3}
1,{6dfe6485-a212-11d0-bcd5-00c04fd8d5b6}

Groups:
6,{c7436f12-a27f-4cab-aaca-2bd27ed1b773}
5,{9a899a50-f96b-11d2-ac78-0008c7726cf7}
5,{AB4909C4-6BEA-11D2-B1B6-00C04F9914BD}
4,{4E40F770-369C-11d0-8922-00A024AB2DBB}
3,{6dfe6488-a212-11d0-bcd5-00c04fd8d5b6}
2,{6dfe648b-a212-11d0-bcd5-00c04fd8d5b6}
1,{6dfe6489-a212-11d0-bcd5-00c04fd8d5b6}

Here's an example of an adminContextMenu (on CN=OrganizationalUnit-Display):
```
4,LimitLogin Tasks...,LimitLoginContainerMMC-Add-In.exe
3,{AB255F23-2DBD-4bb6-891D-38754AC280EF}
2,{08eb4fa6-6ffd-11d1-b0e0-00c04fd8dca6}
1,{6BA3F852-23C6-11D1-B91F-00A0C9A06D2D}
```

ADSIEdit.msc -->

[^1]: https://learn.microsoft.com/en-us/windows/win32/adschema/c-displayspecifier
[^2]: https://learn.microsoft.com/en-us/windows/win32/adschema/a-adminpropertypages
