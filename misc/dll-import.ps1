﻿$MethodDefinition = @'
[DllImport( "advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
private static extern bool LookupAccountSid(
    [In,MarshalAs(UnmanagedType.LPTStr)] string systemName,
    IntPtr sid,
    [Out,MarshalAs(UnmanagedType.LPTStr)] System.Text.StringBuilder name,
    ref int cbName,
    System.Text.StringBuilder referencedDomainName,
    ref int cbReferencedDomainName,
    out int use );

[DllImport( "advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
public static extern bool LookupAccountName(
    [In,MarshalAs(UnmanagedType.LPTStr)] string systemName,
    [In,MarshalAs(UnmanagedType.LPTStr)] string accountName,
    IntPtr sid,
    ref int cbSid,
    System.Text.StringBuilder referencedDomainName,
    ref int cbReferencedDomainName,
    out int use);

[DllImport( "advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
public static extern bool ConvertSidToStringSid(
    IntPtr sid,
    [In,Out,MarshalAs(UnmanagedType.LPTStr)] ref string pStringSid);

[DllImport( "advapi32.dll", CharSet=CharSet.Auto, SetLastError=true)]
public static extern bool ConvertStringSidToSid(
    [In, MarshalAs(UnmanagedType.LPTStr)] string pStringSid,
    ref IntPtr sid);
'@

Add-Type -MemberDefinition $MethodDefinition -Name 'Advapi32' -Namespace 'Win32'