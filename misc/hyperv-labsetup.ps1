$labEnvCt = 4
$desiredLabSubnets=8
# Compute values.

# Find smallest subnet bits that can hold $labEnvCt supernets
$supernetBits=[math]::Ceiling([math]::log2($labEnvCt))
$supernetSlots = [math]::pow(2,$supernetBits)
$vlanIncrement = 256/$supernetSlots
$prefix="10.128"
$prefixCIDR=16
$labCIDR=$prefixCIDR + $supernetBits

for ($i=0; $i -lt $labEnvCt; $i++ ) {
    $labNum=$i*$vlanIncrement
    $labName = "lab$labnum"
    $ip="$prefix.$labNum.1"
    New-VMswitch -name "$labName" -SwitchType Internal
    New-NetIPAddress -ipAddress $ip -PrefixLength $LABCIDR -InterfaceAlias "vEthernet ($labName)"
}
new-NetNat -name "Lab-NAT" -InternalIPInterfaceAddressPrefix "$prefix.0.0/$PrefixCIDR"