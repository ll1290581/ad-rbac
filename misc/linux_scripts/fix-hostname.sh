#!/bin/bash
# This script fixes hostnames to lowercase.
# First it rebuilds keytabs (using the existing principals + secrets),
# then it updates /etc/hosts and sets the hostname

klistLines=`klist -kKe | grep "@" | tr -s " " | sed 's/(0x//g' | sed 's/[()]//g'`
echo $klistLines | sort -u | while read line; do
    kvno=`echo $line | cut -d " " -f 2`
    principal=`echo $line | cut -d " " -f 3`
    enctype=`echo $line | cut -d " " -f 4`
    key=`echo $line | cut -d " " -f 5`
    fixedprincipal=`echo $principal | sed -E "s/^([^/]*\/)?([^@]+)(@.*)/\1\L\2\U\3/"`
    principalsInKeytab=`klist -kKe /etc/krb5.newkeytab | grep -c "$kvno *$principal *($enctype).*(0x$key)"`
    if [[ $principalsInKeytab == 0 ]]; then
        {
            echo "addent -key -p $principal -k $kvno -e $enctype"
            sleep 0.5
            echo $key
            sleep 0.5
            echo "write_kt /etc/krb5.newkeytab"
        } |
        ktutil
    fi
    fixedPrincipalsInKeytab=`klist -kKe /etc/krb5.newkeytab | grep -c "$kvno *$fixedprincipal *($enctype).*(0x$key)"`
    if [[ $fixedPrincipalsInKeytab == 0 ]]; then
        {
            echo "addent -key -p $fixedprincipal -k $kvno -e $enctype"
            sleep 0.5
            echo $key
            sleep 0.5
            echo "write_kt /etc/krb5.newkeytab"
        } |
        ktutil
    fi
done
mv /etc/krb5.keytab /etc/krb5.keytabold
mv /etc/krb5.newkeytab /etc/krb5.keytab
newhostname=`hostname | tr '[:upper:]' '[:lower:]'`
chmod 660 /etc/krb5.keytab
chown root:root /etc/krb5.keytab
hostnamectl set-hostname $newhostname
cat /etc/hosts | tr '[:upper:]' '[:lower:]' > /etc/hosts
