﻿#!/bin/bash
# Unmount all CIFS shares
umount -a -t cifs -l

# remove all kerberos tickets
kdestroy -A

#upgrade
#dnf update

#remove all but most recent kernel
dnf remove -y $(dnf repoquery --installonly --latest-limit=-1 -q)

#cleanup packages
#dnf clean all



#clear old logs
logrotate -f /etc/logrotate.conf
rm -f /var/log/*-???????? /var/log/*.gz
rm -f /var/log/dmesg.old
#find archives
find /var/log/ -type f -iname *.xz -print0 | while IFS= read -r -d $'\0' file
do
	echo "Deleting log: $file"
	rm -f $file
done

#delete date-stamped archives or backups
find /var/log -type f -regextype sed \( -regex ".*/.*[0-9]\{6,8\}.*" -o -regex ".*\.[0-9]" -o -regex ".*\.prior" -o -regex ".*\.prev" \) -print0 | while IFS= read -r -d $'\0' file
do
	echo "Deleting log: $file"
	rm -f $file
done

#clearing other logs
find /var/log -type f -print0 | while IFS= read -r -d $'\0' file
do
	echo "Empyting log: $file"
	cat /dev/null >$file
done

# Clearing LVM devices
rm -f /etc/lvm/devices/system.devices

# Clear hardware rules
rm -f /etc/udev/rules.d/70*

echo "Remove uuid from ifcfg scripts"
sed -i '/^(HWADDR|UUID)=/d' /etc/sysconfig/network-scripts/ifcfg-*


echo "remove host SSH keys"
rm -f /etc/ssh/*key*

echo "clean tmp"
rm -rf /tmp/*
rm -rf /var/tmp/*

echo "clear known hosts"
rm -f /home/*/.ssh/known_hosts
rm -f /root/.ssh/known_hosts


echo "remove histories"
unset HISTFILE
history -cw
rm -f /root/.bash_history
rm -f /home/*/.bash_history
history -cw

echo " #########   Shutdown now!   #######"