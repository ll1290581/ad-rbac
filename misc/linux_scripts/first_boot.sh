#!/bin/bash
# Environmental variables
function get_realm () {
    local dnsSuffix=`nmcli -g IP4.DOMAIN dev show | grep -E "\w+\.\w+"`
    local realmCandidates=($(realm discover -n) "$dnsSuffix")
    # Make sure we don''t get any null array elements
    realmCandidates=($(echo ${realmCandidates[*]} | grep -E -v '^$'))
    read -ep 'Enter AD realm name > ' -i $realmCandidates realm
    if [[ -z $realm ]]; then
        echo "No realm provided. Please try again." > /dev/tty
        break
    fi
    # Return as uppercase because AD expects uppercase realm for salt
    echo $realm | tr '[:lower:]' '[:upper:]'
}
function clean_exit() {
    kdestroy -A
    umount -a -t cifs -l
    exit 1
}

realm=`get_realm`
domainDN=$(echo "DC=$realm" | sed 's/\./,DC=/g')
orgOUPath="OU=Orgs,OU=RAD,$domainDN"
componentOUName="Components"
endpointOUName="Endpoints"
gitRootSmb="//$realm/repos/Git"
pkiHost="pki.$realm"
pkiHttpPath="http://$pkiHost/pki/"
os_name=`cat /etc/*-release | grep "^NAME" | sed 's/^NAME=//g' | sed 's/"//g'`
os_version=`cat /etc/*-release | grep "^VERSION=" | sed 's/^VERSION=//g' | sed 's/"//g'`

gitRepos=(
    "cepces.git"
    "LAPS4LINUX.git"
)
packageList="krb5-workstation samba-client ansible-core sssd git certmonger python3"

function get_kerberos_tgt () {
    if [[ -z $realm ]]; then
        realm=`get_realm`
    fi
    read -ep "Enter Active Directory username (UPN format. Default domain: $realm):"
    # uppercase the string to avoid issues with kinit (the salt is the uppercase realm name, and username doesnt care)
    ad_user=`echo $REPLY | tr '[:lower:]' '[:upper:]' | sed -E "s/^([^@]+)$/\1@$realm/"`

    if [[ -z $ad_user ]]; then
        echo "No username provided. Please try again." > /dev/tty
        clean_exit
    fi

    read -esp $'Enter Active Directory password:\x0a' ad_password
    echo "Attempting to authenticate against kerberos..." > /dev/tty
    err=`echo $ad_password | kinit $ad_user 2>&1`
    if [[ $? -eq 1 ]]; then
        if [[ $err =~ "kinit: Cannot find KDC" ]]; then
            echo "Issue finding realm '$realm', check spelling and DNS." > /dev/tty
        elif [[ $err =~ "kinit: Password incorrect" ]]; then
            echo "Password supplied is incorrect. Try again." > /dev/tty
        elif [[ $err =~ "kinit: KDC reply did not match expectations" ]]; then
            echo "KDC reply mismatch. Make sure your realm name ($realm) is all uppercase." > /dev/tty
        elif [[ $err =~ "kinit: Client '$ad_user@$realm' not found in Kerberos database" ]]; then
            echo "Username '$ad_user@$realm' is incorrect / not found in directory." > /dev/tty
        else
            echo "Unknown issue authenticating to kerberos. Check DNS, krb5.conf, or sssd.conf?" > /dev/tty
            echo "Error message: $err" > /dev/tty
        fi
        clean_exit
    fi
}

function find_ldap_server () {
    if [[ -z $realm ]]; then
        realm=`get_realm`
    fi
    local ldapServerList=($(dig -t SRV _ldap._tcp.$realm +noall +short | cut -f4 -d' ' | sed 's/\.$//'))
    #echo "Performing LDAP ping on $LDAPPINGSERVER"
    #NETLOGON=`ldapsearch -LLL -H LDAP://$LDAPPINGSERVER -b '' -s base '(&(NtVer=\06\00\00\00)(AAC=\08\00\00\00))' netlogon | grep netlogon | cut -f2 -d' '`
    for server in ${ldapServerList[@]}; do
        echo "pinging: $server" >/dev/tty
        ldapsearch -x -LLL -H LDAP://$server -o nettimeout=1 -b '' -s base '(&(NtVer=\06\00\00\00)(AAC=\08\00\00\00))' netlogon 2>/dev/null 1>/dev/null  || continue
        echo $server
        return 0
    done
    echo "Couldn't reach LDAP server. Exiting...."
    clean_exit
}
function decode_sid () {
    local OBJECT_ID=$1
    # Decode it, hex-dump it and store it in an array
    G=($(echo -n $OBJECT_ID | base64 -d -i | hexdump -v -e '1/1 " %02X"'))

    # SID in HEX
    # SID_HEX=${G[0]}-${G[1]}-${G[2]}${G[3]}${G[4]}${G[5]}${G[6]}${G[7]}-${G[8]}${G[9]}${G[10]}${G[11]}-${G[12]}${G[13]}${G[14]}${G[15]}-${G[16]}${G[17]}${G[18]}${G[19]}-${G[20]}${G[21]}${G[22]}${G[23]}-${G[24]}${G[25]}${G[26]}${G[27]}${G[28]}

    # SID Structure: https://technet.microsoft.com/en-us/library/cc962011.aspx
    # LESA = Little Endian Sub Authority
    # BESA = Big Endian Sub Authority
    # LERID = Little Endian Relative ID
    # BERID = Big Endian Relative ID

    BESA2=${G[8]}${G[9]}${G[10]}${G[11]}
    BESA3=${G[12]}${G[13]}${G[14]}${G[15]}
    BESA4=${G[16]}${G[17]}${G[18]}${G[19]}
    BESA5=${G[20]}${G[21]}${G[22]}${G[23]}
    BERID=${G[24]}${G[25]}${G[26]}${G[27]}${G[28]}

    LESA1=${G[2]}${G[3]}${G[4]}${G[5]}${G[6]}${G[7]}
    LESA2=${BESA2:6:2}${BESA2:4:2}${BESA2:2:2}${BESA2:0:2}
    LESA3=${BESA3:6:2}${BESA3:4:2}${BESA3:2:2}${BESA3:0:2}
    LESA4=${BESA4:6:2}${BESA4:4:2}${BESA4:2:2}${BESA4:0:2}
    LESA5=${BESA5:6:2}${BESA5:4:2}${BESA5:2:2}${BESA5:0:2}
    LERID=${BERID:6:2}${BERID:4:2}${BERID:2:2}${BERID:0:2}

    LE_SID_HEX=${LESA1}-${LESA2}-${LESA3}-${LESA4}-${LESA5}-${LERID}

    # Initial SID value which is used to construct actual SID
    SID="S-1"

    # Convert LE_SID_HEX to decimal values and append it to SID as a string
    IFS='-' read -ra ADDR <<< "${LE_SID_HEX}"
    for OBJECT in "${ADDR[@]}"; do
        SID=${SID}-$((16#${OBJECT}))
    done

    echo ${SID}
}

function get_ldap_attribute_from_ldapsearch () {
    # Pass ldapsearch output (attribute: value format) in via stdin, and supply an attribute to get
    if [[ -z $1 ]]; then
        echo "Missing attribute to filter on."
        break
    fi
    local attrib=$1
    local output=($(grep -i $attrib | awk -v RS='\n' -v FS=': ' -v ORS='\n' -v OFS='|' '{print $2}'  | sed -E 's/^(dn:)?\|$//g' | grep -E -v '^$'))
    for line in ${output[@]}; do
        if [ "${attrib,,}" =  "objectsid" ]; then
            decode_sid "$line"
        else
            echo $line | base64 -d 2>/dev/null 1>/dev/null
            if [ "$?" -eq "0" ]; then
                echo $line | base64 -d
                printf "\n"
            else
                echo $line
            fi
        fi
    done
}

function select_rbac_element () {
    if [[ -z $ldapServer ]]; then
        ldapServer=`find_ldap_server`
    fi
    local searchdn=$1
    local displaytype=$2
    local IFS=$'\n'
    local elementList=($(ldapsearch -LLL -o ldif-wrap=no -Y GSSAPI -Q -H ldap://$ldapServer:3268 -O maxssf=256 -s onelevel -b "$searchdn" '(objectclass=organizationalUnit)' name description -S name | awk -F '\n?[a-zA-Z]+: ' -v RS='\n\n' -v ORS='\n' -v OFS='|' '{print NR,$4,$3,$2}'))
    printf '\n\n' > /dev/tty
    while
        for element in ${elementList[@]}; do
            echo $element | awk -v FS='|' '{printf "%5s. %-32s %s\n", $1,$2,$3}' > /dev/tty
        done
        read -p "Enter the number for the RBAC $displaytype this host will belong to > " elementNum
        ! [[ $elementNum =~ ^[0-9]+$  && $elementNum -ge 0 && $elementNum -le ${#elementList[@]} ]]
    do printf "  Invalid entry. Please specify an $displaytype by number, or 0 to exit\n\n" > /dev/tty; sleep 1 ; done
    if [[ $elementNum == 0 ]]; then
        clean_exit
    else
        ((elementNum-=1))
    fi
    local selectedElement=${elementList[$elementNum]}
    local elementName=`echo $selectedElement | cut -d '|' -f 2`
    local elementDescription=`echo $selectedElement | cut -d '|' -f 3`
    local elementDN=`echo $selectedElement | cut -d '|' -f 4`
    printf "\n\nSelected:    %-32s %s\n" $elementName $elementDN > /dev/tty
    echo $elementDN
}

get_kerberos_tgt

#LDAP vars
ldapBaseOptions="-LLL -o ldif-wrap=no"
ldapServer=`find_ldap_server`
ldapSearchOptions="$ldapBaseOptions -Y GSSAPI -Q -H ldap://$ldapServer:3268 -O maxssf=256"
ldapModOpts="$ldapBaseOptions -Y GSSAPI -Q  -H ldap://$ldapServer:389 -O maxssf=256"



orgDN=`select_rbac_element "$orgOUPath" "parent organization"`
orgName=`echo $orgDN | sed -E "s/^OU=(.*),$orgOUPath$/\1/i"`
componentDN=`select_rbac_element "OU=$componentOUName,$orgDN" "parent Component"`
componentName=`echo $componentDN | sed -E "s/^OU=(.*),OU=$componentOUName,$orgDN$/\1/i"`

computerOU="OU=$endpointOUName,$componentDN"
# Hostname should be uppercase to simplify kinit using host keytab-- keytab will have principals for uppercase name for samaccountname.
hostnameShort=`echo $HOSTNAME | cut -f1 -d. | tr [:lower:] [:upper:]`
hostnameFull="${hostnameShort}.$realm"

lapsGroupName=`ldapsearch $ldapSearchOptions -s subtree -b $componentDN "(&(objectClass=group)(name=*-$orgName-$componentName*LAPS*))" name | get_ldap_attribute_from_ldapsearch name`
lapsGroupSID=`ldapsearch $ldapSearchOptions -s subtree -b $componentDN "(&(objectClass=group)(name=$lapsGroupName))" objectSID | get_ldap_attribute_from_ldapsearch objectSID`


cat <<EOF

########################################################

Realm:                 $realm
User:                  $ad_user
Hostname:              ${hostnameFull}
OSName:                $os_name
OSVersion:             $os_version
Org:                   ${orgName}
Component:             ${componentName}
ComputerOU:            ${computerOU}
LAPS Group:            ${lapsGroupName} (SID: ${lapsGroupSID})

########################################################

Tasks to complete:
 + Normalize hostname to ${hostnameFull}
 + Enable SSHD
 + Enable Cockpit
 + Enable AD-compatible encryption
 + Disabling FIPS mode for SMB / NTLMv2
 + configure SSSD
 + Configure LAPS4LINUX for automatic root password changes
 + Adding sudoGroups for this org
 + (Re-)Join AD domain ${realm}

EOF

read -n 1 -p "Continue (y/[n])? " answer
if [ ${answer} != "y" ]
then
   echo "OK, exiting."
   clean_exit
fi

sleep 1
echo "#########  Leaving AD realm if it exists"
if [ `realm list | grep -c $realm` -ne 0 ]
then
    echo "Domain configuration exists, removing."
    echo $ad_password | realm leave -U $ad_user --remove -v --server-software=active-directory $realm
else
    # if adcli finds the computer, delete it.
    echo "Clearing computer object from AD prior to join..."
    adcli show-computer ${hostnameShort} --login-ccache 2>/dev/null &&  adcli delete-computer ${hostnameShort} --login-ccache -v
fi

echo -e "\n##########  Normalizing hostname to ${hostnameFull}"
hostnamectl set-hostname "${hostnameFull}"

#sleep 1
#echo -e "\n##########  Installing ${packageList}"
#dnf install -yq ${packageList}

sleep 1
echo "##########  Enabling SSHD"
systemctl enable sshd.service --now

sleep 1
echo "##########  Enabling Cockpit"
systemctl enable cockpit.socket --now

sleep 1
echo "##########  Updating crypto policies to support older kerberos enctypes for AD. (Might not be necessary once everything is AES128/AES256 )."
update-crypto-policies --set DEFAULT:AD-SUPPORT
update-crypto-policies --set DEFAULT:AD-SUPPORT-LEGACY

sleep 1
echo "##########  Disabling FIPS mode (Necessary to support NTLMv2 SMB which requires MD5-HMAC)"
fips-mode-setup --disable

sleep 1
echo "##########  Configuring SSSD with sudo / netgroup support"
cat << EOF > /etc/sssd/conf.d/ad-rbac.conf # sssd looks for *.conf files
[sssd]
debug_level = 4
# Allow users to log in without realm.
# Permitted login styles: 'samAccountName'; 'samAccountName@REALM'; 'userPrincipalName@Realm'
default_domain_suffix = $realm
# ssh and sudo to allow use of sudoers, sshPubkey. Autofs also possible
services = nss, pam, sudo, ssh
# Hardening to prevent passwords from leaking into logs
core_dumpable = false

[pam]
debug_level = 4
# Days to allow logins via PAM while offline
offline_credentials_expiration = 1
# Attempts before locking account. Set to 5 to work around some bugs in RHEL 7 cockpit (automatic lock)
offline_failed_login_attempts = 5
# Delay before unlocking account
offline_failed_login_delay = 5
pam_account_locked_message = Account locked.
# For debugging. Range = 0-3
pam_verbosity = 1
pam_gssapi_services = sudo, sudo-i

# This service pulls sudoRole objects from Active Directory
[sudo]
debug_level = 5

# Allows fetching SSH pubkeys from ldap
[ssh]
debug_level = 5
ssh_hash_known_hosts = true
ssh_known_hosts_timeout = 180

[domain/$realm]
debug_level = 4
# Use pre-authentication
krb5_use_fast = demand
account_cache_expiration = 1
# Allow AD to manage access control
access_provider = ad
ad_gpo_access_control = enforcing
# Fixes some log warnings in vSphere
ad_gpo_map_interactive = +vmtoolsd
# Dynamic DNS updates (secure)
dyndns_refresh_interval = 86400
dyndns_update_ptr = true
dyndns_update = true
dyndns_auth = gss-tsig
dyndns_refresh_interval_offset = 600
# ID mapping so uidNumber and gidNumber are not needed / used
ldap_id_mapping = True
ldap_schema = ad
# See man page. LDAP can return a lower-case realm part of UPN which 'can cause authentication to fail.' This defaults to false.
ldap_force_upper_case_realm = true
# Add linux integrations
ldap_user_extra_attrs = sshPublicKeys:sshPublicKeys
ldap_user_gecos = displayName
ldap_user_ssh_public_key = sshPublicKeys
ldap_netgroup_search_base = OU=Netgroups,OU=LinuxFeatures,OU=RAD,$domainDN?subtree?
ldap_sudo_search_base = OU=SudoRoles,OU=LinuxFeatures,OU=RAD,$domainDN?subtree?
ldap_autofs_search_base = OU=autofs,OU=LinuxFeatures,OU=RAD,$domainDN?subtree?
# Normally SAMAccountName is used. This switches to userPrincipalName
#ldap_user_name = userPrincipalName
# Fully handles group nesting so nesting-level is not needed. This may not be needed.
#ldap_use_tokengroups = True
# This allows unqualified usernames, but it conflicts with 'default_domain_suffix' in the services section.
#use_fully_qualified_names = false

EOF

sleep 1
echo "##########  Configuring LAPS4LINUX"
cat << EOF > /etc/laps-runner.json
{
    "COMMENT": "Keep server and domain blank to autodiscover via DNS. security-descriptor has to be a SID.",
    "domain": "$realm",
    "use-starttls": true,

    "cred-cache-file": "/tmp/laps.temp",
    "client-keytab-file": "/etc/krb5.keytab",

    "native-laps": true,
    "security-descriptor": "$lapsGroupSID",
    "history-size": 4,
    "ldap-attribute-password": "msLAPS-EncryptedPassword",
    "ldap-attribute-password-history": "msLAPS-EncryptedPasswordHistory",
    "ldap-attribute-password-expiry": "msLAPS-PasswordExpirationTime",

    "hostname": null,

    "password-change-user": "admin",
    "password-days-valid": 30,
    "password-length": 16,
    "password-alphabet": "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#%^_+-="
}
EOF


sleep 1
echo "##########  Defining sudoers"
sudogroups=($(ldapsearch $ldapSearchOptions "(&(objectClass=group)(|(name=*Right-Global-sudo*)(name=*Right-$orgName-sudo*)(name=*Right-$orgName-$componentName-sudo*)))" name description -S description | get_ldap_attribute_from_ldapsearch name))
cat << EOF | tee /etc/sudoers.d/ad-rbac
Defaults listpw=never

Cmnd_Alias OPERATE = /bin/sudo -l, /usr/bin/dmesg, /usr/bin/journalctl, /usr/sbin/aureport, /usr/sbin/ausearch, /usr/bin/tail /var/log/audit/*, /usr/bin/systemctl start *, /usr/bin/systemctl restart *, /usr/bin/systemctl reload *, /usr/bin/systemctl status *, /usr/bin/systemctl reboot, /usr/sbin/tcpdump, /usr/bin/reboot
Cmnd_Alias CONFIGURE = /usr/bin/kill, /usr/bin/killall, /usr/bin/nmtui, /usr/bin/nmcli, /usr/bin/hostnamectl, /usr/bin/resolvectl, /usr/sbin/ip, /usr/bin/firewall-cmd, /usr/sbin/dhclient, /usr/sbin/ifconfig, /usr/sbin/route, /usr/sbin/tcpdump, /usr/bin/systemctl start *, /usr/bin/systemctl stop *, /usr/bin/systemctl enable *, /usr/bin/systemctl disable *, /usr/bin/systemctl restart *, /usr/bin/systemctl reload *, /usr/bin/systemctl status *, /usr/bin/systemctl reboot, /usr/bin/dnf, /usr/bin/yum, /usr/bin/rpm, /usr/bin/reboot
Cmnd_Alias SOFTWARE = /usr/bin/dnf, /usr/bin/yum, /usr/bin/rpm

EOF
for group in ${sudogroups[@]}; do
    permission=""
    passwd=" "
    upngroup="$group@$realm"
    case ${group,,} in
        *sudo-nopasswd*) passwd="NOPASSWD:";;
        *sudo_*nopw) passwd="NOPASSWD:";;
        *) passwd=" ";;
    esac;
    case ${group,,} in
        *-sudo*operate*) permission=OPERATE;;
        *-sudo*software*) permission=SOFTWARE;;
        *-sudo*configure*) permission=CONFIGURE;;
        *-sudo*full*) permission=ALL;;
        *) continue;;
    esac;
    printf "%%%-64s  ALL = (ALL)  %-10s %s\n" $upngroup $passwd $permission
done >> /etc/sudoers.d/ad-rbac # do not put dots in the filename or sudo will ignore it

sleep 1
echo "##########  Beginning domain join tasks"
echo "Joining $realm as $ad_user..."
#echo $ad_password | realm join --user $ad_user $REALM --computer-ou=${host_ou} > /dev/null
# This should use the kerberos credential cache, do not specify a user.
realm join --computer-ou="${computerOU}" --os-name="$os_name" --os-version="$os_version" -v $realm

clean_exit
