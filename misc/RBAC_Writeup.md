---
title: "RBAC SOP"
author: John
date: March 31, 2024
output: word_document
---
# RBAC Overview

## Introduction

Role-based access control (RBAC) is a key part of system security. It establishes standardized roles with well-defined access which users can then be associated with.

There are two key pieces to making this work:
 -  Standardized roles make it easy to replicate access for new employees and troubleshoot access denied issues when they arise, while also simplifying system audits.
 - **Well-defined access rights** provide flexibility in defining new roles.....

By implementing a strong RBAC system, organizations can enforce the princicple of *least privilege*, granting individuals only as much access as is needed to perform their job duties.


### Scope

This document describes the particular implementation of RBAC that our organization is using, and how to administer it. It covers the RBAC heirarchy, the standardized roles and rights groups, the naming scheme, and what particular access each is granted.

### Audience

Anyone with a general understanding of directory systems and access control should be familiar with the concepts discussed here. However, this document is particularly aimed at systems administrators who might grant access and systems operators who might use the access granted.

## Rights and Roles

We implement the RBAC concepts above using Active Directory security groups. Users are members of global security groups called **Roles**, which are members of domain local security groups called **Rights**.

```mermaid
flowchart TD
    U[Users] --Member--> L[Roles]
    L --Member--> R[Rights]
    S[Service Accounts] --Member--> L
    ACLs --Ref--> R
    SAML/OIDC --Ref--> R
    Applications --Ref--> R
```

**Users** ---MemberOf---> **Roles** --MemberOf--> **Rights**

In general, access lists or permissions statements should only ever reference **Rights**. For instance, a Linux /etc/sudoers file or Web Application 'Administrators' definition should only ever reference an AD rights group, never a role or a user.

This allows Roles to be flexibly defined or redefined without impacting access, and to make it simple to audit what access a user has.

Rights should generally not be reused for multiple areas of access-- one right should correspond to one privilege or access.

### Naming

Most rights provide predefined access in the directory and are not intended to be reused. Thus most rights are prefixed with a UTF-8 symbol (❖) specifically to make it hard for a systems administrator to refer to them directly. Rights that are not preassigned-- and are available for use within an application-- do not have this prefix and may be used. This is discussed further in the Components subheading of the OU Heirarchy section.

Likewise, Roles should not be referenced by Access Control Lists (ACLs), and so include a UTF-8 symbol (☍). The symbol is applied as a suffix to make it possible to perform searches on the group name while still making it hard to refer to by name.

Some roles fall under the 'Protected Users' group which applies significant restrictions and protections to it. These roled are prefixed by another UTF-8 symbol (⚡) as they should not be used in the normal course of business.

OU Heirarchies will be discussed later, but all Rights and Roles belong to an RBAC heirarchy 'element' and include the name of that element in their own name. Thus, these are some examples of possible names:

 - ⚡Role-Global-PKI-Admin☍
 - Role-Infra-Email-Svc-Local☍
 - ❖Right-Infosec-Tenable-WindowsAdmin
 - Right-Network-PaloAlto-App-Access


## OU Heirarchy

Active Directory allows organizing systems, groups, and users in a tree structure using "Organizational Units" (or OUs). This allows delegating control and policies to groups of end systems. Path names are in a reverse-name format, formatted as `CN=ComputerName,OU=Parent,OU=Container,DC=lab,DC=local` (for a domain called `lab.local`).

Generally, having a role at the global or org level grants a similar level of access at lower levels of the heirarchy. For instance, a global role with WindowsAdmin permissions would generally have that level of access across all systems in the RBAC OU tree.

Note: The RBAC chart associated with the rights at each level detailed in Appendix 1.

### Global level
The top-level of the RBAC tree structure is at `OU=RAD,DC=Lab,DC=local` (RAD short for RBAC Active Directory), referred to here as the **Global** level. This is the lowest point where delegated permissions and policies are applied. Containing everything at within this OU avoids a number of security pitfalls such as granting permissions to edit a GPO that might affect Domain Controllers.

The Global level has a number of OUs within it for containing different objects:

 - Users
 - Unassigned computers
 - Roles
 - Rights
 - Orgs

#### Global Roles

There are several roles defined at the global level:

| Name | Description |
| -- | --- |
|⚡Role-Global-RBAC-Admin☍ | Manage all RBAC groups, OUs, and GPOs. Imlicit full control of everything under RBAC root. Restricted Login. |
|⚡Role-Global-PKI-Admin☍ | Permissions to approve cert requests and manage CAs. Restricted login. |
|Role-Global-Operator☍ | Local admin rights, DNS / DHCP, Cert enrollment, and domain join |
|Role-Global-Svc-Remote-Admin☍ | Local admin rights. Sudo without password. Suitable for remote Scan services like Nessus. |
|Role-Global-Orgs-Admin☍ | Access to create and delete Orgs. Implicit full control of all objects under all orgs. |
|Role-Global-App-Admin☍ | Admin access within all applications under all components. This does not explicitly grant RDP / SSH access or host root. |
|Role-Global-GPO-Admin☍ | Permissions to edit, link and troubleshoot all GPOs created within RBAC structure |
|Role-Global-Account-Admin☍ | Create and reset standard accounts; create sensitive accounts |

Of special note is the 'Svc-Remote-Admin' role, which is designed to be used by security products performing remote privileged scans. It is not intended to have any directory access but does have a high level of access across the organization.

### Org level

The next level in the heirarchy is the Org, located at `OU=<OrgName>,OU=Orgs,OU=RAD,DC=Lab,DC=Local` where <name> refers to the Org name.

Orgs are business units, tenants, departments, or other similar business-oriented group."Infrastructure" or "SoftwareDev" would be examples-- they will often align with teams.

Orgs contain several OUs for holding:

 - Roles
 - Rights
 - Components

#### Org Roles

There are several roles defined at the org level:

| Name | Description |
| --- | --- |
| Role-OrgName-Owner☍ | Owner of this Org. Assumed to have full administrative rights on all systems in child components. |
| Role-OrgName-Operator☍ | Elevated rights for OS and application. Note: Does not have App Admin |
| Role-OrgName-Svc-Remote-Admin☍ | Local admin rights. Sudo without password. Suitable for remote Scan services like Nessus. |
| Role-OrgName-User☍ | Act as user for applications under org |

Generally speaking, Owners have permissions to modify their area of the directory, while operators only have privileges on the systems themselves.

### Components

The lowest level in the heirarchy is the component, which has a path like `OU=<ComponentName>,OU=Components,OU=<OrgName>,OU=Orgs,OU=RAD,DC=Lab,DC=Local`.

Components are systems running a software stack, projects, or engagements that share an owner, a common lifecycle, and access levels. "VMWare"; "ServiceNow" are examples. All components are owned by an Org.

There are by default 3 rights created at the Component level that are not preassigned to any policies or ACLs:

 - ...App-Access
 - ...App-Modify
 - ...App-Admin

These do not have any UTF-8 prefix, and are designed to be used by the associated application. The global and org levels have corresponding groups which use nested membership to ensure that the heirarchical access is enforced.

#### Component Roles

These are the roles defined within every component.

| Name | Description |
| --- | --- |
| Role-OrgName-Component-Owner☍ | Owner of this component. Assumed to have full administrative rights on all systems in child components. |
| Role-OrgName-Component-Operator☍ | Local operator and app-modify rights. |
| Role-OrgName-Component-Svc-Local☍ | Service Account with local admin rights. |
| Role-OrgName-Component-User☍ | Standard user for this component. Assumed to be allowed to log in and make changes to data in the application. |

Owners and operators function the same as at the Org level: Owners can modify the directory structure itself (change group memberships), and operators can only affect the endpoints within the component.

The Svc-Local role is intended for services running directly on the endpoint, and generally has privileges needed by services like ADFS, Microsoft Exchange, etc.

## Administration of RBAC

We are using an open-source powershell module for managing the RBAC structure called `ad-rbac`, which can be found at https://www.gitlab.com/breakwaterlabs/ad-rbac. This module provides a number of commands such as:

 - add-rbacOrg
 - add-rbacComponent
 - add-rbacServiceAccount

These commands fully automate the creation of new OU tree structures, rights, roles, AD ACLs, and GPOs.

Because the "Active Directory Users and Computers" GUI tool (aka dsa.msc) is so popular, we have also created an addon for it to simplify management (see Appendix 2 for how this is accomplished). Right-clicking on any organizational Unit will show a "Launch Management Shell" option which opens a powershell prompt with this module already installed.

For new domains, one would need to manually install the `ad-rbac` module and run `add-rbac` with Domain Admin rights. Some options (such as sshPublicKey and sudoers integrations) require Schema Admin rights, but the basic installation will simply deploy the Global OU tree structure and relocate the default users / computers containers.

Creating orgs is generally as simple as `add-rbacOrg -org "InfoSec" -description "Compliance and Auditing"`, and dry runs are possible with the `-whatif` parameter.

Auditing is done using the `get-RBACOrg` (and related) commands, which can either be run in 'mock' mode which outputs the baseline template, or in live mode which validates the actual RBAC structure against the baseline and identifies any deviance.

More information can be seen by consulting the help files for the module (e.g. `get-help add-rbacComponent -detailed` or `get-command -module ad-rbac`).

## Appendix 1: Role RBAC Charts

### Global Roles Chart

| Right | RBAC-Admin | Operator | Svc-Remote-Admin | Orgs-Admin | App-Admin | GPO-Admin | PKI-Admin | Account-Admin | Description |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| ..DHCPAdmin |  | ✅ |  |  |  |  |  |  | Authorize and manage DHCP servers. |
| ..DNSOperator |  | ✅ |  |  |  |  |  |  | Rights to manage DNS server. Built in delegations on MicrosoftDNS node in AD. |
| ..DFSCreateReplGroup |  | ✅ |  |  |  |  |  |  | Permissions to create new DFS-R Groups. Permissions will need to be manually added to new Repl groups. |
| ..DFSNamespaceManage |  | ✅ |  |  |  |  |  |  | Manage DFS Namespaces for domain |
| ..PKI-ManageCA |  |  |  |  |  |  | ✅ |  | Manage CA settings and cert templates |
| ..PKI-Issue |  |  |  |  |  |  | ✅ |  | Approve and revoke certificates |
| ..PKI-RequestCert |  | ✅ |  |  |  |  |  |  | Request certificates |
| ..UserCreate |  |  |  |  |  |  |  | ✅ | Create standard users |
| ..UserControl |  |  |  |  |  |  |  | ✅ | Allowed to enable / disable / delete users |
| ..UserReset |  |  |  |  |  |  |  | ✅ | Reset passwords for standard users |
| ..AdminReset |  |  |  |  |  |  |  |  | Reset passwords for sensitive / Administrative accounts |
| ..AdminCreate |  |  |  |  |  |  |  | ✅ | Create sensitive / administrative accounts |
| ..AdminControl |  |  |  |  |  |  |  | ✅ | Allowed to enable / disable / Delete sensitive accounts |
| ..SudoManager | ✅ |  |  | ✅ |  |  |  |  | Rights to Create and modify Sudoroles and Netgroups. This allows gaining sudo rights on arbitrary systems. |
| ..App-access |  |  |  |  | ✅ |  |  |  | Allowed Log in to applications (e.g. Web UIs). |
| ..App-modify |  |  |  |  | ✅ |  |  |  | Poweruser or modify access to applications (e.g. Web UIs) |
| ..App-admin |  |  |  |  | ✅ |  |  |  | Admin access to applications (e.g. Web UIs) |
| ..AddEndpoint |  | ✅ |  |  |  |  |  |  | Allowed to create / join computer objects |
| ..GPOCreate | ✅ |  |  | ✅ |  | ✅ |  |  | Allowed to create new GPOs. Replaces GPOCreatorsOwners, but without rights to default domain GPOs |
| ..GPOAudit |  | ✅ |  |  |  | ✅ |  |  | Allowed to run RSOP and GPO Modelling |
| ..GPOLink | ✅ |  |  |  |  | ✅ |  |  | Allowed to link and unlink GPOs in org |
| ..GPOEdit | ✅ |  |  |  |  | ✅ |  |  | Edit rights on All GPOs |
| ..OUCreate |  |  |  |  |  |  |  |  | Create and update arbitrary Organizational Units |
| ..OUManage | ✅ |  |  |  |  |  |  |  | Manage arbitrary OUs, properties and permissions on ACLs |
| ..OrgManage | ✅ |  |  | ✅ |  |  |  |  | Create and delete Orgs, as well as update their DACLs. Implicit full control of all orgs. |
| ..OrgDelete | ✅ |  |  | ✅ |  |  |  |  | Delete orgs and all subordinate objects |
| ..Rights-Admin | ✅ |  |  |  |  |  |  |  | Create and delete new rights, and modify membership of all groups. |
| ..Roles-Manage | ✅ |  |  |  |  |  |  |  | Create and delete new roles, and modify membership of roles. |
| ..LAPSReadPassword |  | ✅ |  |  |  |  |  |  | Fetch local machine passwords via Windows LAPS |
| ..WindowsAdmin |  | ✅ | ✅ |  |  |  |  |  | Local admin rights on Windows / Linux hosts |
| ..WindowsOps |  |  |  |  |  |  |  |  | Limited operator rights on Windows hosts: event log, performance monitoring, network changes |
| ..sudo_full |  | ✅ |  |  |  |  |  |  | Linux Sudo: Unrestricted / full root. |
| ..sudo_full-nopw |  |  | ✅ |  |  |  |  |  | Linux Sudo (no passwd): Unrestricted / full root |
| ..sudo_operate |  |  |  |  |  |  |  |  | Linux Sudo: Read logs, manage services |
| ..sudo_configure |  |  |  |  |  |  |  |  | Linux sudo: change network settings, time, firewall, and installed packages |
| ..LogonLocal |  | ✅ |  |  |  |  |  |  | Rights for Local logon |
| ..LogonRemote |  | ✅ | ✅ |  |  |  |  |  | Allow log on through Remote Desktop Services /SSH. |
| ..LogonBatch |  |  |  |  |  |  |  |  | Rights for batch logon / scheduled task / cron access |
| ..LogonService |  |  |  |  |  |  |  |  | Rights for logon as service in this Org |
| ..DenyRemote | ✅ |  |  |  |  |  | ✅ |  | Denied login via SSH / RDP |
| ..DenyService | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | Denied service / batch login |
| Protected Users | ✅ |  |  |  |  |  | ✅ |  | Members of this group are afforded additional protections against authentication security threats. See http://go.microsoft.com/fwlink/?LinkId=298939 for more information. |

### Org RBAC Chart

| Right | Owner | Operator | Svc-Remote-Admin | User | Description |
| --- | --- | --- | --- | --- | --- |
| ..App-access | ✅ | ✅ |  | ✅ | Allowed Log in to applications (e.g. Web UIs). |
| ..App-modify | ✅ | ✅ |  | ✅ | Poweruser or modify access to applications (e.g. Web UIs) |
| ..App-admin | ✅ |  |  |  | Admin access to applications (e.g. Web UIs) |
| ..AddEndpoint | ✅ | ✅ |  |  | Allowed to create / join computer objects |
| ..GPOAudit | ✅ | ✅ |  |  | Allowed to run RSOP and GPO Modelling |
| ..GPOLink | ✅ |  |  |  | Allowed to link and unlink GPOs in org |
| ..GPOEdit | ✅ | ✅ |  |  | Edit rights on All GPOs |
| ..OUCreate | ✅ |  |  |  | Create and update arbitrary Organizational Units |
| ..OUManage | ✅ |  |  |  | Manage arbitrary OUs, properties and permissions on ACLs |
| ..Rights-Admin | ✅ |  |  |  | Create and delete new rights, and modify membership of all groups. |
| ..Roles-Manage | ✅ |  |  |  | Create and delete new roles, and modify membership of roles. |
| ..LAPSReadPassword | ✅ | ✅ |  |  | Fetch local machine passwords via Windows LAPS |
| ..WindowsAdmin | ✅ | ✅ | ✅ |  | Local admin rights on Windows / Linux hosts |
| ..WindowsOps |  |  |  |  | Limited operator rights on Windows hosts: event log, performance monitoring, network changes |
| ..ServiceAcct-MSA | ✅ | ✅ |  |  | Create, Delete, and manage sMSA and gMSAs |
| ..ServiceAcct-Legacy | ✅ |  |  |  | Create, Delete, and manage legacy service accounts |
| ..sudo_full | ✅ |  |  |  | Linux Sudo: Unrestricted / full root. |
| ..sudo_full-nopw |  |  | ✅ |  | Linux Sudo (no passwd): Unrestricted / full root |
| ..sudo_operate |  | ✅ |  |  | Linux Sudo: Read logs, manage services |
| ..sudo_configure |  | ✅ |  |  | Linux sudo: change network settings, time, firewall, and installed packages |
| ..LogonLocal | ✅ | ✅ |  |  | Rights for Local logon |
| ..LogonRemote | ✅ | ✅ | ✅ |  | Allow log on through Remote Desktop Services /SSH. |
| ..LogonBatch |  |  |  |  | Rights for batch logon / scheduled task / cron access |
| ..LogonService |  |  |  |  | Rights for logon as service in this Org |
| ..DenyRemote |  |  |  |  | Denied login via SSH / RDP |
| ..DenyService | ✅ | ✅ | ✅ | ✅ | Denied service / batch login |
| ❖Right-Global-GPOCreate | ✅ |  |  |  | Allowed to create new GPOs. Replaces GPOCreatorsOwners, but without rights to default domain GPOs |

### Component RBAC Chart

| Right | Owner | Operator | Svc-Local | User | Description |
| --- | --- | --- | --- | --- | --- |
| ..App-access | ✅ | ✅ |  | ✅ | Allowed Log in to applications (e.g. Web UIs). |
| ..App-modify | ✅ | ✅ |  | ✅ | Poweruser or modify access to applications (e.g. Web UIs) |
| ..App-admin | ✅ | ✅ |  |  | Admin access to applications (e.g. Web UIs) |
| ..AddEndpoint | ✅ | ✅ |  |  | Allowed to create / join computer objects |
| ..GPOAudit | ✅ | ✅ | ✅ |  | Allowed to run RSOP and GPO Modelling |
| ..GPOLink | ✅ |  |  |  | Allowed to link and unlink GPOs in org |
| ..GPOEdit | ✅ | ✅ |  |  | Edit rights on All GPOs |
| ..Rights-Admin | ✅ |  |  |  | Create and delete new rights, and modify membership of all groups. |
| ..Roles-Manage | ✅ |  |  |  | Create and delete new roles, and modify membership of roles. |
| ..OUCreate |  |  |  |  | Create and update arbitrary Organizational Units |
| ..OUManage |  |  |  |  | Manage arbitrary OUs, properties and permissions on ACLs |
| ..LAPSReadPassword | ✅ | ✅ |  |  | Fetch local machine passwords via Windows LAPS |
| ..ServiceAcct-MSA | ✅ | ✅ |  |  | Create, Delete, and manage sMSA and gMSAs |
| ..ServiceAcct-Legacy | ✅ |  |  |  | Create, Delete, and manage legacy service accounts |
| ..WindowsAdmin | ✅ | ✅ | ✅ |  | Local admin rights on Windows / Linux hosts |
| ..WindowsOps |  |  |  |  | Limited operator rights on Windows hosts: event log, performance monitoring, network changes |
| ..sudo_full | ✅ |  |  |  | Linux Sudo: Unrestricted / full root. |
| ..sudo_full-nopw |  |  | ✅ |  | Linux Sudo (no passwd): Unrestricted / full root |
| ..sudo_operate |  | ✅ |  |  | Linux Sudo: Read logs, manage services |
| ..sudo_configure |  | ✅ |  |  | Linux sudo: change network settings, time, firewall, and installed packages |
| ..GenerateSecurityAudit |  |  | ✅ |  | Primarily for use by ADFS and IIS service accounts. Granted via GPO |
| ..SeTcbPrivilege |  |  | ✅ |  | 'Act as part of operating system' rights. Usually only needed for services. |
| ..LogonLocal | ✅ | ✅ | ✅ |  | Rights for Local logon |
| ..LogonRemote | ✅ | ✅ |  |  | Allow log on through Remote Desktop Services /SSH. |
| ..LogonBatch |  |  | ✅ |  | Rights for batch logon / scheduled task / cron access |
| ..LogonService |  |  | ✅ |  | Rights for logon as service in this Org |
| ..DenyRemote |  |  | ✅ |  | Denied login via SSH / RDP |
| ..DenyService | ✅ | ✅ |  | ✅ | Denied service / batch login |


## Appendix 2: Creating AD Addons

AD supports GUI addons. These are deployed by done by creating a 'DisplaySpecifier' object under `CN={Codepage},CN=DisplaySpecifiers,CN=Configuration,DC=Lab,DC=local` (English codepage is 409). Its commonname should be `CN={ldapDisplayName of objectClass}-Display`. For example, `CN=msDS-GroupManagedServiceAccount-Display`. Hint: English codepage is 409.

The `ad-rbac` module includes a module called `addsinfo` which has a function for creating these addons:

```Powershell
import-module ...\ADDSInfo
set-dsAdminContextMenu -LDAPObjectName OrganizationalUnit -MenuText "✅ Launch Management Shell" -Command "\\lab.local\NetLogon\AD-Management\LaunchCommandShell_OU.bat" -Action Add -verbose
```