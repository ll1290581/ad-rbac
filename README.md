# ad-rbac
AD out of the box makes it difficult to enact best practice around delegation and least privilege. Much of it is manual, or requires in depth knowledge, which in turn requires extensive documentation.

I believe an "opinionated" take on how to organize AD, plus a set of scripts and templates, can make many things easy:
- Replicating "Joe's" rights for new employee "Bill" without granting unnecessary access
- Auditing / confirming that everyone's access is as it should be
- Applying regulatory policies (e.g. STIG) without causing mass breakage
- Integrating Linux into AD, and letting AD define authorizedKeys, sudoers, and sshd HBAC rules to minimize local configuration burden
- Reflecting organizational realities via Active Directory permissions: who owns what, and who is allowed to change what
- Using modern tools like LAPS and MIM/PAM

## Terminology
I use the following terms / OUs / naming conventions: 
- **Org**: A business unit, tenant, department, or other grouping with an owner. This is business-oriented. "Infrastructure" or "SoftwareDev" would be examples 
- **Component**: Software, stack, or project with an owner, and a common lifecycle. "VMWare"; "ServiceNow" are examples. All components are owned by an Org 
- **Rights**: A domain-local security group that grants a specific privilege, like "joinDomain" or "Reset Password", to a single thing
- **Role**: A collection of 'rights' groups that are applied to a user
- **Endpoint**: A computer object joined to the domain. This includes kerberos / LDAP-capable devices like Switches and Appliances

## Approach
This script module defines templates for what the "Orgs" and "Components" OU trees should look like, with default rights, roles, OU structure, GPOs, and AD Delegations. Deploying an Org only requires a name and description, while components also require the name of the owning org.

It provides some main functions to ease these tasks:
- add-rbac:    Sets the directory up by creating a "LinuxFeatures" and "Orgs" OU to hold all objects
- add-rbacorg
- add-rbacComponent
- get-rbacOrg
- get-rbacComponent

These functions have corresponding "remove-" versions, and should leave behind no traces of the structures. They all support the "-whatif", "-confirm", and "-verbose" flags, but by default will display actions being taken.

## Getting started
The quickest way to get started with this is to download this module and run it with an account that has permissions to create OUs at the root of the domain, redirect the computer / user default location, and create GPOs; for many environments this will be the Domain Admin.

> Note that the add-adschemamod commands are optional, and will irreversibly modify your Domain's schema. They add the ability to link an ssh key with a user account, and to define sudoers via LDAP objects. You can skip them, and the script will adjust to exclude those features.
```
import-module -path [PathToModule]\ad-rbac.psm1
add-adschemamod -name sshpublickey
add-adschemamod -name sudoRoles
add-rbac -whatif
add-rbac
add-rbacOrg -org "Infra" -description "Core Production Infra"
Add-rbacComponent -org "Infra" -component "Updates" -description "WSUS, Bigfix, and Satellite"
Add-rbacComponent -org "Infra" -component "Virtualization" -description "vSphere and HyperV"
Add-rbacComponent -org "Infra" -component "ServiceNow" -description "ITSM and ticketing"
```
At this point you can join some computers to the domain, move them into their respective components 'Endpoints' OU, and create some users. Linux hosts should use the sample configs (for sshd_config, sssd.conf, and nsswitch.conf) in the /misc folder. Once you have done so, synchronize the linux netgroups to reflect OU membership:
 
```
sync-rbacNetgroups
```
Now you should be able to ssh in using public keys associated with your user account, and `sudo` should work for members of the sudoers_full and sudoers_operate groups.

## Installation
This module is published on the Powershell Gallery [here](https://www.powershellgallery.com/packages/ad-rbac/), and can be installed using `install-module ad-rbac`.

## Contributing and Support
If you have any questions or wish to contribute, please feel free to email me at john@breakwaterlabs.net.

## Roadmap
LAPS functionality was recently added.

Future wishlist features are:
 * Helper commands for: 
   * Move-RBACEndpoint -source [ORG-COMPONENT] -destination [ORG-COMPONENT]
   * User add (Do we need?)
   * Add-RBACUserProperty -Component|ORG -role X -SSHKey X
   * Remove-RBACUserProperty -Role X -sshkey X
   * Export-RBACLinuxConfigs (Generate sssd.conf, sshd_config, laps-runner.json, nsswitch.conf, authselect????)
   * Add-RBACServiceAccount -type [MSA | Legacy] ....
   * Install-rbacSudoroleService (using MSA....)
   
 * Store configuration in Active Directory 
 * Allow adding "flavors" of components, e.g. ADFS, DHCP, etc with corresponding GPO templates
 * Ability to choose 2- or 3- tiered structure (e.g. with tenants)

## Authors and acknowledgment
Parts of this code use the MIT-licensed GPRegistryPolicyParser, which can be found at https://github.com/PowerShell/GPRegistryPolicyParser

## License
Currently, this is licensed AGPLv3. If you have a usecase that requires a different license, please reach out to me.

## Project status
This is in active development as of 9/13/2023.
