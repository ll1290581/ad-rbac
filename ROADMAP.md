# Roadmap

## Painpoints
During ADCS deployment, component model has issues under STIG:
 * Wants enterprise admin and local admin for many functions
 * solved with a gpo override for the deny policy, and adding to local admin....
 * I should just auto-create a PKI OU with correct DACLs, and a GPO granting enterprise admins access as this is required and inferred

## Service account make
- Inputs: 
  - Component
  - 15-character description (PKI-CEPCES)
	- DNS Name
	- hosts (array)
	- personality (IIS; ADFS; PKI)
		- ADFS adds "generate security audit"
		- IIS adds IIS_IUSRS membership
		- PKI adds ACLs for issuance for CEP/CES

- Output
	- new "rights" group? could make admin easier, or more complicated: gmsa-members-[component]-gmsa_[gmsaName]
	- new gMSA under service accounts with SPNs set to http/dnsname


new-adserviceAccount -name "msa-[NAME]" -DNS 

## LAPS:
 - Should have a role for "people who can force a LAPS reset": (Taken from lapspsh.dll SetLapsADResetPasswordPermission)
             @{ # lapspsh.dll SetLapsADResetPasswordPermission 
                ADPathLeaf = ""
                Principal = "Right-Whatever-LAPSForceReset"
                ADRight = "ReadProperty, WriteProperty"
                TargetObject = "ms-LAPS-PasswordExpirationTime"
                AppliesTo = "Computer"
                InheritanceType = "Descendents"
            }
 - Should add auditing (See lapspsh.dll setLapsADAuditing)
 - Would be really nice to have a way to stick arbitrary passwords in for non-managed accounts


## Default GPOs:
 - Airgap:
   - (pwsh slow startup) public key policies / Cert Path Validation Settings / Net Retrieval / disable Microsoft cert validation 
	 - Admin Templates \
	   - Control Panel \ Allow Online Tips (DISABLE)
		 - Microsoft Edge \ Smart Screen \ Force Microsoft Defender Checks..... (DISABLE)
		 - System \ Internet Comm Management / Internet Comm Settings / Turn Off Automatic Root Cert... (Enabled)
		 - Windows Components /
		   - File Explorer / Configure Windows Defender SmartScreen (DISABLED)
		   - Software Protection Platform / Turn off KMS Client Online AVS .... (Enabled)
			 - Windows Defender SmartScreen /
			   - Explorer
			     - Configure App Install Control (DISABLED)
  				 - Configure Windows Defender SmartScreen (DISABLED)
			   - Microsoft Edge 
				   - Configure Windows Defender Smartscreen (DISABLED)
					 - Prevent Bypassing prompts for sites... (Disabled)
			 - Windows Error Reporting / 
			   - Disable Windows Error Reporting (Enabled)
				 - Advanced Error Reporting Settings / Default Application Reporting Settings
				   - Disable / Disable / do not report
			 - Windows Media DRM / Prevent Windows Media Internet Access... (ENABLED)
			 - Windows Powershell / Set Default Path for update-help????
	- Prefs / Registry:
	  - Fix "about:security_mmc.exe"
		  HKLM\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\security_mmc.exe ; about; DWORD; 0x2
		- Domain trust for SMB
		  HKLM\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\$DNSDOMAIN ; file; DWORD; 0x1
		- Disable SmartScreen
		  HKLM\Software\Policies\Microsoft\Windows\System; EnableSmartScreen; DWORD; 0x0
		- SMB settings
		  HKLM\System\CurrentControlSet\Services\lanmanserver\Parameters
			 - Enncryptdata
			 - SMB2
			 etc


## Enhance Linux integration via SSSD

Much of the linux integration works well-- with sudo, SSH (pubkeys, GSSAPI, HBAC), and authentication pulling from AD.
However there are a few gaps that are trivially fixable:

### Automatic Sudo Netgroup population
Currently, sudoroles reference netgroup objects that have to manually be synced to reflect the org-component membership. This is, frankly, horrible, clunky, and raises issues:
 * do you schedule the script? 
 * What service account does it use, that you trust with implict full `su` to all *nix boxen?
 * How often does the script run-- rebuilding the objects thrashes AD, but too infrequent makes administration a pain as you wait for the sssd smart refresh to align with your script
 
But two minor changes to `sssd-ad` would remedy this.
 * Option to dynamically create a virtual netgroup in sssd's SYSDB named for the computer object's parent OU
	* Example: `lab.local/orgs/infra/components/endpoints`, populated with all resolved members of the OU
	* Now your sudoHost can reference this virtual group, and the getent calls will resolve it
	* Now OU membership can be used by other applications, e.g. for access control
 * Option to use templating in `ldap_search_base` based on the computer object's parent OU
    * example:
	  `ldap_sudo_
	  `ldap_sudo_search_base = %OUTEMPLATE/sudoroles?subtree?
 
 * A 'ldap_derive_virtual_netgroup' option which takes the parent OU's cn (lab.local/orgs/infra/components/endpoints)
 * A 'ldap_virtual_netgroup_pattern' option which defines how that CN string is built, for instance:
   `ldap_derive_virtual_netgroup = %domain/%parentOU` --> `lab.local/orgs/infra/components/endpoints`