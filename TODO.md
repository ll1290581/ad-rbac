## Unnesting effort: 
 - [VERIFY] Org-level DACLs for component endpoints, groups etc read/write/create should be confirmed applied at Components parent OU with inheritance
 - [X] DACLs on Endpoints, Rights, Roles etc should use "deny" to prevent creation of objects in wrong spots. This avoids group nesting and keeps DACLs simple.
 - [X] Org-level GPO / Restricted Groups should use MemberOf to inject org-level and Global localadmin into component level restrictedGroups
 - [X] Component-level GPO / RestrictedGroups should use "Members" to inject global and component-level localadmin
 - [X] LAPSEncryptor / Reader should be consolidated -- ensure correct permissions are set
 - [ ] Multiple sudo definitions should merge, so all group nesting should be removed and tested
 - [X] All GPO delegations should be directly assigned.
 - [ ] Local / Batch / Service Logon should be handled as restrictedGroups using local group membership, in the same way as localadmin
 - [X] App-Access should not nest under modify / admin.
 - [X] Only remaining nested groups shoulld be "App-Access", "App-Modify", "App-Admin". (And LAPS)
 - [ ] ???? All groups should get the "Self-Membership" / "bf9679c0-0de6-11d0-a285-00aa003049e2" extended right on write-property for inheritedObjectType 000..0000
 - [ ] ??? Managed-by should belong to org-level for all groups???



## Function Updates

add-oupermission.ps1: 
 - [ ] Update extendedright logic: "extendedRight" should be added to ADRight if not present
 - [ ] ExtendedRight ArgumentCompleter should be updated to support completion based on letters entered
 - [ ] AppliesTo ArgumentCompleter should be updated to support completion based on letters entered
 - [ ] ADRighths should use [system.securityServices.ActiveDirectoryRights].GetEnumNames()
 - [ ] AppliesTo argumentCompleter / validation should check that $_.appliesto contains the $TargetObject (AppliesTo is extendedRight under "CN=Extended-Rights,$((Get-ADRootDSE).ConfigurationNamingContext)")
 - [ ] Type on AppliesTo -- can it be changed? Might just be string enum'd from rootDSE


get-ouacls:
 - [ ] Rename function to "Get-RADACLs" to reflect that its not just OUs.

## Features:

### ADUC GUI Integration

Provide native GUI ADUC-based support for scripting tools.

Details: Modifying display-Specifiers / AdminContextMenu in AD allows the ADUC (dsa.msc) GUI to natively call scripts / executables. This would let an admin right-click on a component and get a "Create RBAC Component" menu item that automatically creates the folder structure.

To Do: 
 - [ ] Create a batch launcher into powershell
 - [ ] Create low-overhead powershell script that can determine node type
 - [ ] If it is a supported type, generate a text menu of options (potentially loading the full module and prompting for needed parameters)
 - [ ] Create a function to add / remove the display-specifier objects
 - [ ] OU functions: 
   - [ ] Add Component
   - [ ] Add Org
   - [ ] Create new gSMA
 - [ ] Computer Object functions
   - [ ] Generate Linux configs
   - [ ] Set LAPS password for non-Windows hosts
 - [ ] User functions
   - [ ] Manage SSH PublicKeys

### Application Settings

Provide native storage of RBAC settings

Details: Many of the functions make assumptions about e.g. object names. I may want to change these in upstream code, but not break downstream users. By storing these settings in an ms-ds-App-Configuration object, I can ensure that the script uses consistent object names.

To Do: 
 - [ ] Make a function for reading / writing  key-value pairs into ms-ds-settings attribute
 - [ ] set this up to run at add-rbac instantiation

### LAPS Integration 

Windows LAPS can be made to act as a password vault.

Details: Windows LAPS by design wants to integrate with windows hosts only, not other hosts like Linux or vCenter. But I have already written a script that will the storage of arbitrary usernames / passwords into the LAPS attributes in ways that AD will support. This should be baked in as a sub-module, and provided a ADUC GUI method for inputting new passwords

To Do: 
 - [ ] Test existing LAPS code against lab instance
 - [ ] Create submodule (RAD-LAPS)
 - [ ] Create AdminContextMenu launcher

### Extend GUI for gMSAs

Many newer objects like gMSAs have poor support in ADUC. This can be fixed.

Details: The property pages displayed in ADUC depend on display-specifiers (AdminPropertyPages). By copying some of the existing CLSIDs to poorly supported objects, we can gain much better GUI support (e.g. for gMSA group membership).

To Do: 
 - [ ] Identify relevant CLSIDs for gMSAs
     - [ ] SPN setting
     - [ ] Group Membership
     - [ ] LAPS????
 - [ ] Create function to add / remove this feature as part of instantiation
 - [ ] Create contextMenu item?

### Provide Linux config generator code

Some features like sudoRoles need custom linux configurations. This should be provided.

Details: I should finish out the scripts that generate sssd.conf, sshd_config, and nsswitch.conf. Support for authselect should also be provided, with a one-shot bash script that can be pasted into a linux host.

To Do: 
 - [ ] Validate existing config samples
 - [ ] Finish function to generate test code
 - [ ] Integrate with ADUC 