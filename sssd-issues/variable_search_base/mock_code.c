/* see sssd/src/util/sss_nss.c line 24 for homedir templating */
/* code around walking the tree and canonicalizing taken from samba/lib/ldb/common/ldb_dn.c */
/* also base dn parsing defined at https://github.com/breakwaterlabs/sssd/blob/233a846e864fe2a364e05d08c3ae91475b5916d1/src/providers/ldap/sdap.c#L1133C1-L1134C1*/
/* Search bases seem to be stored under sdom->search_bases[] and are defined  
https://github.com/breakwaterlabs/sssd/blob/233a846e864fe2a364e05d08c3ae91475b5916d1/src/providers/ldap/sdap_domain.c#L69
sdap_domain_get_by_dn which populates a passed pointer to *opts

providers / ldap / ldap_common.c seems key
*/

/* Given a dn, -x removes x components, x returns at most x components, and 0 returns null */
    /* Hook in where ldap_search_base is fetched:
    struct ldb_dn *ldap_template_dn;
    ldap_template_dn = ldb_dn_copy(mem_ctx, computer_dn)
    get_linearized_relative_dn(mem_ctx, ldap_template_dn, ldap_search_base_template_relationship);
*/
static int 

static int *get_linearized_relative_dn (TALLOC_CTX *mem_ctx, 
                                         struct ldb_dn *dn,
                                         int rel_level) 
{
    unsigned strip_levels = 0;
    struct ldb_dn *rel_dn = NULL;
    char *rel_dn_linearized = NULL;
    
    if (abs(rel_level) > dn->comp_num) {
        return NULL;
    }
    if ( rel_level < 0) {
        strip_levels = abs(rel_level);
    } else if (rel_level > 0 ) {
        strip_levels = (dn->comp_num) - rel_level;
    } else {
        return NULL;
    }

    rel_dn = ldb_dn_copy(mem_ctx, dn);
    if (!ldb_dn_remove_child_components(rel_dn, strip_levels)) return NULL;
    rel_dn_linearized = ldb_dn_get_linearized(rel_dn);
    talloc_free(rel_dn);

    return rel_dn_linearized;
}