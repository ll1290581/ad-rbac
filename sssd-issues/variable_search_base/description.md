# Implement support for variables in ldap_*_search_base 

Consider the following OU structure, from the perspective of an administrator desiring to apply sudoRoles to the Linux clients.
```
DC=Contoso,DC=Net
    |-> OU=Prod
    |   |-> OU=Openshift
    |   |   |-> OU=Sudoroles
    |   |   |   |-> CN=sudorole-Openshift 
    |   |   |           sudoHost = +Prod/Openshift/Computers
    |   |   |           sudoGroup = %Right-Prod-Openshift-SudoAll
    |   |   |           sudoCommand = ALL
    |   |   |
    |   |   |-> OU=Computers
    |   |       |-> CN=Linux01
    |   |
    |   |-> OU=Satellite
    |       |-> OU=Sudoroles
    |       |   |-> CN=sudorole-Openshift 
    |       |           sudoHost = +Prod/Satellite/Computers
    |       |           sudoGroup = %Right-Prod-Satellite-SudoAll
    |       |           sudoCommand = ALL
    |       |
    |       |-> OU=Computers
    |           |-> CN=Linux02
    |
    |-> OU=Staging
    ...
```
Setting `ldap_sudo_search_base` to the root of the domain (or prod) is dangerous because it exposes all of the systems to sudoroles that may be controlled by administrators for one of the leaf OUs; for instance administrators for the Openshift OU may create a rogue sudoRole object with sudoHost = All that grants them access to systems under the Satellite OU.

But requiring a separate sssd.conf for each of these systems violates the design principal of AD and sssd, which is to automatically pull these policies in from the directory instead of using local config. See for instance the sudoers service or GPO-based access control, which defer those policies to the directory.

There are two common approaches to using sudoRoles that an administrator might use. One is to have a single central OU where those objects are stored. The other is to have a standard, repeating OU structure (as seen above) and delegate permissions to various administrative groups at the heirarchy level.

Both of these scenarios can be accomodated with two simple configuration additions under the DOMAIN section.

The first is described in another issue ([6937](https://github.com/SSSD/sssd/issues/6937)).

The second is to provide a variable, based on the computer object's directory path, that can be referenced by the ldap_sudo_search_base setting (and others like it). This would allow the setting to follow the computer object.

* Name: ldap_uri_relative_token
  * Type: int 
  * Description: defines variable ($LDAP_URI_TOKEN) for use in other ldap searchbase settings  referencing a relative LDAP URI for netgroup searching. 
    * zero (0) returns the full URI of the computer: 
    `CN=Linux01,OU=Computers,OU=Openshift,OU=Prod,DC=Contoso,DC=Net`
    * Positive intergers grab the first X tokens of the computer's URI path: 
    `2` -->  `DC=Contoso,DC=net`
    * Negative integers remove the last X tokens from the computer's URI
    `-2` --> `OU=Openshift,OU=Prod,DC=Contoso,DC=Net`
  * 

Below is an example config, which would allow all SSSD clients in the domain to correctly pick up and process the relevant sudoroles
```
...
[DOMAIN/CONTOSO.NET]
ldap_uri_relative_token = -2
provide_virtual_netgroup = true
ldap_sudo_search_base = OU=SudoRoles,$LDAP_URI_TOKEN?oneLevel?
```