Function Remove-RBACOrg {
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline )]
        [ValidateScript({[bool](get-rbacOrg -org $_)})]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            (get-rbacOrg -org "$wordToComplete*").Org
        })]
        [String]$Org,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    Begin {

        $shouldProcess = @{
            Confirm = [bool]($ConfirmPreference -ne "none")
            Whatif = [bool]($WhatIfPreference.IsPresent)
        }
    }

    Process {
        if ($PsItem.org) {
            $Org = $_.Org
        } elseif ($PsItem) {
            $Org = $_
        }

        if ($Org) {
            $OrgList = get-rbacOrg -org $org -server $server
        } else {
            $OrgList = get-rbacOrg  -server $server
        }
        foreach ($orgObject in $orgList) {
            write-Host ("Moving any computer objects out to Global @ {0}" -f $Settings.OUPaths.DefaultComputers )
                $EndpointList = get-RBACComponentEndpoints -org ($orgObject.Org)  -server $server
                foreach ($endpoint in $endpointList) {
                    $MoveParams = @{
                        identity = $endpoint.distinguishedName
                        TargetPath = $Settings.OUPaths.DefaultComputers
                    }
                    $moveParams
                    move-adobject -server $server @MoveParams @shouldProcess
                }
                $RemovePath = $OrgObject.DistinguishedName
                if ($PSCmdlet.ShouldProcess($removePath,"Removing protection and deleting OU Subtree")) {
                    DeleteOUSubtreeWithConfirm -path $removePath @ShouldProcess -server $server
                }
        }
    }
}