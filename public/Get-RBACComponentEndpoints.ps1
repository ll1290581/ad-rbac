Function get-RBACComponentEndpoints {
    [CmdletBinding(DefaultParameterSetName='None')]
    Param
    (
        [Parameter(ParameterSetName = 'None', ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [Parameter(ParameterSetName = 'SpecificComponent', Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ValidateScript({ [bool](get-rbacOrg -org $_) })]
                [Parameter(ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            (get-rbacOrg -org "$wordToComplete*").Org
        })]
        [String]$Org,

        [Parameter(ParameterSetName = 'SpecificComponent', ValueFromPipelineByPropertyName, ValueFromPipeline)]
                [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            if ($fakeBoundParameters.containsKey('Org')) {
                (get-rbacComponent -org $fakeBoundParameters.Org -component "$wordToComplete*"  | sort-object -unique Component).Component
            } else {
                (get-rbacComponent -component "$wordToComplete*").Component
            }
        })]
        [String]$Component,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    Process{
        if ($PsItem.org) { $Org = $_.Org}
        if ($PsItem.Component) {$Component = $_.Component}
        if ($Org) {
            $OrgList = get-rbacOrg -org $org -server $server
        } else {
            $OrgList = get-rbacOrg -server $server
        }
        foreach ($orgObject in $orgList) {
            if ($component) {
                $ComponentList = get-RBACComponent -org $orgObject.org -component $Component
            } else {
                $ComponentList = get-RBACComponent -org $orgObject.org
            }
            foreach ($componentObject in $componentList) {
                $ComponentPath = $ComponentObject.distinguishedName
                $EndpointPath = "OU={0},{1}" -f $Settings.Names.EndpointsOU, $ComponentPath
                Get-ADComputer -server $server -SearchBase $EndpointPath -Filter * -properties OperatingSystem,OperatingSystemVersion| select-Object @{n="Endpoint";e={$_.name}},@{n="Component";e={$Component}},@{n="Org";e={$Org}},OperatingSystem,OperatingSystemVersion,DistinguishedName
            }
        }
    }
}
