Function Get-RADSettings {
  <#
      .SYNOPSIS
      Reads in settings for RAD module
      .DESCRIPTION
      This will search AD for an MS-DS-App-Configuration object named "PS.RAD".
      If it exists, it will normalize the settings and return them.
      If it does not exist, it will initialize settings with defaults and return them.

      .INPUTS
      none
      .OUTPUTS
      JSON
  #>
  [CmdletBinding()]
  Param(
    [String]$SettingsCollection="Current"
  )
  Begin {
    $moduleData = $MyInvocation.MyCommand.Module
    $date = (get-date)
    $Domain = (get-ADDomain)
    $DomainDN = $Domain.DistinguishedName
    $SettingsPath = "CN=Program Data,{0}" -f $DomainDN
    }
    Process {
      try {
        $SettingsList = (get-adobject -Identity "CN=$($moduleData.PrivateData.Names.SettingsObject),$SettingsPath"  -properties "msDS-Settings").'msds-settings'.valueList
      } catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
        write-loghandler -level "warning" -message "Settings do not exist. Creating defaults..."
        $RADRoot = "OU={0},{1}" -f $moduleData.PrivateData.Names.TopLevelOU, $DomainDN

        $oidcmd = "JABTAEkARAAgAD0AIAAoAFsAcwB5AHMAdABlAG0ALgBzAGUAYwB1AHIAaQB0AHkALgBwAHIAaQBuAGMAaQBwAGEAbAAuAHMAZQBjAHUAcgBpAHQAeQBJAGQAZQBuAHQAaQBmAGkAZQByAF0AOgA6AG4AZQB3ACgAJAAoAFsAYQBkAHMAaQBdACIATABEAEEAUAA6AC8ALwAkACgAJABlAG4AdgA6AFUAcwBlAHIARABOAFMARABvAG0AYQBpAG4AKQAiACkALgBvAGIAagBlAGMAdABTAEkARAAuAHYAYQBsAHUAZQAsADAAKQApAC4AdgBhAGwAdQBlAAoAJABTAHQAcgBlAGEAbQAgAD0AIABbAEkATwAuAE0AZQBtAG8AcgB5AFMAdAByAGUAYQBtAF0AOgA6AG4AZQB3ACgAWwBiAHkAdABlAFsAXQBdAFsAYwBoAGEAcgBbAF0AXQAkAFMASQBEACkACgAkAFMASQBEAEgAYQBzAGgAIAA9ACAAJAAoAGcAZQB0AC0AZgBpAGwAZQBIAGEAcwBoACAALQBpAG4AcAB1AHQAUwB0AHIAZQBhAG0AIAAkAHMAdAByAGUAYQBtACAALQBhAGwAZwBvAHIAaQB0AGgAbQAgAFMASABBADIANQA2ACkALgBoAGEAcwBoAAoAJABDAHUAcgByAGUAbgB0AFQAaQBtAGUAIAA9ACAAKABnAGUAdAAtAGQAYQB0AGUAKQAuAHQAbwBGAGkAbABlAFQAaQBtAGUAVQBUAEMAKAApAAoAJABTAHQAcgBlAGEAbQAgAD0AIABbAEkATwAuAE0AZQBtAG8AcgB5AFMAdAByAGUAYQBtAF0AOgA6AG4AZQB3ACgAWwBiAHkAdABlAFsAXQBdAFsAYwBoAGEAcgBbAF0AXQAkACgAIgAkAFMASQBEAEgAYQBzAGgAIgAgACsAIAAiACQAQwB1AHIAcgBlAG4AdABUAGkAbQBlACIAKQApAAoAJABDAGgAZQBjAGsAcwB1AG0AIAA9ACAAJAAoAGcAZQB0AC0AZgBpAGwAZQBIAGEAcwBoACAALQBpAG4AcAB1AHQAUwB0AHIAZQBhAG0AIAAkAFMAdAByAGUAYQBtACAALQBhAGwAZwBvAHIAaQB0AGgAbQAgAFMASABBADIANQA2ACkALgBoAGEAcwBoAAoAJABKAFMATwBOAFMAdAByAGkAbgBnACAAPQAgACQAKABbAG8AcgBkAGUAcgBlAGQAXQBAAHsACgAgACAAIAAgAEsAVgAgAD0AIAAxAAoAIAAgACAAIABPAEkARAAgAD0AIAAkAEMAdQByAHIAZQBuAHQAVABpAG0AZQAKACAAIAAgACAAUwBIACAAPQAgACQAUwBJAEQASABhAHMAaAAKACAAIAAgACAAQwBTACAAPQAgACQAYwBoAGUAYwBrAHMAdQBtAAoAfQApACAAfAAgAGMAbwBuAHYAZQByAHQAdABvAC0AagBzAG8AbgAgAC0AYwBvAG0AcAByAGUAcwBzAAoAJABCADYANABKAFMATwBOACAAPQAgAFsAcwB5AHMAdABlAG0ALgBjAG8AbgB2AGUAcgB0AF0AOgA6AFQAbwBCAGEAcwBlADYANABTAHQAcgBpAG4AZwAoAFsAcwB5AHMAdABlAG0ALgB0AGUAeAB0AC4AZQBuAGMAbwBkAGkAbgBnAF0AOgA6AFUAbgBpAGMAbwBkAGUALgBnAGUAdABCAHkAdABlAHMAKAAkAEoAUwBPAE4AUwB0AHIAaQBuAGcAKQApAAoAJABCADYANABKAFMATwBOAA=="
        $oid = powershell.exe -encodedCommand $oidcmd
        $Versions = $moduleData.PrivateData.Versions
        $Versions.OriginalModuleVersion = $moduleData.version.toString()
        $Versions.CurrentModuleVersion = $moduleData.version.toString()
        $Defaults = [ordered]@{
            Description = "PS.RAD Core Metadata"
            Base64_OID = $oid
            URL = $moduleData.projecturi.absoluteuri
            InstallVersion = $moduleData.PrivateData.Versions
            InstallDate = $date.ToLongDateString() + " @ " + $date.ToLongTimeString()
            Names = $moduleData.PrivateData.Names
            OUPaths = @{
              RADRoot = $RADRoot
              Settings = "CN=Program Data,{0}" -f $DomainDN
              Global = "OU={0},{1}" -f $moduleData.PrivateData.Names.GlobalOU, $RADRoot
              OrgsBase = "OU={0},{1}" -f $moduleData.PrivateData.Names.OrgsOU, $RADRoot
            }
            FilePaths = @{
              PrimordialTemplates = "\\$($domain.dnsroot)\SYSVOL\$($domain.dnsroot)\$($moduleData.PrivateData.Names.SysvolSettingsPath)\PrimordialTemplates\"
              JSONTemplates = "\\$($domain.dnsroot)\SYSVOL\$($domain.dnsroot)\$($moduleData.PrivateData.Names.SysvolSettingsPath)\JSONTemplates\"
              LDIF = "\\$($domain.dnsroot)\SYSVOL\$($domain.dnsroot)\$($moduleData.PrivateData.Names.SysvolSettingsPath)\LDIF_Imports\"
              Settings = "\\$($domain.dnsroot)\SYSVOL\$($domain.dnsroot)\$($moduleData.PrivateData.Names.SysvolSettingsPath)\settings.json"
            }
            Modules = @{
              Core = @{
                Version = "1.2.0"
                InstallDate = $date.ToLongDateString() + " @ " + $date.ToLongTimeString()
                SettingsObject = "CN=$($moduleData.PrivateData.Names.SettingsObject)"
              }
            }
          }
          $DefaultsJSON = $defaults | convertTo-json -compress
          $SettingsList = @("Current=$DefaultsJSON";"Backup=$DefaultsJSON")
          new-adobject -name $moduleData.PrivateData.Names.SettingsObject -path $SettingsPath -OtherAttributes @{'msDS-Settings'=$SettingsList} -type "msDS-App-Configuration"
      } catch {
        write-loghandler -level "warning" -message "Whoops, this shouldnt happen"
        throw $_
      }
      $SettingsList.where({$_ -like "$SettingsCollection=*"}).replace("$SettingsCollection=","") | ConvertFrom-Json
    }
  }