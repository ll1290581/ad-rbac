Function Add-RBACOrg {
    <#
        .SYNOPSIS
        Adds an "Org" OU structure from a predefined template
        .DESCRIPTION
        This creates a regular OU structure in Active Directory representing a distinct and self-managing business unit or tenant.
        .PARAMETER Org
        The name of the 'org'
        .PARAMETER Description
        The description for the org OU
        .PARAMETER Path
        The distinguishedName for the parent OU of this org
        .EXAMPLE
        add-rbacOrg -Org "Developers" -description "Software engineering team"

        This creates an OU tree at "OU=Developers,OU=Orgs,DC=Contoso,DC=Local"
        It should contain the following children OUs:
          * Components
          * Rights
          * Roles
          * PrivilegedAccounts
        .EXAMPLE
        add-rbacOrg -Org "Developers" -description "Software engineering team" -path "OU=Orgs,DC=Contoso,DC=Local"

        Creates the OU tree at the specified path
        .INPUTS
        System.String
        .OUTPUTS
        $null
#>
    [CmdletBinding(DefaultParameterSetName = "Path", SupportsShouldProcess=$true)]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateLength(1,15)]
        [Alias("Name")]
        [String]$Org,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$Description,

        [Parameter(ParameterSetName = "Path", ValueFromPipelineByPropertyName)]
        [String]$Path = $Settings.OUPaths.OrgsBase,

        [Parameter(ParameterSetName = "DN", ValueFromPipelineByPropertyName)]
        [String]$DistinguishedName,

        [switch]$ResetRoleMembership,

        [switch]$ResetRightsMembership,

        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    BEGIN {
        $shouldProcess = @{
            Confirm = [bool]($ConfirmPreference -eq "low")
            Whatif = [bool]($WhatIfPreference.IsPresent)
            verbose = [bool]($VerbosePreference -ne "SilentlyContinue")
        }
    }
    PROCESS {
        write-loghandler -level "Verbose" -message "Beginning add-org process (DC: $($Server.Hostname))"
        $ResetParams = @{
            ResetRoleMembership = [bool]($ResetRoleMembership)
            ResetRightsMembership = [bool]($ResetRightsMembership)
        }
        if ($PsItem.org) { $Org = $_.Org}
        if ($PsItem.Description) {$Description = $_.Description}
        if ($PsItem.Path) {
            $Path = $PSItem.path
        }
        if ($PSItem.DistinguishedName) {
            $path = $PSItem.DistinguishedName -replace "OU=$org,",""
        }
        #if ($PSCmdlet.ShouldProcess($path,"Creating Org structure for $org @ $path")) {
            Add-OUStructureFromTemplate -name $Org -Description $Description -asOrg @shouldProcess @ResetParams -server $Server
        #}

    }
}