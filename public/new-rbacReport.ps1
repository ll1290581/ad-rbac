<#Function new-rbacReport {
    [CmdletBinding(DefaultParameterSetName='console')]
    Param (
        [Parameter(parametersetname='console',Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [Parameter(parametersetname='file',Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ArgumentCompleter( { @("Global","Org","Component") } )]
        [ValidateScript( { $_ -in @("Global","Org","Component") } )]
        [String]$Report,

        [Parameter(parametersetname='console',ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [Parameter(parametersetname='file',ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ArgumentCompleter( { @("JSON","Table","AsRaw") } )]
        [ValidateScript( { $_ -in @("JSON","Table","AsRaw") } )]
        [String]$displayformat = "table",

        [Parameter(parametersetname='file',Mandatory, ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [ArgumentCompleter( { @("JSON","CSV","CliXML") } )]
        [ValidateScript( { $_ -in @("JSON","CSV","CliXML") } )]
        [String]$format,

        [Parameter(parametersetname='file',ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [String]$path = "$HOME\Documents\",

        [Parameter(parametersetname='file',ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [Switch]$Quiet
    )

    Begin {
    }

    Process {
        $template = switch ($report) {
            "Global" {
                get-rbacGlobal -mock -detailed
            }
            "Component" {
                get-RBACComponent -mock -mockOrg -detailed -org "Test" -Component "Test"
            }
            "Org" {
                get-rbacOrg -mock -detailed -org "Test"
            }
        }

        # Make Role name vertical (for better formatting), so we need to map that to the actual role
        $RoleDisplayNameMapping = [ordered]@{}

        # Create a base object
        # Right | Role1 | Role2 | Role3 | Description

        $ReportLineTemplate = [ordered]@{}
        $ReportLineTemplate.add("Right","")
        foreach ($role in $Template.Roles.getEnumerator()) {
            $roleDisplayName = $role.key -split '' -join "`n"
            $RoleDisplayNameMapping.add($role.
            write-warning $roleDisplayname

            $ReportLineTemplate.add(,"")
        }
        $ReportLineTemplate.add("Description","")
        $ReportLineTemplateObject = [pscustomObject]$ReportLineTemplate

        # Create a list of rights based on the order of the template. We resolve name to shortnamee both here and later, because we can't guarantee that every right is actually mapped to a role.
        $RightsList = [ordered]@{}
        foreach ($right in $template.rights.getEnumerator()) {
            $reportLine = $reportLineTemplateObject.PSObject.Copy()
            $reportLine.Description = $right.value.description
            if ($right.value.name -match ("{0}?{1}-{2}-(.*)" -f $settings.names.RightsPrefix, $settings.names.RightsName, $template.objectMidName)) {
                $reportLine.Right = "..$($matches[1])"
            } else {
                $reportLine.Right = $right.value.name
            }
            $RightsList.add( $right.value.name, $reportLine)
        }

        # Now iterate through roles in earnest and map right/role relationship.
        foreach ($role in $Template.Roles.getEnumerator()) {
            foreach ($right in $role.value.memberof) {
                # This covers rights outside normal RBAC structure like Protected Users
                if (-not $rightsList.contains($right)) {
                    write-warning "Adding aux right $($right) (at role $($role.key))"
                    $reportLine = $reportLineTemplateObject.PSObject.Copy()
                    $reportLine.Description = (get-dsobject -ldapfilter "(&(objectClass=group)(samAccountName=$right))" -properties Description).description.value
                    if ($right -match ("{0}?{1}-{2}-(.*)" -f $settings.names.RightsPrefix, $settings.names.RightsName, $template.objectMidName)) {
                        $reportLine.Right = "..$($matches[1])"
                    } else {
                        $reportLine.Right = $right
                    }
                    $RightsList.add( $right, $reportLine)
                }
                $rightsList[$right].$($role.key) = "$([char]0x2714)"
            }
        }

        # Loop through rightsList one last time, resolving shortNames and Descriptions
        $RightsChart = foreach ($right in $rightsList.GetEnumerator()) {
            $right.value
        }


        if (-not $quiet) {
            switch ($displayformat) {
                "JSON" { $RightsChart | convertTo-JSON }
                "Table" { $RightsChart | format-table -autosize}
                "AsRaw" { $RightsChart }
            }
        }
        if ($format) {
            $dateStamp = Get-Date -format FileDateTime
            $filePath = "$path\RBAC-$Report-$dateStamp.$format"
            write-host ("Writing report: {0,-10} as {1,-5} to {2}" -f $report, $format, $filepath)
            try {
                switch ($format) {
                    "CliXML" {$RightsChart | Export-Clixml -path $filePath  }
                    "CSV" { $RightsChart | export-csv -path $filePath }
                    "JSON" { $RightsChart | convertTo-JSON | out-file $filePath }
                }
            } Catch {
                write-warning $_.exception.getType().fullname
                write-loghandler -level "warning" -message ("Failed to write report: {0,-10} as {1,-5} to {2}" -f $report, $format, $filepath)
                throw $_
            }

        }
    }
}#>