Function Remove-RBACComponent {
    [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact='high')]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateScript({ $(get-rbacComponent).component.contains($_) })]
                [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            if ($fakeBoundParameters.containsKey('Org')) {
                (get-rbacComponent -org $fakeBoundParameters.Org -component "$wordToComplete*"  | sort-object -unique Component).Component
            } else {
                (get-rbacComponent -component "$wordToComplete*").Component
            }
        })]
        [String]$Component,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateScript({[bool](get-rbacOrg -org $_)})]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            (get-rbacOrg -org "$wordToComplete*").Org
        })]
        [String]$Org,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    BEGIN {
        $shouldProcess = @{
            Confirm = [bool]($ConfirmPreference -ne "none")
            Whatif = [bool]($WhatIfPreference.IsPresent)
        }
    }
    Process {
        if ($PsItem.org) { $Org = $_.Org}
        if ($PsItem.Component) {$Component = $_.Component}

        if ($Org) {
            $OrgList = get-rbacOrg -org $org -server $server
        } else {
            $OrgList = get-rbacOrg -server $server
        }
        foreach ($orgObject in $orgList) {
            if ($component) {
                $ComponentList = get-RBACComponent -org $orgObject.org -Component $Component -detailed
            } else {
                $ComponentList = get-rbacComponent -org $orgObject.org -detailed
            }
            foreach ($componentObject in $componentList) {

                write-Host ("Moving any computer objects out to Global @ {0}" -f $Settings.OUPaths.DefaultComputers )
                $EndpointList = get-RBACComponentEndpoints -org $ComponentObject.Org -component $ComponentObject.component
                foreach ($endpoint in $endpointList) {
                    $MoveParams = @{
                        identity = $endpoint.distinguishedName
                        TargetPath = $Settings.OUPaths.DefaultComputers
                    }
                    $moveParams
                    move-adobject -server $server @MoveParams @shouldProcess
                }
                $RemovePath = $ComponentObject.distinguishedName
                if ($PSCmdlet.ShouldProcess($removePath,"Removing protection and deleting OU Subtree")) {
                    DeleteOUSubtreeWithConfirm -path $removePath -Confirm:$false -server $server
                }
                foreach ($GPOItem in ($componentTemplate.GPOs.getEnumerator())) {
                    $GPOName = "{0}-{1}" -f $GPOItem.metadata.NamePrefix, $ComponentObject.ObjectMidName
                    if ($PSCmdlet.ShouldProcess($GPOName,"Deleting GPO")) {
                        get-GPO -name $GPOName -verbose -server $server | remove-GPO -server $server
                    }
                }
            }
        }
    }

}
