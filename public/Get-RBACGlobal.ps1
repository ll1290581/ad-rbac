function Get-RBACGlobal {
    [CmdletBinding(DefaultParameterSetName = 'None')]
    Param
    (

        # Don't search AD-- mock what you would get.
        [Parameter(ParameterSetName = "Mock", Mandatory)]
        [switch]$Mock,

        [Parameter(ParameterSetName = "Mock", ValueFromPipelineByPropertyName, ValueFromPipeline)]
        [String]$Description = "Mock Description",

        [switch]
        $Detailed,
        # Dont resolve parents because this is a recursion.
        [switch]$DontResolveParents,

        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover),

        [switch]$fixGroupMemberships
    )

    Begin {
        $type = "Global"
        Switch ($type) {
            "Global" {
                $BaseDN = $Settings['OUPaths']['Global']
                $ParentGlobal = $false
                $parentOrg = $false
                $template = $GlobalTemplate
            }
            "Org" {
                $BaseDN = $Settings['OUPaths']['OrgsBase']
                $ParentGlobal = $true
                $parentOrg = $false
                $template = $OrgTemplate
            }
            "Component" {
                $ParentGlobal = $true
                $parentOrg = $true
                $template = $ComponentTemplate
            }
        }
        $SearchBase = @{ SearchBase = $BaseDN }
        $SearchParams = @{
            SearchScope = "OneLevel"
            Server      = $server
            Properties  = "Description", "objectSID", "Name", "DistinguishedName"
        }
    }

    PROCESS {
        $(
            if ([bool]$mock) {
                @{
                    Name              = $(split-ldapPath -distinguishedName $BaseDN -leaf -NodeNameOnly)
                    Description       = $Description
                    DistinguishedName = $BaseDN
                }
            }
            else {
                get-dsobject -distinguishedName $baseDN | foreach-object {
                    # todo: Gremlin here. Why is there a null result that doesn't appear in the base object?
                    if ($null -ne $_) {
                        @{
                            Name              = $_.name.value
                            Description       = $_.description.value
                            DistinguishedName = $_.distinguishedName.value
                        }
                    }

                }
            }
        ) | foreach-object {
            $output = @{
                Name              = $_['name']
                Component         = ""
                Org               = ""
                Description       = $_['Description']
                DistinguishedName = $_['distinguishedName']
                Path              = split-ldappath -DistinguishedName $_['distinguishedName'] -Parent
                Type              = $type
                ObjectMidName     = $Settings['Names']['GlobalOU']
            }
            if ([bool]$Detailed) {
                $RBACMark = "$([char]0x2714)" #  ✔   checkthick
                $RightsToProcess = [System.Collections.Generic.List[string]]$template['DefaultRights']
                $RBACReport = [ordered]@{}
                [void]$output.add("RBACReport", [System.Collections.Generic.List[pscustomobject]]::new())
                [void]$output.add("Rights", [ordered]@{})
                [void]$output.add("Roles", [ordered]@{})
                [void]$output.add("Parents", [ordered]@{})
                [void]$output.add("Children", [ordered]@{})
                if (-not $DontResolveParents) {
                    if ($parentGlobal) {
                        [void]$output['Parents'].add("Global",$(get-rbacGlobal  -server $server -detailed -DontResolveParents))
                    }
                    if ($parentOrg) {
                        [void]$output['Parents'].add("Org",   $(get-rbacOrg -org $orgName -detailed -mock:$mockOrg -server $server  -DontResolveParents))
                    }
                }
                foreach ($child in  $template['LDAPContainers']) {
                    $SplitDN = $(
                        if ($child.distinguishedName) {
                            $child.distinguishedName
                        } else {
                            if ($child.relativePath) {
                                # remove any number of trailing or opening commas
                                $subpath = ",{0}," -f $($child.relativepath -replace '(?<!\\),+$','' -replace '^,+','')
                            } else {
                                $subpath = ","
                            }
                            "OU={0}{1}{2}" -f $child.name, $subpath, $baseDN
                        }
                    ) | split-LDAPPath -asHashtable
                    $templateChild = [ordered]@{}
                    [void]$templateChild.add("DistinguishedName", $SplitDN['DistinguishedName'])
                    [void]$templateChild.add("Description", "$($child.description)" )
                    if ([bool]$mock) {
                        $thisChild = $templateChild
                    } else {
                        $state = "$([char]0x2714)"
                        $thisChild = [ordered]@{}
                        get-dsobject -distinguishedName $templateChild['distinguishedName'] | foreach-object {
                            # I think this is necessary to deal with nulls that appear
                            if ($null -ne $_) {
                                foreach ($attribute in $templateChild.getEnumerator()) {

                                    if ($_.$($attribute.key).value -eq $attribute.value) {
                                        $thisChild[$attribute.key] = $_.$($attribute.key).value
                                    } else {
                                        $state = "$([char]0x274C)"
                                        $thisChild[$attribute.key] = $($_.$($attribute.key).value)
                                    }
                                }
                                $thisChild.insert(0,"State",$state)
                            }
                        }
                    }
                    [void]$output.Children.add($SplitDN['leafName'], [pscustomobject]$thisChild)
                }
                $reportLine = [ordered]@{
                    "Right"  = ""
                }
                # Check for undeclared rights, and precreate a "RbacReport"
                foreach ($role in $template['DefaultRoles']) {
                    foreach ($right in ($role.rights)) {
                        if (-not $RightsToProcess.contains($right)) {
                            write-warning ("Role '{0}' references right '{1}' that is not declared in this template. This is probably an error in the template." -f $role.nameSuffix, $right)
                            $RightsToProcess.add($right)
                        }
                    }
                    $roleReportName = $role.nameSuffix # -split '' -join "`n"
                    $role['ReportColumn'] = $roleReportName
                    #[void]$rolesToProcess.add($role)
                    [void]$reportLine.add($roleReportName,"")
                }
                [void]$reportLine.add("Description","")
                [void]$reportLine.add("FullName","")
                $reportLine = [PSCustomObject]$reportLine

                foreach ($rightName in $RightsToProcess) {
                    $right = $settings['Rights'][$rightName]
                    $Prefix = if ($right.DoNotPrefixGroupName) {
                        ""
                    }
                    else {
                        $settings['Names']['RightsPrefix']
                    }
                    $name = "{0}{1}-{2}-{3}" -f $prefix, $settings['Names']['RightsName'], $output.objectMidName, $right['nameSuffix']

                    if ($right.parent) {
                        $parentName = $right.parent
                    } else {
                        $parentName = $rightName
                    }

                    if (-not $DontResolveParents) {
                        # Start with this group's parent. Iterate through parent objects (which are sequentially defined above) and evalue their parents too.
                        $currentParent = $parentName
                        $parentGroups = @(
                            $output['parents'].getEnumerator() | foreach-object {
                                if ($_.value.rights.contains($currentParent)){
                                    $parentRight = $_.value.Rights.$currentParent
                                    $parentRight.name
                                    # The right name only changes from the child object if 'parent' is defined on the right.
                                    if (-not [string]::IsNullOrWhiteSpace($parentRight.parent)) {
                                        $currentParent= $parentRight.parent
                                    }
                                }
                            }
                        )
                    } else {
                        $parentGroups = @()
                    }
                    $Definition = if ([bool]$mock) {
                        if (-not $DontResolveParents -and $right.addParents) {
                            $members = $parentGroups
                        } else {
                            $Members = @()
                        }
                        [ordered]@{
                            Name = $name
                            Description = if ($right.contains('Description')) {$right['description']} else {$null}
                            Info = if ($right.contains('info')) {$right['info']} else {$null}
                            GroupScope = $settings.AppSettings.RightScope
                            Members = $members
                            Parent=$parentName
                            ResolvedParents = $parentGroups
                        }

                    } else {
                        get-dsobject -DistinguishedName "CN=$name,OU=$($settings['Names']['RightsOU']),$BaseDN" | foreach-object {
                            if ($null -ne $_) {
                                [ordered]@{
                                    name = $_.Name.value
                                    #objectSID = [security.Principal.SecurityIdentifier]::new([byte[]]$_.objectSID.value,0)
                                    Description = $_.description.value
                                    Info = $_.info.value
                                    GroupScope = resolve-dsGroupType -groupType $($_.groupType.value)
                                    Members =  $_.member.value | where-object {-not [string]::IsNullOrWhiteSpace($_)} | foreach-object { $_ | split-ldapPath -leaf -nodeNameOnly }
                                    Parent=$parentName
                                    ResolvedParents = $parentGroups
                                }
                            }
                        }
                    }

                    $thisReportLine = $ReportLine.PSObject.Copy()
                    $ThisReportLine.Right = "..$($right['nameSuffix'])"
                    $ThisReportLine.Description = $Definition.Description
                    $ThisReportLine.FullName = $name
                    [void]$RBACReport.add($rightName,$thisReportLine)
                    [void]$output['rights'].add($rightName, [pscustomobject]$definition)
                }
                foreach ($role in $template['DefaultRoles']) {
                    $protectedRole = [bool]$role.Protected
                    $prefix = if ($protectedRole) {$settings['Names']['RoleProtected']} else { "" }
                    $name = "{0}{1}-{2}-{3}{4}" -f $prefix,$settings['Names']['RolesName'], $output.objectMidName, $role['nameSuffix'], $settings['Names']['RolesSuffix']
                    # Move template role out of conditional-- if this is an actual group we want to be able to compare actuals to template
                    $memberOf = [System.Collections.Generic.List[string]]::new()
                    $auxGroups = [System.Collections.Generic.List[string]]::new()
                    # Add these groups up here-- any undeclared rights should 'stick out' at the bottom of the report.
                    # We also don't want to add 'protected users' just based on a nested membership (protectedRole gets modified below for naming purposes)
                    foreach ($auxGroup in $role['AuxiliaryGroups']) {
                        $auxGroups.add($auxGroup)
                    }
                    if ($protectedRole) {
                        $auxGroups.add("Protected Users")
                    }
                    # We could try to build 'memberOf' and then process the report in one shot later to avoid using auxGroups, but that would require more searches through RBACReport to find the 'right' (which has a shortened name)
                    # This way allows me to directly reference the report line held by the right, which is a shared reference in the RBACReport.
                    foreach ($right in $role.Rights) {
                        if ([string]::IsNullOrWhiteSpace($right)) {
                            write-warning "Continuing on blank"
                            continue
                        }
                        if (-not $output['rights'].contains($right)) {
                            throw "Could not find $right in rightslist"
                        }
                        # Saves a lot of code vs trying to iterate through memberOf and resolve RBACReport lines later....
                        $RBACReport[$right].$($Role.ReportColumn) = $RBACMark
                        if ($settings['Rights'][$right].Protected) {
                            $protectedRole=$true
                        }
                        $memberOf.add($output['rights'][$right].name)
                    }

                    foreach ($right in $auxGroups) {
                        if (-not $memberOf.contains($right)){
                            $memberOf.add($right)
                        }
                        if (-not $RBACReport.contains($right)) {
                            $auxreportLine = $ReportLine.PSObject.Copy()
                            $auxreportLine.Description = (get-dsobject -ldapfilter "(&(objectClass=group)(samAccountName=$right))" -properties Description).description.value
                            $auxreportLine.Right = $right
                            $auxreportLine.FullName = $right
                            [void]$RBACReport.add($right,$auxReportLine)
                        }
                        $RBACReport[$right].$($role.reportColumn) = $RBACMark
                    }

                    $TemplateRole = [ordered]@{
                        Name              = $name
                        Description       = if ($role.contains('description')) {$role['description']} else {$null}
                        Info               = if ($role.contains('Info')) {$role['Info']} else {$null}
                        GroupScope         = $settings['AppSettings']['RoleScope']
                        MemberOf          = $memberOf.where({-not [String]::IsNullOrWhiteSpace($_)})
                    }

                    $Definition = if ([bool]$mock) {
                        $TemplateRole
                    } else {
                        get-dsobject -distinguishedName "CN=$name,OU=$($settings['Names']['RolesOU']),$BaseDN" | foreach-object {
                            if ([String]::IsNullOrWhiteSpace($_)) {
                                # Need to use return in foreach-object or it acts like break.
                                # Remember that foreach and foreach-object are quite different
                                return
                            }
                            if ($null -ne $_) {
                                $memberOf = [System.Collections.Generic.List[string]]::new()
                                $_.memberof.value | where-object {-not [string]::IsNullOrWhiteSpace($_)} | foreach-object {
                                    $memberShortName = $($_ | split-ldapPath -leaf -nodeNameOnly)
                                    $memberof.add($memberShortName)
                                    if (-not $templateRole['MemberOf'].contains($memberShortName)) {
                                        if ($output['Rights'].contains($memberShortName)) {
                                            write-warning "Extra right $memberShortName for role $name"
                                            # TODO: This doesn't really belong here
                                            if ($fixGroupMemberships) {
                                                remove-adgroupmember -identity $templateMember -members $name -verbose
                                            }
                                            $output['Rights'].getEnumerator().where({$_.value.FullName -eq $memberShortName})
                                        } else {
                                            write-warning "Foreign group membership for role $name -- $membershortname"
                                        }
                                        if (-not $rbacReport.contains($memberShortName)) {
                                            $auxreportLine = $ReportLine.PSObject.Copy()
                                            $auxreportLine.Description = (get-dsobject -ldapfilter "(&(objectClass=group)(samAccountName=$memberShortName))" -properties Description).description.value
                                            $auxreportLine.Right = $memberShortName
                                            $auxreportLine.FullName = $memberShortName
                                            [void]$RBACReport.add($memberShortName,$auxReportLine)
                                        }
                                        $rbacReport[$memberShortName].$($role.reportColumn) = "$([char]0x2755)"
                                    }
                                }
                                foreach ($templateMember in $templateRole['Memberof']) {
                                    if (-not $memberOf.contains($templateMember)) {
                                        write-warning "Missing right $templateMember for role $name"
                                        # TODO: This doesn't really belong here
                                        if ($fixGroupMemberships) {
                                            add-adgroupmember -identity $templateMember -members $name -verbose
                                        }
                                        $($rbacReport.values.where({$_.Fullname -eq $templateMember})).$($role.reportColumn) = "$([char]0x2753)"
                                    }
                                }
                                [ordered]@{
                                    name              = $_.Name.value
                                    Description       = $_.description.value
                                    Info              = $_.info.value
                                    GroupScope        = resolve-dsGroupType -groupType $($_.groupType.value)
                                    MemberOf           =  $memberOf
                                }
                            }
                        }
                    }
                    # add Report line to Rights object

                    [void]$output['Roles'].add($role.nameSuffix, [pscustomobject]$definition)
                }
                foreach ($line in $RBACReport.getEnumerator()) {
                    [void]$output['RBACReport'].add($line.value)
                }


            }
            [pscustomObject]$Output
        }
    }
}