Function Remove-RBAC {
    <#
        .SYNOPSIS
        Removes basic OU skeleton for component-oriented AD
        .DESCRIPTION
        This Removes several OUs that will support the RBAC system:
         * OU=Orgs
         * OU=LinuxFeatures
         *  --> Sudoroles
         *  --> netgroups
         * OU=Global
         *  --> UnprivilegedUsers
         *  --> Rights
        .INPUTS
        none
        .OUTPUTS
        none
    #>
    [CmdletBinding(SupportsShouldProcess=$true,ConfirmImpact="High")]
    Param(
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    Begin {
        $Domain = get-addomain
        $defaultUsersDN = "CN=Users,$($domain.distinguishedName)"
        $defaultComputersDN = "CN=Computers,$($domain.distinguishedName)"
        $shouldProcess = @{
            Confirm = [bool]($ConfirmPreference -eq "low")
            Whatif = [bool]($WhatIfPreference.IsPresent)
            verbose = [bool]($VerbosePreference -ne "SilentlyContinue")
        }
        $OrgsBase = $Settings.OUPaths.OrgsBase
    }
    PROCESS {
        if ($PSCmdlet.ShouldProcess($defaultUsersDN,"Redirecting default user container")) {
            redirusr $defaultUsersDN
        }
        if ($PSCmdlet.ShouldProcess($Settings.OUPaths.TenantRoot,"Migrating all contained users --> $defaultUsersDN")) {
            try {
                get-aduser -server $server -searchBase $Settings.OUPaths.TenantRoot -filter * | move-adobject -server $server -targetPath $DefaultUsersDN @shouldProcess
            } catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
                if ($_.CategoryInfo.activity -ne "Get-aduser") {
                    throw $_
                }
            }
        }

        if ($PSCmdlet.ShouldProcess($defaultUsersDN,"Redirecting default Computer container and migrating computer objects there")) {
            redircmp $defaultComputersDN
        }
        if ($PSCmdlet.ShouldProcess($Settings.OUPaths.TenantRoot,"Migrating all contained Computers --> $defaultComputersDN")) {
            try {
                $ComputersMoved = get-adComputer -server $server -searchBase $Settings.OUPaths.TenantRoot -filter * | move-adobject -server $server -targetPath $DefaultComputersDN @shouldProcess -passthru
                write-host ("Moved {0} computers to default OU ({1})" -f $ComptersMoved.count, $DefaultComputersDN)
            } catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] {
                #we can ignore errors in getting computers from the searchbase
                if ($_.CategoryInfo.activity -ne "Get-adComputer") {
                    throw $_
                }
            }
            write-loghandler -level "Verbose" -message "Waiting for changes to process...."
            start-sleep -seconds 2
        }
        if ($PSCmdlet.ShouldProcess($Settings.OUPaths.TenantRoot,"Removing msMQ-Custom-Recipient protection")) {
            get-adobject -server $server -Filter "objectClass -eq 'msMQ-Custom-Recipient'" -SearchBase $Settings.OUPaths.TenantRoot -SearchScope OneLevel | set-adobject -server $server -ProtectedFromAccidentalDeletion $false -confirm:$false
        }
        $DeleteOUs = @(
            $Settings.OUPaths.TenantRoot
            $Settings.OUPaths.LinuxFeaturesBase
            $Settings.OUPaths.Global
        )
        foreach ($Path in $DeleteOUs) {
            if ($PSCmdlet.ShouldProcess($Path,"Deleting OU Subtree")) {
                write-loghandler -level "warning" -message "!!! Deleting OU Subtree: $Path"
                DeleteOUSubtreeWithConfirm -path $Path @shouldProcess -server $server
            }
        }
    }
}