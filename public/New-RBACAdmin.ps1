function New-RBACAdmin {
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateScript({
            $Results = [adsisearcher]::new(
                [ADSI]"LDAP://$($Settings['OUPaths'].DefaultUsers)",
                "(&(name=$_)(objectclass=user))"
            ).findAll()
            $results.count -eq 1
        })]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            [string[]]$(
                [adsisearcher]::new(
                    [ADSI]"LDAP://$($Settings['OUPaths'].DefaultUsers)",
                    "(&(name=$wordToComplete*)(objectclass=user))",
                    "name"
                ).findAll()).properties.name
        })]
        [String]$ParentUser
    )

    BEGIN {
        $SA_OU_Name = $settings['OUPaths'].PrivilegedUsers
        $DefaultUserOU = $Settings['OUPaths'].DefaultUsers
        $NamePrefix = "SA_"
        $PropertiesToFetch = @(
            "name",
            "givenName",
            "sn",
            "samAccountName",
            "userPrincipalName",
            "sshPublicKeys",
            "AltSecurityIdentities",
            "displayname",
            "telephoneNumber",
            "mail",
            "HomeDirectory",
            "HomeDrive"
        )
    }

    Process {
        $ParentUserObject = $([adsisearcher]::new(
            [ADSI]"LDAP://$DefaultUserOU",
            "(&(name=$ParentUser)(objectclass=user))",
            $PropertiesToFetch
        ).findAll())

        $SA_SAMAccountName = "{0}{1}" -f $namePrefix, [string]$ParentUserObject.properties['samaccountName']

        $ExistingSAs = [adsiSearcher]::new(
            "(&(samaccountname=$SA_SAMAccountName)(objectClass=user))"
        ).findAll()
        if ($existingSAs.count -gt 1) {
            write-warning "Multiple SA Exists, bailing"
            break
        } elseif ($existingSAs.count -eq 1) {
            $commonName = $existingSAs[0].Properties.name
        } else {
            $commonName = $SA_SAMAccountName
        }
        $properties = @{
            userPrincipalName = "{0}{1}" -f $namePrefix, [string]$ParentUserObject.properties['userprincipalName']
            DisplayName      = "⚠️{0} (Admin)⚠️" -f [string]$ParentUserObject.properties['DisplayName']
            SAMAccountName = $SA_SAMAccountName
        }
        foreach ($property in $ParentUserObject.properties.getEnumerator()) {
            if (-not $properties.containsKey($property.name)) {
                $properties.add($property.name,[string]$property.value)
            }
        }
        $properties.remove("name")
        $properties.remove("adspath")
        set-dsobject -name $CommonName -path $SA_OU_Name -objectClass user -Attributes $Properties -forceUpdate
    }
}
