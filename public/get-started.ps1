﻿Function get-started {
@"
This is the AD-rbac shell, which automates the creation of OUs, security groups, and delegations.
To get started, you should familiarize yourself with the commands available by running:
    get-command -module ad-rbac

Most operations will use one of these commands:

    get-RBACOrg             # Shows existing orgs and their AD path
    get-RBACComponent       # Same as above for components
    Add-RBACOrg             # Adds a new org, or sets an existing org's descriptions, DACLs, and GPOs to conform to the template
    Add-RBACComponent       # Same as above for components
    Add-RBACServiceAccount  # Create service accounts
    New-RBACReport          # Output an RBAC chart of rights / roles in CSV or console format

New deployments will usually start with 'Add-RBAC', which will create a monolithic OU structure.
You can view the `$settings variable (`$settings['OUPaths']) for details on what would be or has been deployed.

Most of the commands should have helpfiles so if you are unsure of usage you can run,
    get-help get-RBACOrg -detailed

Currently logged in as: $env:username
"@
}