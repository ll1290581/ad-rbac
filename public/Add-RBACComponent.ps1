Function Add-RBACComponent {
    <#
        .SYNOPSIS
        Adds a "component" OU structure from a predefined template under the specified 'org'
        .DESCRIPTION
        This creates a regular OU structure in Active Directory representing a software stack or project sharing a common ownership, and lifecycle.
        .PARAMETER Component
        The name of the 'component'
        .PARAMETER Description
        The description for the Component OU
        .PARAMETER Org
        The name of the parent 'org' owning this component

        .EXAMPLE
        add-rbacComponent -Component "Splunk" -description "Splunk Log Aggregation" -Org "Developers"

        This creates an OU tree at "OU=Splunk,OU=Components,OU=Developers,OU=Orgs,DC=Contoso,DC=Local"
        It should contain the following children OUs (defined by template):
        * Endpoints         -- For Computer objects
        * Rights            -- DomainLocal groups granting access to elements inside this software stack
        * ServiceAccounts   -- Service accounts including gMSAs
        .INPUTS
        System.String
        .OUTPUTS
        $null
    #>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param
    (
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateLength(1,15)]
        [Alias("Name")]
        [String]$Component,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String]$Description,

        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [ValidateScript({[bool](get-rbacOrg -org $_)})]
        [ArgumentCompleter( {
            param ( $commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameters )
            (get-rbacOrg -org "$wordToComplete*").Org
        })]
        [String]$Org,

        [switch]$ResetRoleMembership,
        [switch]$ResetRightsMembership,
        [Microsoft.ActiveDirectory.Management.ADDirectoryServer]$Server = (get-addomainController -Writable -Discover)
    )
    BEGIN {
        $shouldProcess = @{
            Confirm = [bool]($ConfirmPreference -eq "low")
            Whatif = [bool]($WhatIfPreference.IsPresent)
            verbose = [bool]($VerbosePreference -ne "SilentlyContinue")
        }
    }
    PROCESS {
        write-loghandler -level "Verbose" -message "Beginning add-component process (DC: $($Server.Hostname))"
        $ResetParams = @{
            ResetRoleMembership = [bool]($ResetRoleMembership)
            ResetRightsMembership = [bool]($ResetRightsMembership)
        }
        if ($PsItem.org) { $Org = $_.Org}
        if ($PsItem.Component) {$Component = $_.Component}
        if ($PsItem.Description) {$Description = $_.Description}
        $OrgPath = "OU={0},{1}" -f $Org, $Settings.OUPaths.OrgsBase
        $ComponentBasePath = "OU={0},{1}" -f $Settings.Names.ComponentsOU, $OrgPath
        Add-OUStructureFromTemplate -name $Component -Description $Description -AsComponent -parentOrg $org @shouldProcess @ResetParams -server $Server
    }
}