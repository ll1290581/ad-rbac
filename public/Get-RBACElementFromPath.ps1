function get-RBACElementFromPath {
    [CmdletBinding(SupportsShouldProcess=$false)]
    Param
    (
        [Parameter(Mandatory=$False, Position = 0)]
        [ValidateScript({ [bool](get-adorganizationalUnit -identity $_) })]
        [String]$distinguishedName
    )
    Begin {
        $FullDict = @{}
        get-RBACComponent -detailed | foreach-object {
            $FullDict[$_.distinguishedName] = $_
        }
        Get-RBACOrg -includeGlobal -detailed | foreach-Object {
            $FullDict[$_.distinguishedName] = $_
        }
    }
    Process {
        $fullDict[$distinguishedName]
    }
    End {
    }
}