#requires -modules ADDSInfo
Function Test-RBACFeatures {
    @{
        sudoRoles = [bool]$(get-DSSchemaClass -objectName "sudoRole")
        sshPublicKeys = [bool]$(get-DSSchemaClass -objectName "sshPublicKeys")
        LAPS = [bool]$(get-DSSchemaClass -objectName "msLAPS-EncryptedPassword")
        PKI = try {
                [bool]$(
                    [adsisearcher]::new(
                        [adsi]"LDAP://CN=Public Key Services,CN=Services,CN=Configuration,$(([adsi]"LDAP://RootDSE").rootDomainNamingContext)",
                        '(!(|(objectClass=Container)(name=OID)))'
                    ).findOne()
                )
            } catch {
                $False
            }
    }
}